@extends('frontend.Layout.master')
@section('title','Liên hệ với chúng tôi')
@section('content')
    <div id="PageContainer" class="is-moved-by-drawer">
        <main class="main-content" role="main">
            <div id="page-wrapper">
                <div class="wrapper">
                    <div class="inner">
                        <div class="grid">
                            <div class="grid__item large--one-whole">
                                <div class="page-contact-wrapper">
                                    <div class="page-body">
                                        <div class="grid">
                                            <div class="contact-wrapper">
                                                <div class="grid__item large--one-third medium--one-third small--one-whole">
                                                    <div class="contact-item">
                                                        <div class="contact-title">
                                                            <div class="contact-icon">
                                                                <svg class="svg-inline--fa fa-map-marker fa-w-12" aria-hidden="true" data-prefix="fa" data-icon="map-marker" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 384 512" data-fa-i2svg=""><path fill="currentColor" d="M172.268 501.67C26.97 291.031 0 269.413 0 192 0 85.961 85.961 0 192 0s192 85.961 192 192c0 77.413-26.97 99.031-172.268 309.67-9.535 13.774-29.93 13.773-39.464 0z"></path></svg><!-- <i class="fa fa-map-marker" aria-hidden="true"></i> -->
                                                            </div>
                                                            <h4>
                                                                Địa chỉ
                                                            </h4>
                                                        </div>
                                                        <div class="contact-info">
                                                            Số nhà 28, liền kề 36, khu đô thị mới Vân Canh, Xã Vân Canh, Huyện Hoài Đức, Thành phố Hà Nội, Việt Nam
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="grid__item large--one-third medium--one-third small--one-whole">
                                                    <div class="contact-item">
                                                        <div class="contact-title">
                                                            <div class="contact-icon">
                                                               <i class="fa fa-phone" aria-hidden="true"></i>
                                                            </div>
                                                            <h4>
                                                                Số điện thoại:
                                                            </h4>
                                                        </div>
                                                        <div class="contact-info">
                                                            <a href="tel:097.800.7172 ">097.800.7172</a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="grid__item large--one-third medium--one-third small--one-whole">
                                                    <div class="contact-item">
                                                        <div class="contact-icon">
                                                            <svg class="svg-inline--fa fa-envelope fa-w-16" aria-hidden="true" data-prefix="fa" data-icon="envelope" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" data-fa-i2svg=""><path fill="currentColor" d="M502.3 190.8c3.9-3.1 9.7-.2 9.7 4.7V400c0 26.5-21.5 48-48 48H48c-26.5 0-48-21.5-48-48V195.6c0-5 5.7-7.8 9.7-4.7 22.4 17.4 52.1 39.5 154.1 113.6 21.1 15.4 56.7 47.8 92.2 47.6 35.7.3 72-32.8 92.3-47.6 102-74.1 131.6-96.3 154-113.7zM256 320c23.2.4 56.6-29.2 73.4-41.4 132.7-96.3 142.8-104.7 173.4-128.7 5.8-4.5 9.2-11.5 9.2-18.9v-19c0-26.5-21.5-48-48-48H48C21.5 64 0 85.5 0 112v19c0 7.4 3.4 14.3 9.2 18.9 30.6 23.9 40.7 32.4 173.4 128.7 16.8 12.2 50.2 41.8 73.4 41.4z"></path></svg><!-- <i class="fa fa-envelope" aria-hidden="true"></i> -->
                                                        </div>
                                                        <div class="contact-title">
                                                            <h4>
                                                                Email
                                                            </h4>
                                                        </div>
                                                        <div class="contact-info">
                                                            <a href="mailto:info@ambio.vn">info@ambio.vn</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="page-head">
                                            <h1>Liên hệ</h1>
                                        </div>
                                        <div class="form-vertical clearfix">
                                            <form accept-charset="UTF-8" action="{!! route('postcomment') !!}" class="contact-form" method="post">
                                                <input name="form_type" type="hidden" value="contact">
                                                <input name="utf8" type="hidden" value="✓">

                                                <div class="grid">
                                                    <div class="grid__item large--one-third medium--one-third small--one-whole">

                                                        <label for="ContactFormName" class="hidden-label">Họ tên của bạn</label>
                                                        <input type="text" id="ContactFormName" class="input-full" name="name" placeholder="Họ tên của bạn" autocapitalize="words" value="">
                                                    </div>
                                                    <div class="grid__item large--one-third medium--one-third small--one-whole">
                                                        <label for="ContactFormEmail" class="hidden-label">Địa chỉ email của bạn</label>
                                                        <input type="email" id="ContactFormEmail" class="input-full" name="email" placeholder="Địa chỉ email của bạn" autocorrect="off" autocapitalize="off" value="">
                                                    </div>

                                                    <div class="grid__item large--one-third medium--one-third small--one-whole">
                                                        <label for="ContactFormEmail" class="hidden-label">Địa chỉ</label>
                                                        <input type="email" id="ContactFormEmail" class="input-full" name="address" placeholder="Địa chỉ của bạn" autocorrect="off" autocapitalize="off" value="">
                                                    </div>
                                                    <div class="grid__item large--one-third medium--one-third small--one-whole">

                                                        <label for="ContactFormPhone" class="hidden-label">Số điện thoại của bạn</label>
                                                        <input type="tel" id="ContactFormPhone" class="input-full" name="phone" placeholder="Số điện thoại của bạn" pattern="[0-9\-]*" value="">
                                                    </div>
                                                    <div class="grid__item">
                                                        <label for="ContactFormMessage" class="hidden-label">Nội dung</label>
                                                        <textarea rows="10" id="ContactFormMessage" class="input-full" name="contents" placeholder="Nội dung"></textarea>

                                                    </div>
                                                    <div class="grid__item text-center">
                                                        <input type="submit" class="btn btnContactSubmit" value="Gửi">
                                                    </div>
                                                </div>

                                                <input id="83d88429c644455e9c9caf3a00a647a9" name="g-recaptcha-response" type="hidden" value="03AGdBq26W34PwUiY9mfs9kySoxiK7LzN9bChI0EeM1VDMGayE5NWfVObarff8yFuWayB4pIunbP9atP8iKeT_EXyedsJ1ZiV9pMfYyQIxri9HD7t9UagswehCnpEn6AIvucpPKlfcQ_sQLPcU5hw7T-8byEiI_-fksqzVxknBw1_JJ9wZWfYMAnXmr7-UsK9wSUwH4fKIVeaMiN5SPxh44LTFpQGR0Ksv1FuDLrRQ_qbfKOTtzGDZ-I60OQ74mKJKutRuESHm3KvzJYNxnqJ0rwpH9zQmzcbrVsFJxJSA34bgquRtMkUT-tfp8KE2rdXrVymGrYBEDeAvMlQFyQZoEHNQ4IK9KqR20qbcfg3jIHDjsMgcmumuAc6zms5apDmcK8SeD71otl4gKK3J4rKYjqYHlyi8cnJzpGvD_mj9y3Kc-EASdli0aHq-nAkLvm3vt7TE5XU5_MKc"><script src="https://www.google.com/recaptcha/api.js?render=6LdD18MUAAAAAHqKl3Avv8W-tREL6LangePxQLM-"></script><script>grecaptcha.ready(function() {grecaptcha.execute('6LdD18MUAAAAAHqKl3Avv8W-tREL6LangePxQLM-', {action: 'submit'}).then(function(token) {document.getElementById('83d88429c644455e9c9caf3a00a647a9').value = token;});});</script></form>
                                        </div>
                                    </div>
                                    <div class="contact-map">
                                        <iframe src="https://goo.gl/maps/BCPibURsJJBWaBXN7" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>



        </main>
    </div>
@stop
