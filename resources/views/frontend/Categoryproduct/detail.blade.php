@extends('frontend.Layout.master')
@section('title',$catpro->categoryName)
@section('content')
    <section id="collection-wrapper">
        <div class="wrapper">
            <div class="inner">
                <div class="grid">
                    <div class="grid__item large--three-quarters medium--one-whole small--one-whole" style="width: 100% !important;">
                        <div class="article-item article-featured">
                            <div class="collection-head">
                                <div class="grid mg-left-15">
                                    <div class="grid__item large--one-half medium--one-whole small--one--whole">
                                        <div class="collection-title">
                                            <h1>{!! $catpro->categoryName !!}</h1>
                                        </div>
                                    </div>

{{--                                    <div--}}
{{--                                        class="grid__item large--one-third medium--one-whole small--one-whole pd-left15">--}}
{{--                                        <div class="collection-sorting-wrapper">--}}
{{--                                            <!-- /snippets/collection-sorting.liquid -->--}}
{{--                                            <div class="form-horizontal text-right">--}}
{{--                                                <label for="SortBy">Sắp xếp</label>--}}
{{--                                                <select name="SortBy" id="SortBy">--}}
{{--                                                    <option value="manual">Tùy chọn</option>--}}
{{--                                                    <option value="1">Theo bảng chữ cái từ A-Z</option>--}}
{{--                                                    <option value="2">Theo bảng chữ cái từ Z-A</option>--}}
{{--                                                    <option value="3">Giá từ thấp tới cao</option>--}}
{{--                                                    <option value="4">Giá từ cao tới thấp</option>--}}
{{--                                                    <option value="5">Mới nhất</option>--}}
{{--                                                    <option value="6">Cũ nhất</option>--}}
{{--                                                </select>--}}
{{--                                            </div>--}}

{{--                                        </div>--}}
{{--                                    </div>--}}
                                </div>
                            </div>
                            <div class="collection-body">
                                <div class="grid-uniform product-list md-mg-left-10">
                                    @if($product)
                                        @foreach($product as $itemproduct)

                                            <div
                                                class="md-pd-left10 grid__item large--one-quarter medium--one-third small--one-half">
                                                <div class="product-item">
                                                    <div class="product-img">
                                                        <a href="{!! route('getdetailpro',$itemproduct->slug) !!}">
                                                            <picture>
                                                                <source media="(max-width: 480px)"
                                                                        srcset="{!! asset("uploads/product/".$itemproduct->photo) !!}">
                                                                <source media="(min-width: 481px)"
                                                                        srcset=" {!! asset("uploads/product/".$itemproduct->photo)  !!}">
                                                                <img id="1030415289" src=""
                                                                     alt="Smart Feed 4.0 - Máy cho tôm ăn thông minh"/>
                                                            </picture>
                                                        </a>
                                                        <div class="product-tags">
                                                        </div>
                                                    </div>
                                                    <div class="product-info">
                                                        <div class="product-title">
                                                            <a href="{!! route('getdetailpro',$itemproduct->slug) !!}">{!! Illuminate\Support\Str::limit($itemproduct->productname,16) !!}
                                                                ...</a>
                                                        </div>
                                                        <div class="product-price clearfix">
                                                                <span
                                                                    class="current-price">{!! number_format($itemproduct->price,0) !!}₫</span>
                                                        </div>
                                                        <div class="product-actions text-center clearfix">
                                                            <div>
                                                                <button type="button" class="btn-addToCart medium--hide small--hide" onclick="addToCart({!! $itemproduct->id !!})" data-id="{!! $itemproduct->id !!} "> <span><i class="fa fa-cart-plus" aria-hidden="true"></i></span></button>
                                                                <button type="button" class="btn-quickview quick-view medium--hide small--hide" data-toggle="modal" data-target=".bd-example-modal-lg" data-id="{!! $itemproduct->id !!} " data-name="{!! $itemproduct->productname!!}" data-price="{!! $itemproduct->price !!}" data-image="{!! asset("uploads/product/".$itemproduct->photo)  !!}" data-symbol="{!! $itemproduct->symbol !!}" data-link="{!! route('getdetailpro',[$catpro->slug,$itemproduct->slug]) !!}"><span><i class="fa fa-search-plus" aria-hidden="true"></i></span> </button>
                                                                <button type="button" class="btn-buyNow quickbuy medium--hide small--hide" data-id="{!! $itemproduct->id !!}"> Mua ngay</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    @endif

                                </div>
                                <div class="pagination not-filter">
                                    <div id="pagination-" class="text-center clear-left">
                                        <div class="pagination-custom">

                                            {{ $product->links() }}

                                        </div>
                                    </div>
                                </div>
                                <style>
                                    ul.pagination {
                                        list-style: none;
                                    }
                                    ul.pagination li {
                                        display: inline-block;
                                    }
                                </style>
                            </div>
                        </div>

                    </div>



                </div>
            </div>
        </div>
    </section>

@stop
