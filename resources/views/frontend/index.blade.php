@extends('frontend.Layout.master')
@section('title','CÔNG TY CP SAKURA GREEN')
@section('content')
    <div id="main-slider">
        @if(isset($slider))
            @foreach($slider as $itemslider)
                <div class="main-slide">
                    <img src="{!! asset("uploads/".$itemslider->image) !!}" alt=""/>
                </div>
            @endforeach
        @endif
    </div>
    <div id="PageContainer" class="is-moved-by-drawer">
        <main class="main-content" role="main">
            @if($catpro)
                @foreach($catpro as $itemcatpro)
                    <section id="home-collections">
                        <div class="wrapper">
                            <div class="inner">
                                <div class="section-title">
                                    <h2 class="uk-h2 uk-heading-line uk-text-center" id="page#5-0-0-0"><span><a
                                                class="el-link uk-link-reset" href="">{!! $itemcatpro->categoryName !!}</a></span>
                                    </h2>
                                </div>

                                <div class="section-content">
                                    <div class="tab hide">
                                    </div>
                                    <div id="tab1" class="tabcontent">
                                        <div class="grid-uniform md-mg-left-10">
                                            <?php $product = \App\Product::where(['catpro_id'=>$itemcatpro->id,'status'=>1])->orderBy('id','DESC')->take(1)->limit(4)->get() ?>
                                            @foreach($product as $itemproduct)
                                                <?php $catpro = \App\CategoryProduct::find($itemproduct->catpro_id); ?>

                                                <div
                                                    class="md-pd-left10 grid__item large--one-quarter medium--one-third small--one-half">
                                                    <input type="hidden" name="id" class="idproduct" value="{!! $itemproduct->id !!}">
                                                    <div class="product-item">
                                                        <div class="product-img">
                                                            <a href="{!! route('getdetailpro',$itemproduct->slug) !!}">
                                                                <picture>
                                                                    <source media="(max-width: 480px)"
                                                                            srcset="{!! asset("uploads/product/".$itemproduct->photo) !!}">
                                                                    <source media="(min-width: 481px)"
                                                                            srcset=" {!! asset("uploads/product/".$itemproduct->photo)  !!}">
                                                                    <img id="{!! $itemproduct->id !!}" src="{!! asset("uploads/product/".$itemproduct->photo)  !!}"
                                                                         alt="{!! $itemproduct->productname !!}"/>
                                                                </picture>
                                                            </a>
                                                            <div class="product-tags">
                                                            </div>
                                                        </div>
                                                        <div class="product-info">
                                                            <div class="product-title">
                                                                <a href="{!! route('getdetailpro',$itemproduct->slug) !!}">{!! $itemproduct->productname!!}
                                                                    </a>
                                                            </div>
                                                            <div class="product-price clearfix">
                                                                <span
                                                                    class="current-price" >{!! number_format($itemproduct->price,0) !!}₫</span>
                                                            </div>
                                                            <div class="product-actions text-center clearfix">
                                                                <div>
                                                                    <button type="button" class="btn-addToCart medium--hide small--hide" onclick="addToCart({!! $itemproduct->id !!})" data-id="{!! $itemproduct->id !!} "> <span><i class="fa fa-cart-plus" aria-hidden="true"></i></span></button>
                                                                    <button type="button" class="btn-quickview quick-view medium--hide small--hide" data-toggle="modal" data-target=".bd-example-modal-lg"  data-id="{!! $itemproduct->id !!} " data-name="{!! $itemproduct->productname!!}" data-price="{!! $itemproduct->price !!}" data-image="{!! asset("uploads/product/".$itemproduct->photo)  !!}" data-symbol="{!! $itemproduct->symbol !!}" data-link="{!! route('getdetailpro',$itemproduct->slug) !!}"><span><i class="fa fa-search-plus" aria-hidden="true"></i></span> </button>
                                                                    <button type="button" class="btn-buyNow quickbuy medium--hide small--hide" data-id="{!! $itemproduct->id !!}" >{!! trans('messages.buynow') !!}</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach()

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                @endforeach
            @endif


            <section id="home-articles">
                <div class="wrapper">
                    <div class="inner">
                        <div class="section-title">
                            <div class="section-title">
                                <h2 class="uk-h2 uk-heading-line uk-text-center" id="page#5-0-0-0"><span><a
                                            class="el-link uk-link-reset" href="">Tin tức</a></span>
                                </h2>
                            </div>
                        </div>
                        <div class="section-content">
                            <div class="grid">
                                @if(isset($firstnew))
                                    <?php $catslug = \App\CategoryNew::find($firstnew->catnew_id); ?>
                                    <div class="grid__item large--one-half medium--one-whole small--one--whole">
                                        <div class="article-item article-featured">
                                            <div class="article-img">
                                                <a href="{!! route('detailnews',[$catslug->slug,$firstnew->slug]) !!}">
                                                    <img
                                                        src="{!! asset("uploads/".$firstnew->image) !!}"
                                                        alt="{!! $firstnew->nametitle !!}">
                                                    <div class="overlay">
                                                    </div>
                                                </a>
                                            </div>
                                            <div class="article-info">
                                                <a href="{!! route('detailnews',[$catslug->slug,$firstnew->slug]) !!}"
                                                   class="article-title">
                                                    {!! $firstnew->nametitle !!}
                                                </a>
                                                <div class="article-author">
                                                    <span> Sakura green</span>
                                                </div>
                                                <div class="article-desc ">
                                                    {!! substr($firstnew->description,0) !!}...
                                                </div>
                                                <a href="{!! route('detailnews',[$catslug->slug,$firstnew->slug]) !!}"
                                                   class="article-readmore">{!! trans('messages.seemore') !!}<i
                                                        class="fas fa-long-arrow-alt-right"></i></a>
                                                <div class="article-info-relate">
                                                    <div class="article-published-at">
                                                        <i class="fa fa-calendar"></i> {!! $firstnew->created_at !!}
                                                    </div>
                                                    <div class="aricle-comment">
                                                        <i class="fa fa-comment"></i> <span class="fb-comments-count"
                                                                                            data-href=""></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endif()
                                @if(isset($new))

                                    <div class="grid__item large--one-half medium--one-whole small--one--whole">
                                        <div class="article-list">
                                            @foreach($new as $itemnew)
                                                <?php $catslug = \App\CategoryNew::find($itemnew->catnew_id); ?>
                                                <div class="article-item">
                                                    <div class="grid">
                                                        <div
                                                            class="grid__item large--one-third medium--one-third small--one-half">
                                                            <div class="article-img">
                                                                <a href="">
                                                                    <img
                                                                        src="{!! asset('uploads/'.$itemnew->image) !!}"
                                                                        alt="{!! $itemnew->nametitle !!}">
                                                                    <div class="overlay">
                                                                    </div>
                                                                </a>
                                                            </div>
                                                        </div>
                                                        <div
                                                            class="grid__item large--two-thirds medium--two-thirds small--one-half article-info">
                                                            <a href="{!! route('detailnews',[$catslug->slug,$itemnew->slug]) !!}"
                                                               class="article-title">
                                                                {!! $itemnew->nametitle !!}
                                                            </a>
                                                            <div class="article-author">
                                                                <span>Sakura green</span>
                                                            </div>
                                                            <div class="article-desc small--hide">
                                                                {!! substr($itemnew->description,0) !!}...
                                                            </div>
                                                            <a href="{!! route('detailnews',[$catslug->slug,$itemnew->slug]) !!}"
                                                               class="article-readmore">{!! trans('messages.seemore') !!}<i
                                                                    class="fas fa-long-arrow-alt-right"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                @endif
                                <div class="grid__item text-center">
                                    <a href="{!! route('catenew') !!}" class="article-viewmore">{!! trans('messages.seemore') !!}</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </main>
    </div>
@stop
