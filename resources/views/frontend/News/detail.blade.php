@extends('frontend.Layout.master')
@section('title',$new->nametitle)
@section('content')
    <section id="blog-wrapper">
        <div class="wrapper">
            <div class="inner">
                <div class="grid">
                    @if($new)
                        <article class=" grid__item large--seven-tenths medium--one-whole small--one-whole" itemscope=""
                                 itemtype="http://schema.org/Article">
                            <div class="article-content">
                                <div class="article-head">
                                    <h1>{!! $new->nametitle !!}</h1>
                                    <div class="grid mg-left-15">
                                        <div
                                            class="grid__item large--one-half medium--one-half small--one-half pd-left15">
                                            <div class="article-date-comment">
                                                <div class="date">
                                                    <svg class="svg-inline--fa fa-calendar fa-w-14" aria-hidden="true"
                                                         data-prefix="fa" data-icon="calendar" role="img"
                                                         xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"
                                                         data-fa-i2svg="">
                                                        <path fill="currentColor"
                                                              d="M12 192h424c6.6 0 12 5.4 12 12v260c0 26.5-21.5 48-48 48H48c-26.5 0-48-21.5-48-48V204c0-6.6 5.4-12 12-12zm436-44v-36c0-26.5-21.5-48-48-48h-48V12c0-6.6-5.4-12-12-12h-40c-6.6 0-12 5.4-12 12v52H160V12c0-6.6-5.4-12-12-12h-40c-6.6 0-12 5.4-12 12v52H48C21.5 64 0 85.5 0 112v36c0 6.6 5.4 12 12 12h424c6.6 0 12-5.4 12-12z"></path>
                                                    </svg><!-- <i class="fa fa-calendar" aria-hidden="true"></i> -->
                                                    {!! $new->created_at !!}
                                                </div>

                                                <div class="author">
                                                    <i class="fa fa-user" aria-hidden="true"></i>Sakura Green
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                                <div class="article-body">
                                    {!! $new->description !!}
                                    {!! $new->contents !!}
                                </div>

                            </div>
                            <div class="social-network-actions-outside text-right">
                                <div class="fb-send"
                                     data-href="https://shop.ambio.vn/blogs/news/ambio-giai-phap-toi-uu-cai-thien-chat-luong-nuoc-ao-nuoi"></div>
                                <div class="fb-like fb_iframe_widget"
                                     data-href="https://shop.ambio.vn/blogs/news/ambio-giai-phap-toi-uu-cai-thien-chat-luong-nuoc-ao-nuoi"
                                     data-layout="button" data-action="like" data-size="small" data-show-faces="true"
                                     data-share="true" fb-xfbml-state="rendered"
                                     fb-iframe-plugin-query="action=like&amp;app_id=&amp;container_width=0&amp;href=https%3A%2F%2Fshop.ambio.vn%2Fblogs%2Fnews%2Fambio-giai-phap-toi-uu-cai-thien-chat-luong-nuoc-ao-nuoi&amp;layout=button&amp;locale=vi_VN&amp;sdk=joey&amp;share=true&amp;show_faces=true&amp;size=small">
                                    <span style="vertical-align: bottom; width: 120px; height: 28px;"><iframe
                                            name="f2ad850ebd0fea8" width="1000px" height="1000px"
                                            data-testid="fb:like Facebook Social Plugin"
                                            title="fb:like Facebook Social Plugin" frameborder="0"
                                            allowtransparency="true" allowfullscreen="true" scrolling="no"
                                            allow="encrypted-media"
                                            src="https://www.facebook.com/v2.11/plugins/like.php?action=like&amp;app_id=&amp;channel=https%3A%2F%2Fstaticxx.facebook.com%2Fx%2Fconnect%2Fxd_arbiter%2F%3Fversion%3D46%23cb%3Df2c15f4459fbdcc%26domain%3Dshop.ambio.vn%26is_canvas%3Dfalse%26origin%3Dhttps%253A%252F%252Fshop.ambio.vn%252Ff2441f8d77d44d4%26relation%3Dparent.parent&amp;container_width=0&amp;href=https%3A%2F%2Fshop.ambio.vn%2Fblogs%2Fnews%2Fambio-giai-phap-toi-uu-cai-thien-chat-luong-nuoc-ao-nuoi&amp;layout=button&amp;locale=vi_VN&amp;sdk=joey&amp;share=true&amp;show_faces=true&amp;size=small"
                                            style="border: none; visibility: visible; width: 120px; height: 28px;"
                                            class=""></iframe></span></div>
                                <div class="zalo-share-button" data-href="" data-oaid="579745863508352884"
                                     data-layout="2" data-color="blue" data-customize="false"></div>
                            </div>
                            <div id="section-fbcomment" class="fb-comments-wrapper">
                                <div class="fb-comments fb_iframe_widget fb_iframe_widget_fluid_desktop"
                                     data-href="https://shop.ambio.vn/blogs/news/ambio-giai-phap-toi-uu-cai-thien-chat-luong-nuoc-ao-nuoi"
                                     data-width="100%" data-numposts="5" fb-xfbml-state="rendered"
                                     fb-iframe-plugin-query="app_id=&amp;container_width=782&amp;height=100&amp;href=https%3A%2F%2Fshop.ambio.vn%2Fblogs%2Fnews%2Fambio-giai-phap-toi-uu-cai-thien-chat-luong-nuoc-ao-nuoi&amp;locale=vi_VN&amp;numposts=5&amp;sdk=joey&amp;version=v2.11&amp;width="
                                     style="width: 100%;"><span
                                        style="vertical-align: bottom; width: 100%; height: 202px;"><iframe
                                            name="f1fd3c8816b1664" width="1000px" height="100px"
                                            data-testid="fb:comments Facebook Social Plugin"
                                            title="fb:comments Facebook Social Plugin" frameborder="0"
                                            allowtransparency="true" allowfullscreen="true" scrolling="no"
                                            allow="encrypted-media"
                                            src="https://www.facebook.com/v2.11/plugins/comments.php?app_id=&amp;channel=https%3A%2F%2Fstaticxx.facebook.com%2Fx%2Fconnect%2Fxd_arbiter%2F%3Fversion%3D46%23cb%3Df35be1ce6e77648%26domain%3Dshop.ambio.vn%26is_canvas%3Dfalse%26origin%3Dhttps%253A%252F%252Fshop.ambio.vn%252Ff2441f8d77d44d4%26relation%3Dparent.parent&amp;container_width=782&amp;height=100&amp;href=https%3A%2F%2Fshop.ambio.vn%2Fblogs%2Fnews%2Fambio-giai-phap-toi-uu-cai-thien-chat-luong-nuoc-ao-nuoi&amp;locale=vi_VN&amp;numposts=5&amp;sdk=joey&amp;version=v2.11&amp;width="
                                            style="border: none; visibility: visible; width: 100%; height: 202px;"
                                            class=""></iframe></span></div>
                            </div>

                            <div class="related-articles">
                                <div class="related-articles-head">
                                    <h3>
                                        Các bài viết liên quan
                                    </h3>
                                </div>
                                <div class="related-articles-body">

                                    <ul class="no-bullets">

                                        @if($news)
                                            @foreach($news as $item)
                                                <?php $catnew = \App\CategoryNew::where('id', $item->catnew_id)->first(); ?>
                                                <li>
                                                    <a href="{!! route('detailnews',[$catnew->slug,$item->slug]) !!}">{!! $item->nametitle !!}</a>
                                                </li>
                                            @endforeach
                                        @endif
                                    </ul>
                                </div>
                            </div>


                        </article>
                    @endif
                    <div class="grid__item large--three-tenths medium--one-whole small--one-whole">
                        <div class="blog-sidebar">

                            <div class="list-categories">
                                <div class="blog-sb-title clearfix">
                                    <h3>
                                        Danh mục tin tức
                                    </h3>
                                </div>
                                <ul class="no-bullets">

                                </ul>
                            </div>


                            <div class="all-tags">
                                <div class="blog-sb-title clearfix">
                                    <h3>
                                        Từ khóa
                                    </h3>
                                </div>
                                <div class="all-tags-wrapper clearfix">

                                </div>
                            </div>


                            <div class="grid gallery-wrapper mg-left-15">


                                <div class=" grid__item large--one-third medium--one-third small--one-third  pd-left15">
                                    <div class="gallery-item">

                                        <a href="//theme.hstatic.net/1000345738/1000619099/14/ft_1.png?v=399"
                                           data-fancybox="home-gallery-images" data-caption="gallery 1">
                                            <img src="//theme.hstatic.net/1000345738/1000619099/14/ft_1.png?v=399"
                                                 alt="gallery 1">
                                            <div class="overlay">

                                            </div>
                                        </a>
                                    </div>
                                </div>


                                <div class=" grid__item large--one-third medium--one-third small--one-third  pd-left15">
                                    <div class="gallery-item">

                                        <a href="//theme.hstatic.net/1000345738/1000619099/14/ft_2.png?v=399"
                                           data-fancybox="home-gallery-images" data-caption="gallery 2">
                                            <img src="//theme.hstatic.net/1000345738/1000619099/14/ft_2.png?v=399"
                                                 alt="gallery 2">
                                            <div class="overlay">

                                            </div>
                                        </a>
                                    </div>
                                </div>


                                <div class=" grid__item large--one-third medium--one-third small--one-third  pd-left15">
                                    <div class="gallery-item">

                                        <a href="//theme.hstatic.net/1000345738/1000619099/14/ft_3.png?v=399"
                                           data-fancybox="home-gallery-images" data-caption="gallery 3">
                                            <img src="//theme.hstatic.net/1000345738/1000619099/14/ft_3.png?v=399"
                                                 alt="gallery 3">
                                            <div class="overlay">

                                            </div>
                                        </a>
                                    </div>
                                </div>


                                <div class=" grid__item large--one-third medium--one-third small--one-third  pd-left15">
                                    <div class="gallery-item">

                                        <a href="//theme.hstatic.net/1000345738/1000619099/14/ft_4.png?v=399"
                                           data-fancybox="home-gallery-images" data-caption="gallery 4">
                                            <img src="//theme.hstatic.net/1000345738/1000619099/14/ft_4.png?v=399"
                                                 alt="gallery 4">
                                            <div class="overlay">

                                            </div>
                                        </a>
                                    </div>
                                </div>


                                <div class=" grid__item large--one-third medium--one-third small--one-third  pd-left15">
                                    <div class="gallery-item">

                                        <a href="//theme.hstatic.net/1000345738/1000619099/14/ft_5.png?v=399"
                                           data-fancybox="home-gallery-images" data-caption="gallery 5">
                                            <img src="//theme.hstatic.net/1000345738/1000619099/14/ft_5.png?v=399"
                                                 alt="gallery 5">
                                            <div class="overlay">

                                            </div>
                                        </a>
                                    </div>
                                </div>


                                <div class=" grid__item large--one-third medium--one-third small--one-third  pd-left15">
                                    <div class="gallery-item">

                                        <a href="//theme.hstatic.net/1000345738/1000619099/14/ft_6.png?v=399"
                                           data-fancybox="home-gallery-images" data-caption="gallery 6">
                                            <img src="//theme.hstatic.net/1000345738/1000619099/14/ft_6.png?v=399"
                                                 alt="gallery 6">
                                            <div class="overlay">

                                            </div>
                                        </a>
                                    </div>
                                </div>


                            </div>


                            <script async defer crossorigin="anonymous"
                                    src="https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v11.0"
                                    nonce="U5wb7NyZ"></script>
                            <div class="fb-page" data-href="https://www.facebook.com/SakuraGreen.cnsh"
                                 data-tabs="timeline" data-width="" data-height="" data-small-header="false"
                                 data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true">
                                <blockquote cite="https://www.facebook.com/SakuraGreen.cnsh"
                                            class="fb-xfbml-parse-ignore"><a
                                        href="https://www.facebook.com/SakuraGreen.cnsh">SakuraGreen</a></blockquote>
                            </div>

                        </div>

                    </div>

                </div>
            </div>
        </div>

    </section>

@stop
