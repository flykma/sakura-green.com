@extends('frontend.Layout.master')
@section('title','Giới thiệu công ty')
@section('content')
    <div id="PageContainer" class="is-moved-by-drawer">
        <main class="main-content" role="main">
            <section id="page-wrapper">
                <div class="wrapper">
                    <div class="inner">
                        <div class="grid">

                            <div class="grid__item large--one-whole">

                                <h1>{!! $catintro->name !!}</h1>
                                <div class="rte">
                                    {!! $intro->contents !!}
                                </div>

                            </div>

                        </div>
                    </div>
                </div>
            </section>

        </main>
    </div>
@stop
