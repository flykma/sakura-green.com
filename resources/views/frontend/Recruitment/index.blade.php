@extends('frontend.Layout.master')
@section('content')
    <div id="PageContainer" class="is-moved-by-drawer">
        <main class="main-content" role="main">
            <section id="blog-wrapper">
                <div class="wrapper">
                    <div class="inner">
                        <div class="grid">

                            <div class="grid__item large--seven-tenths medium--one-whole small--one-whole">
                                <div class="blog-content">

                                    <div class="blog-content-wrapper">
                                        <div class="blog-head">
                                            <div class="blog-title">
                                                <h1>Tuyển dụng</h1>
                                            </div>
                                        </div>
                                        <div class="blog-body">
                                            <div class="grid-uniform">


                                                <div
                                                    class="grid__item large--one-half medium--one-half small--one-whole">
                                                    <div class="article-item">
                                                        <div class="article-img">
                                                            <a href="/blogs/tuyen-dung/tuyen-quan-ly-kinh-doanh-khu-vuc">
                                                                <img
                                                                    src="//file.hstatic.net/1000345738/article/image1_45550469279e4376b0d056162fb11f02_large.jpeg"
                                                                    alt="TUYỂN QUẢN LÝ KINH DOANH KHU VỰC">
                                                                <div class="overlay">
                                                                </div>
                                                            </a>
                                                        </div>
                                                        <div class="article-info">
                                                            <a href="/blogs/tuyen-dung/tuyen-quan-ly-kinh-doanh-khu-vuc"
                                                               class="article-title">
                                                                TUYỂN QUẢN LÝ KINH DOANH KHU VỰC
                                                            </a>
                                                            <div class="article-author">
                                                                <span> AmBio  Việt Nam</span>
                                                            </div>
                                                            <div class="article-desc">

                                                                📣BẠN MUỐN CÓ MỘT CÔNG VIỆC CHỦ ĐỘNG, SÁNG&nbsp;TẠO
                                                                TRONG LĨNH VỰC THỦY SẢN?📣BẠN MUỐN LÀM VIỆC CHO MỘT
                                                                THƯƠNG HIỆU NÔI BẬT VÀ KHÁC BIỆT? ĐƯỢC ĐÀO TẠO VÀ HUẤN
                                                                LUYỆN THƯỜNG XUYÊN?📣ĐƯ...

                                                            </div>
                                                            <a href="" class="article-readmore">Xem thêm
                                                                <svg
                                                                    class="svg-inline--fa fa-long-arrow-alt-right fa-w-14"
                                                                    aria-hidden="true" data-prefix="fas"
                                                                    data-icon="long-arrow-alt-right" role="img"
                                                                    xmlns="http://www.w3.org/2000/svg"
                                                                    viewBox="0 0 448 512" data-fa-i2svg="">
                                                                    <path fill="currentColor"
                                                                          d="M313.941 216H12c-6.627 0-12 5.373-12 12v56c0 6.627 5.373 12 12 12h301.941v46.059c0 21.382 25.851 32.09 40.971 16.971l86.059-86.059c9.373-9.373 9.373-24.569 0-33.941l-86.059-86.059c-15.119-15.119-40.971-4.411-40.971 16.971V216z"></path>
                                                                </svg>
                                                                <!-- <i class="fas fa-long-arrow-alt-right"></i> --></a>
                                                            <div class="article-info-relate">
                                                                <div class="article-published-at">
                                                                    <svg class="svg-inline--fa fa-calendar fa-w-14"
                                                                         aria-hidden="true" data-prefix="fa"
                                                                         data-icon="calendar" role="img"
                                                                         xmlns="http://www.w3.org/2000/svg"
                                                                         viewBox="0 0 448 512" data-fa-i2svg="">
                                                                        <path fill="currentColor"
                                                                              d="M12 192h424c6.6 0 12 5.4 12 12v260c0 26.5-21.5 48-48 48H48c-26.5 0-48-21.5-48-48V204c0-6.6 5.4-12 12-12zm436-44v-36c0-26.5-21.5-48-48-48h-48V12c0-6.6-5.4-12-12-12h-40c-6.6 0-12 5.4-12 12v52H160V12c0-6.6-5.4-12-12-12h-40c-6.6 0-12 5.4-12 12v52H48C21.5 64 0 85.5 0 112v36c0 6.6 5.4 12 12 12h424c6.6 0 12-5.4 12-12z"></path>
                                                                    </svg><!-- <i class="fa fa-calendar"></i> -->
                                                                    14/08/2020
                                                                </div>
                                                                <div class="aricle-comment">
                                                                    <svg class="svg-inline--fa fa-comment fa-w-18"
                                                                         aria-hidden="true" data-prefix="fa"
                                                                         data-icon="comment" role="img"
                                                                         xmlns="http://www.w3.org/2000/svg"
                                                                         viewBox="0 0 576 512" data-fa-i2svg="">
                                                                        <path fill="currentColor"
                                                                              d="M576 240c0 115-129 208-288 208-48.3 0-93.9-8.6-133.9-23.8-40.3 31.2-89.8 50.3-142.4 55.7-5.2.6-10.2-2.8-11.5-7.7-1.3-5 2.7-8.1 6.6-11.8 19.3-18.4 42.7-32.8 51.9-94.6C21.9 330.9 0 287.3 0 240 0 125.1 129 32 288 32s288 93.1 288 208z"></path>
                                                                    </svg><!-- <i class="fa fa-comment"></i> --> <span
                                                                        class="fb-comments-count fb_comments_count_zero_fluid_desktop"
                                                                        data-href="https://shop.ambio.vn/blogs/tuyen-dung/tuyen-quan-ly-kinh-doanh-khu-vuc"
                                                                        fb-xfbml-state="parsed"
                                                                        fb-iframe-plugin-query="app_id=&amp;container_width=0&amp;count=true&amp;height=100&amp;href=https%3A%2F%2Fshop.ambio.vn%2Fblogs%2Ftuyen-dung%2Ftuyen-quan-ly-kinh-doanh-khu-vuc&amp;locale=vi_VN&amp;sdk=joey&amp;version=v2.11&amp;width=550"><span
                                                                            class="fb_comments_count">0</span></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="grid__item large--three-tenths medium--one-whole small--one-whole">
                                <div class="blog-sidebar">

                                    <div class="list-categories">
                                        <div class="blog-sb-title clearfix">
                                            <h3>
                                                Danh mục tin tức
                                            </h3>
                                        </div>
                                        <ul class="no-bullets">

                                        </ul>
                                    </div>

                                    <div class="all-tags">
                                        <div class="blog-sb-title clearfix">
                                            <h3>
                                                Từ khóa
                                            </h3>
                                        </div>
                                        <div class="all-tags-wrapper clearfix">

                                        </div>
                                    </div>


                                    <div class="grid gallery-wrapper mg-left-15">


                                        <div
                                            class=" grid__item large--one-third medium--one-third small--one-third  pd-left15">
                                            <div class="gallery-item">

                                                <a href="//theme.hstatic.net/1000345738/1000619099/14/ft_1.png?v=399"
                                                   data-fancybox="home-gallery-images" data-caption="gallery 1">
                                                    <img
                                                        src="//theme.hstatic.net/1000345738/1000619099/14/ft_1.png?v=399"
                                                        alt="gallery 1">
                                                    <div class="overlay">

                                                    </div>
                                                </a>
                                            </div>
                                        </div>


                                        <div
                                            class=" grid__item large--one-third medium--one-third small--one-third  pd-left15">
                                            <div class="gallery-item">

                                                <a href="//theme.hstatic.net/1000345738/1000619099/14/ft_2.png?v=399"
                                                   data-fancybox="home-gallery-images" data-caption="gallery 2">
                                                    <img
                                                        src="//theme.hstatic.net/1000345738/1000619099/14/ft_2.png?v=399"
                                                        alt="gallery 2">
                                                    <div class="overlay">

                                                    </div>
                                                </a>
                                            </div>
                                        </div>


                                        <div
                                            class=" grid__item large--one-third medium--one-third small--one-third  pd-left15">
                                            <div class="gallery-item">

                                                <a href="//theme.hstatic.net/1000345738/1000619099/14/ft_3.png?v=399"
                                                   data-fancybox="home-gallery-images" data-caption="gallery 3">
                                                    <img
                                                        src="//theme.hstatic.net/1000345738/1000619099/14/ft_3.png?v=399"
                                                        alt="gallery 3">
                                                    <div class="overlay">

                                                    </div>
                                                </a>
                                            </div>
                                        </div>


                                        <div
                                            class=" grid__item large--one-third medium--one-third small--one-third  pd-left15">
                                            <div class="gallery-item">

                                                <a href="//theme.hstatic.net/1000345738/1000619099/14/ft_4.png?v=399"
                                                   data-fancybox="home-gallery-images" data-caption="gallery 4">
                                                    <img
                                                        src="//theme.hstatic.net/1000345738/1000619099/14/ft_4.png?v=399"
                                                        alt="gallery 4">
                                                    <div class="overlay">

                                                    </div>
                                                </a>
                                            </div>
                                        </div>


                                        <div
                                            class=" grid__item large--one-third medium--one-third small--one-third  pd-left15">
                                            <div class="gallery-item">

                                                <a href="//theme.hstatic.net/1000345738/1000619099/14/ft_5.png?v=399"
                                                   data-fancybox="home-gallery-images" data-caption="gallery 5">
                                                    <img
                                                        src="//theme.hstatic.net/1000345738/1000619099/14/ft_5.png?v=399"
                                                        alt="gallery 5">
                                                    <div class="overlay">

                                                    </div>
                                                </a>
                                            </div>
                                        </div>


                                        <div
                                            class=" grid__item large--one-third medium--one-third small--one-third  pd-left15">
                                            <div class="gallery-item">

                                                <a href="//theme.hstatic.net/1000345738/1000619099/14/ft_6.png?v=399"
                                                   data-fancybox="home-gallery-images" data-caption="gallery 6">
                                                    <img
                                                        src="//theme.hstatic.net/1000345738/1000619099/14/ft_6.png?v=399"
                                                        alt="gallery 6">
                                                    <div class="overlay">

                                                    </div>
                                                </a>
                                            </div>
                                        </div>


                                    </div>


                                    <div class="fb-page medium--hide small--hide fb_iframe_widget" data-href=""
                                         data-tabs="timeline" data-small-header="false"
                                         data-adapt-container-width="true" data-hide-cover="false"
                                         data-show-facepile="true" fb-xfbml-state="rendered"
                                         fb-iframe-plugin-query="adapt_container_width=true&amp;app_id=&amp;container_width=321&amp;hide_cover=false&amp;href=https%3A%2F%2Fwww.facebook.com%2FAmbio.vn&amp;locale=vi_VN&amp;sdk=joey&amp;show_facepile=true&amp;small_header=false&amp;tabs=timeline">
                                        <span style="vertical-align: bottom; width: 321px; height: 500px;"><iframe
                                                name="fe11e0e3986bc" width="1000px" height="1000px"
                                                data-testid="fb:page Facebook Social Plugin"
                                                title="fb:page Facebook Social Plugin" frameborder="0"
                                                allowtransparency="true" allowfullscreen="true" scrolling="no"
                                                allow="encrypted-media"
                                                src="https://www.facebook.com/v2.11/plugins/page.php?adapt_container_width=true&amp;app_id=&amp;channel=https%3A%2F%2Fstaticxx.facebook.com%2Fx%2Fconnect%2Fxd_arbiter%2F%3Fversion%3D46%23cb%3Df2a5ec70efcbc6%26domain%3Dshop.ambio.vn%26is_canvas%3Dfalse%26origin%3Dhttps%253A%252F%252Fshop.ambio.vn%252Ff257f82f88dcc0c%26relation%3Dparent.parent&amp;container_width=321&amp;hide_cover=false&amp;href=https%3A%2F%2Fwww.facebook.com%2FAmbio.vn&amp;locale=vi_VN&amp;sdk=joey&amp;show_facepile=true&amp;small_header=false&amp;tabs=timeline"
                                                style="border: none; visibility: visible; width: 321px; height: 500px;"
                                                class=""></iframe></span></div>

                                </div>

                            </div>

                        </div>
                    </div>
                </div>
            </section>
        </main>
    </div>
@stop
