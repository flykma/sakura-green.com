@extends('frontend.Layout.master')
@section('title','Thông tin giỏ hàng')
@section('content')
    <div id="PageContainer" class="is-moved-by-drawer">
        <main class="main-content" role="main">
            <div id="page-wrapper">
                <div class="wrapper">
                    <div class="inner">
                        <h1>{!! trans('messages.cart') !!}</h1>
                        @if(session('cart'))

                            <table id="cart" class="table table-hover table-condensed">
                                <thead>
                                <tr>
                                    <th style="width:50%">{!! trans('messages.product') !!}</th>
                                    <th style="width:10%">{!! trans('messages.price') !!}</th>
                                    <th style="width:8%">{!! trans('messages.Quantyli') !!}</th>
                                    <th style="width:22%" class="text-center">{!! trans('messages.money') !!}</th>
                                    <th style="width:22%" class="text-center">{!! trans('messages.action') !!}</th>

                                </tr>
                                </thead>
                                <tbody>
                                @php $total = 0 @endphp

                                @foreach(session('cart') as $id => $details)
                                    @php $total += $details['price'] * $details['quanlity'] @endphp
                                    <tr data-id="{{ $id }}">
                                        <td data-th="Product">
                                            <div class="row">
                                                <div class="col-sm-3 hidden-xs"><img
                                                        src="{{ asset('uploads/product/'.$details['image']) }}"
                                                        width="100" height="100" class="img-responsive"/></div>
                                                <div class="col-sm-9">
                                                    <h4 class="nomargin">{{ $details['name'] }}</h4>
                                                </div>
                                            </div>
                                        </td>
                                        <td data-th="Price">{{ number_format($details['price'],0) }} đ</td>
                                        <td data-th="Quanlity">
                                            <input type="number" value="{{ $details['quanlity'] }}"
                                                   class="form-control quanlity update-cart"/>
                                        </td>
                                        <td data-th="Subtotal" class="text-center" class="subtotal">
                                            {{ number_format($details['price'] * $details['quanlity'],0) }} đ</td>
                                        <td>
                                            <button class="btn btn-danger btn-sm remove-from-cart">Xóa</button>
                                        </td>

                                    </tr>
                                @endforeach

                                </tbody>
                            </table>

                        <div class="grid cart__row">

                            <div class="grid__item two-thirds small--one-whole">

                            </div>

                            <div class="grid__item text-right one-third small--one-whole">
                                <p>
                                    <span class="cart__subtotal-title">{!! trans('messages.total') !!}</span>
                                    <span class="h3 cart__subtotal">{!! number_format($total,0) !!}₫</span>
                                </p>

                                <p><em>{!! trans('messages.shipping') !!}</em></p>
                                <a href="{!! route('checkcart') !!}" type="submit" name="checkout" class="btn">{!! trans('messages.purchase') !!}</a>
                            </div>
                        </div>
                        @else
                            <h1>Hiện tại không có sản phẩm nào trong giỏ hàng</h1>
                        @endif

                    </div>
                </div>
            </div>
        </main>
    </div>
@stop
@section('script')

        <script>
        $(".update-cart").change(function (e) {
            e.preventDefault();

            let ele = $(this);
            let id =ele.parents("tr").attr("data-id");
            let quanlity = ele.parents("tr").find(".quanlity").val();
            $.ajax({
                url: '{{ route('updatecart') }}',
                method: "post",
                data: {
                    _token: '{{ csrf_token() }}',
                    id: id,
                    quanlity: quanlity,
                },
                success: function (response) {
                    toastr.success("Cập nhật thành công !");
                    window.location.reload();
                }
            });
        });

        $(".remove-from-cart").click(function (e) {
            e.preventDefault();

            var ele = $(this);
            let id = ele.parents("tr").attr("data-id");
            let url = '{!! route('removecart','id') !!}';
            let link = url.replace('id', id);
            if (confirm("Are you sure want to remove?")) {
                $.ajax({
                    url: link,
                    method: 'GET',
                    success: function (response) {
                        window.location.reload();
                        toastr.success("Xóa thành công !");
                    }
                });
            }
        });
    </script>
@stop
