@extends('frontend.Layout.master')
@section('title','Tiến hành mua hàng')
@section('style')
    <link rel="stylesheet" href="{!! asset('frontend/css/woocommerce-layout.css') !!}">
@stop
@section('content')
    <div id="PageContainer" class="is-moved-by-drawer">
        <main class="main-content" role="main">
            <div id="page-wrapper">
                <div class="wrapper">
                    <div class="inner">
                        @php $total = 0 @endphp
                        @if(session('cart'))
                            <h1>Thông tin thanh toán</h1>
                            <form method="post" class="form-group"
                                  action="{!! route('order') !!}" enctype="multipart/form-data"
                                  novalidate="novalidate">
                                <div
                                    class="wrap-check-oup-page uk-child-width-1-1 uk-child-width-1-2@s uk-grid-small uk-grid"
                                    uk-grid="">
                                    <div class="uk-first-column">


                                        <div class="col2-set" id="customer_details">
                                            <div class="col-1">
                                                <div class="woocommerce-billing-fields">

                                                    <h3>Thông tin thanh toán</h3>
                                                    <label for="">Họ và tên *</label>

                                                    <div class="woocommerce-billing-fields__field-wrapper">
                                                        <p class="form-row uk-width-1-2 validate-required"
                                                           id="billing_last_name_field" data-priority="1"><label
                                                                for="billing_last_name" class="screen-reader-text">Họ
                                                                tên a&nbsp;<abbr
                                                                    class="required"
                                                                    title="bắt buộc">*</abbr></label><span
                                                                class="woocommerce-input-wrapper"><input type="text"
                                                                                                         class="input-text form-control"
                                                                                                         name="billing_last_name"
                                                                                                         id="billing_last_name"
                                                                                                         placeholder=""
                                                                                                         value=""></span>
                                                        </p>
                                                        <p class="form-row form-row-wide validate-required validate-phone"
                                                           id="billing_phone_field" data-priority="20"><label
                                                                for="billing_phone" class="">Số điện thoại&nbsp;<abbr
                                                                    class="required"
                                                                    title="bắt buộc">*</abbr></label><span
                                                                class="woocommerce-input-wrapper"><input type="tel"
                                                                                                         class="input-text form-control"
                                                                                                         name="billing_phone"
                                                                                                         id="billing_phone"
                                                                                                         placeholder=""
                                                                                                         value=""
                                                                                                         autocomplete="tel"></span>
                                                        </p>
                                                        <p class="form-row form-row-wide validate-email"
                                                           id="billing_email_field" data-priority="21"><label
                                                                for="billing_email" class="">Địa chỉ email&nbsp;<span
                                                                    class="optional">(tuỳ chọn)</span></label><span
                                                                class="woocommerce-input-wrapper"><input type="email"
                                                                                                         class="form-control"
                                                                                                         name="billing_email"
                                                                                                         id="billing_email"
                                                                                                         placeholder=""
                                                                                                         value=""
                                                                                                         autocomplete="email username"></span>
                                                        </p>


                                                        <p>
                                                            <label for="">Chọn tỉnh</label>
                                                            <select name="province" id="province" class="form-control">
                                                            </select>
                                                        </p>
                                                        <p>
                                                            <label for="">Chọn huyện</label>
                                                            <select name="district" id="district" class="form-control">
                                                                <option value="" selected>------</option>
                                                            </select>
                                                        </p>
                                                        <p>
                                                            <label for="">Chọn xã/ phường</label>
                                                            <select name="ward" id="ward" class="form-control">
                                                                <option value="" selected>------</option>
                                                            </select>
                                                        </p>
                                                        <p class="form-row form-row-wide" id="billing_address_1_field"
                                                           data-priority="60"><label for="billing_address_1" class="">Địa
                                                                chỉ&nbsp;<span
                                                                    class="optional">(tuỳ chọn)</span></label><span
                                                                class="woocommerce-input-wrapper"><input type="text"
                                                                                                         class="input-text form-control"
                                                                                                         name="address_4"
                                                                                                         id="billing_address_1"
                                                                                                         placeholder="Ví dụ: Số 20, ngõ 90"
                                                                                                         value=""
                                                                                                         autocomplete="address-line1"></span>
                                                        </p></div>

                                                </div>


                                                <div class="woocommerce-shipping-fields">
                                                </div>
                                                <div class="woocommerce-additional-fields">


                                                    <h3>Thông tin bổ sung</h3>


                                                    <div class="woocommerce-additional-fields__field-wrapper">
                                                        <p class="form-row notes" id="order_comments_field"
                                                           data-priority=""><label for="order_comments" class="">Ghi chú
                                                                đơn
                                                                hàng&nbsp;<span
                                                                    class="optional">(tuỳ chọn)</span></label><span
                                                                class="woocommerce-input-wrapper"><textarea
                                                                    name="order_comments"
                                                                    class="input-text form-control"
                                                                    id="order_comments"
                                                                    placeholder="Ghi chú về đơn hàng, ví dụ: thời gian hay chỉ dẫn địa điểm giao hàng chi tiết hơn."
                                                                    rows="2" cols="5"></textarea></span></p></div>


                                                </div>
                                            </div>

                                        </div>


                                    </div>
                                    <div>
                                        <div class="col-2">

                                            <h3 id="order_review_heading">Đơn hàng của bạn</h3>


                                            <div id="order_review" class="woocommerce-checkout-review-order">


                                                <table id="cart" class="table table-hover table-condensed">
                                                    <thead>
                                                    <tr>
                                                        <th style="width:50%">Sản phẩm</th>
                                                        <th style="width:10%">Giá</th>
                                                        <th style="width:8%">Số lượng</th>
                                                        <th style="width:22%" class="text-center">Thành tiền</th>


                                                    </tr>
                                                    </thead>
                                                    <tbody>


                                                    @foreach(session('cart') as $id => $details)
                                                        @php $total += $details['price'] * $details['quanlity'] @endphp
                                                        <tr data-id="{{ $id }}">
                                                            <td data-th="Product">
                                                                <div class="row">
                                                                    <div class="col-sm-9">
                                                                        <h4 class="nomargin">{{ $details['name'] }}</h4>
                                                                    </div>
                                                                </div>
                                                            </td>
                                                            <td data-th="Price">{{ number_format($details['price'],0) }}
                                                                đ
                                                            </td>
                                                            <td data-th="Quanlity">
                                                                {{ $details['quanlity'] }}
                                                            </td>
                                                            <td data-th="Subtotal" class="text-center" class="subtotal">
                                                                {{ number_format($details['price'] * $details['quanlity'],0) }}
                                                                đ
                                                            </td>

                                                        </tr>
                                                    @endforeach

                                                    </tbody>
                                                </table>


                                                <div id="payment" class="woocommerce-checkout-payment">
                                                    <ul class="wc_payment_methods payment_methods methods">
                                                        <li class="wc_payment_method payment_method_cod">
                                                            <input id="payment_method_cod" type="radio"
                                                                   class="input-radio uk-checkbox" name="payment_method"
                                                                   value="cod" checked="checked"
                                                                   data-order_button_text="">

                                                            <label for="payment_method_cod">
                                                                Nhận hàng và thanh toán tiền tại nhà (COD) </label>
                                                            <div class="payment_box payment_method_cod">
                                                                <p>Đơn hàng này chưa bao gồm phí Ship<br>
                                                                    Phí ship giao động từ 35k-50k (tùy theo trọng lượng)
                                                                </p>
                                                            </div>
                                                        </li>

                                                    </ul>
                                                    <div class="form-row place-order">
                                                        <p style="text-align: right;
    font-size: 27px;">
                                                            <span class="cart__subtotal-title">Tổng tiền</span>
                                                            <span class="h3 cart__subtotal">{!! number_format($total,0) !!}₫</span>
                                                        </p>


                                                        <div
                                                            class="wrap-btn-payment uk-child-width-1-2@s uk-child-width-1-1 uk-grid-small uk-grid"
                                                            uk-grid="">
                                                            <div class="uk-first-column">

                                                            </div>
                                                            <div style="text-align: right">

                                                                <button type="submit" class="btn btn-primary"
                                                                        name="woocommerce_checkout_place_order"
                                                                        id="place_order" value="Đặt hàng"
                                                                        data-value="Đặt hàng">Tiến hành mua hàng
                                                                </button>
                                                            </div>
                                                        </div>

                                                    </div>

                                                </div>

                                            </div>

                                        </div>
                                    </div>


                            </form>
                        @else
                            <h1>Không thể mua hàng khi giỏ hàng trống</h1>
                        @endif


                    </div>
                </div>
            </div>
        </main>
    </div>
@stop

@section('script')
    <script>
        $(window).load(function (e) {
            e.preventDefault();
            let province = document.getElementById('province');
            let url = '{!! route('province') !!}';
            $.ajax({
                url: url,
                method: 'GET',
                success: function (response) {
                    let str = "<option selected>Chọn tỉnh thành</option>";

                    for (let i = 0; i < response.length; i++) {
                        str = str + '<option class="provinceId" data-id="' + response[i]["id"] + '">' + response[i]["_name"] + '</option>';
                    }
                    province.innerHTML = str;
                }
            });
        });
        $('#province').change(function (e) {
            e.preventDefault();
            let district = document.getElementById('district');
            let id = $('#province option:selected').data('id');
            let url = '{!! route('district','id') !!}';
            let link = url.replace('id', id);
            $.ajax({
                url: link,
                method: 'GET',
                dataType: 'json',
                success: function (data) {
                    console.log(data);
                    let str = "<option selected>Chọn quận huyện</option>";
                    for (let i = 0; i < data.length; i++) {
                        str = str + '<option class="provinceId" data-id="' + data[i]["id"] + '">' + data[i]["_name"] + '</option>';
                    }
                    district.innerHTML = str;
                }
            });

        });

        $('#district').change(function (e) {
            e.preventDefault();
            let ward = document.getElementById('ward');
            let id = $('#district option:selected').data('id');
            let url = '{!! route('ward','id') !!}';
            let link = url.replace('id', id);
            $.ajax({
                url: link,
                method: 'GET',
                dataType: 'json',
                success: function (data) {
                    console.log(data);
                    let str = "<option selected>Chọn xã phường</option>";
                    for (let i = 0; i < data.length; i++) {
                        str = str + '<option class="provinceId" data-id="' + data[i]["id"] + '">' + data[i]["_name"] + '</option>';
                    }
                    ward.innerHTML = str;
                }
            });

        });


    </script>
@stop
