@extends('frontend.Layout.master')
@section('title','Danh sách các bài viết tin tức')
@section('content')
    <section id="blog-wrapper">
        <div class="wrapper">
            <div class="inner">
                <div class="grid">

                    <div class="grid__item large--seven-tenths medium--one-whole small--one-whole">
                        <div class="blog-content">

                            <div class="blog-content-wrapper">
                                <div class="blog-head">
                                    <div class="blog-title">
                                        <h1>{!! $catnew->categorynew !!}</h1>
                                    </div>
                                </div>
                                <div class="blog-body">
                                    <div class="grid-uniform">

                                        @if($new)
                                            @foreach($new as $itemnew)
                                                <?php $catnew = \App\CategoryNew::find($itemnew->catnew_id); ?>
                                                <div
                                                    class="grid__item large--one-half medium--one-half small--one-whole">
                                                    <div class="article-item">
                                                        <div class="article-img">
                                                            <a href="">
                                                                <img
                                                                    src="{!! asset('uploads/'.$itemnew->image) !!}"
                                                                    alt="{!! $itemnew->nametitle !!}">
                                                                <div class="overlay">
                                                                </div>
                                                            </a>
                                                        </div>
                                                        <div class="article-info">
                                                            <a href="{!! route('detailnews',[$catnew->slug,$itemnew->slug]) !!}"
                                                               class="article-title">
                                                                {!! $itemnew->nametitle !!}
                                                            </a>
                                                            <div class="article-author">
                                                                <span> Sakura Green</span>
                                                            </div>
                                                            <div class="article-desc">

                                                                {!! substr($itemnew->description,30) !!}

                                                            </div>
                                                            <a href="{!! route('detailnews',[$catnew->slug,$itemnew->slug]) !!}"
                                                               class="article-readmore">Xem thêm
                                                                <svg
                                                                    class="svg-inline--fa fa-long-arrow-alt-right fa-w-14"
                                                                    aria-hidden="true" data-prefix="fas"
                                                                    data-icon="long-arrow-alt-right" role="img"
                                                                    xmlns="http://www.w3.org/2000/svg"
                                                                    viewBox="0 0 448 512"
                                                                    data-fa-i2svg="">
                                                                    <path fill="currentColor"
                                                                          d="M313.941 216H12c-6.627 0-12 5.373-12 12v56c0 6.627 5.373 12 12 12h301.941v46.059c0 21.382 25.851 32.09 40.971 16.971l86.059-86.059c9.373-9.373 9.373-24.569 0-33.941l-86.059-86.059c-15.119-15.119-40.971-4.411-40.971 16.971V216z"></path>
                                                                </svg>
                                                                <!-- <i class="fas fa-long-arrow-alt-right"></i> --></a>
                                                            <div class="article-info-relate">
                                                                <div class="article-published-at">
                                                                    <svg class="svg-inline--fa fa-calendar fa-w-14"
                                                                         aria-hidden="true" data-prefix="fa"
                                                                         data-icon="calendar" role="img"
                                                                         xmlns="http://www.w3.org/2000/svg"
                                                                         viewBox="0 0 448 512" data-fa-i2svg="">
                                                                        <path fill="currentColor"
                                                                              d="M12 192h424c6.6 0 12 5.4 12 12v260c0 26.5-21.5 48-48 48H48c-26.5 0-48-21.5-48-48V204c0-6.6 5.4-12 12-12zm436-44v-36c0-26.5-21.5-48-48-48h-48V12c0-6.6-5.4-12-12-12h-40c-6.6 0-12 5.4-12 12v52H160V12c0-6.6-5.4-12-12-12h-40c-6.6 0-12 5.4-12 12v52H48C21.5 64 0 85.5 0 112v36c0 6.6 5.4 12 12 12h424c6.6 0 12-5.4 12-12z"></path>
                                                                    </svg><!-- <i class="fa fa-calendar"></i> -->
                                                                    {!! $itemnew->created_at !!}
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>


                                                </div>
                                            @endforeach
                                        @endif
                                    </div>


                                    <div id="pagination-" class="text-center clear-left">
                                        <div class="pagination-custom">

                                            {!! $new->render() !!}
                                        </div>
                                    </div>


                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="grid__item large--three-tenths medium--one-whole small--one-whole">
                        <div class="blog-sidebar">

                            <div class="list-categories">
                                <div class="blog-sb-title clearfix">
                                    <h3>
                                        Danh mục tin tức
                                    </h3>
                                </div>
                                <ul class="no-bullets">

                                </ul>
                            </div>
                            <div id="fb-root"></div>
                            <script async defer crossorigin="anonymous" src="https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v11.0" nonce="U5wb7NyZ"></script>
                            <div class="fb-page" data-href="https://www.facebook.com/SakuraGreen.cnsh" data-tabs="timeline" data-width="" data-height="" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/SakuraGreen.cnsh" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/SakuraGreen.cnsh">SakuraGreen</a></blockquote></div>
                    </div>


                </div>

            </div>

        </div>

    </section>
@stop
