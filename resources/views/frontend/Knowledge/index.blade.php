@extends('frontend.Layout.master')
@section('title','Kiến thức')
@section('content')
    <div id="PageContainer" class="is-moved-by-drawer">
        <main class="main-content" role="main">
            <section id="page-wrapper">
                <div class="wrapper">
                    <div class="inner">
                        <div class="grid">

                            <div class="grid__item large--one-whole">

                                <h1>{!! $catknowledge->name !!}</h1>
                                <div class="rte">
                                    {!! $knowledge->contents !!}
                                </div>

                            </div>

                        </div>
                    </div>
                </div>
            </section>

        </main>
    </div>
@stop
