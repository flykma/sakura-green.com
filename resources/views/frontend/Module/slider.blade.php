<section class="section-bannerhome">
    <div class="  listTabBanerHome" id="divBannerHome">
        <?php $slider = \App\Slider::where('status', 1)->get() ?>
        @if(isset($slider))
            @foreach($slider as $itemslider)

                <div class="itemBanner" style="display: inline-block; cursor: pointer;">
                    <!-- MAIN IMAGE -->
                    <a href='#'>
                        <img
                            src="{!! asset('uploads/'.$itemslider->image) !!}"
                            alt="image" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat"
                            class="rev-slidebg">
                    </a>
                </div>
            @endforeach
        @endif


    </div>
</section>
<!-- end danhMucNEW -->
<script type="text/javascript">

    $(document).ready(function () {
        $('#divBannerHome, #danhMuc1').owlCarousel({
            loop: true,
            nav: true,
            navText: true,
            dots: true,
            autoplay: true,
            autoplayTimeout: 2000,
            autoplaySpeed: 2000,
            lazyLoad: true,
            responsive: {
                0: {
                    items: 1
                },
                600: {
                    items: 1
                },
                1000: {
                    items: 1
                },
                1001: {
                    items: 1
                }
            }
        })
    });

</script>
