<?php $setting = \App\Setting::find(1); ?>

<header>
    <div class="top_header onPC">
        <div class="container">
            <div class="pull-left header_left">
                <ul>
                    <li><a href="{!! route('home') !!}"> <span style="font-size: 36px;
    font-family: fantasy;
    font-weight: 600;">{!! $setting->company?$setting->company : " " !!}</span></a></li>
{{--                    <li class="onPC"><i class="fa fa-envelope-o s_color" aria-hidden="true"></i><a href="#">{!! $setting->address?$setting->address:" " !!}</a></li>--}}
                </ul>
            </div>

            <div class="pull-right header_right">
                <div class="state mobilePhoneHeader" id="value1">
                    <ul>

                        <li>
                            <a href="tel:0978 007 172" style="font-size:20px">{!! chunk_split($setting->phone, 3, ' ');!!}</a></li>

                    </ul>
                </div>


            </div>
        </div> <!-- End of .container -->
    </div> <!-- End of .top_header -->
    <div class="top_header onMobile">
        <div class="container">

        </div> <!-- End of .container -->
    </div> <!-- End of .top_header -->

    <div class="bottom_header onPC	">
        <div class="container">
            <div class="row">
{{--                <div class="col-md-4 col-sm-12 col-xs-12">--}}
{{--                    <div class="search-box">--}}
{{--                        <form action="#" onsubmit="searchProduct('searchKw');return false;" class="clearfix">--}}
{{--                            <input type="text" id="searchKw" placeholder="Tìm kiếm sản phẩm...">--}}
{{--                            <button><i class="fa fa-search"></i></button>--}}
{{--                        </form>--}}
{{--                    </div>--}}
{{--                </div>--}}
                <div class="col-md-3 col-sm-5 col-xs-12 logo-responsive">
                    <div class="logo-area">
                        <a href="{!! route('home') !!}" class="pull-left logo">
                            <img src="{!! asset('uploads/'.$setting->logo) !!}" alt="logo">
                            <br/>

                        </a>

                    </div>
                </div>
                <div class="col-md-9 col-sm-5 col-xs-12 logo-responsive">
                    <span class="logoText pcshow ">{!! $setting->address?$setting->address : " " !!}</span>
                </div>


            </div>

        </div>
    </div> <!-- End of .bottom_header -->
</header>
