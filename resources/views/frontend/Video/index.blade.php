@extends('frontend.Layout.master')
@section('title','Danh mục VIDEO')
@section('content')
    <div id="PageContainer" class="is-moved-by-drawer">
        <main class="main-content" role="main">


            <section id="blog-wrapper">
                <div class="wrapper">
                    <div class="inner">
                        <div class="grid">

                            <div class="grid__item large--seven-tenths medium--one-whole small--one-whole">
                                <div class="blog-content">

                                    <div class="blog-content-wrapper">
                                        <div class="blog-head">
                                            <div class="blog-title">
                                                <h1>Video</h1>
                                            </div>
                                        </div>
                                        <div class="blog-body">
                                            <div class="grid-uniform">
                                                @if($video)
                                                    @foreach($video as $itemvideo)

                                                        <div
                                                            class="grid__item large--one-half medium--one-half small--one-whole">
                                                            <div class="article-item">
                                                                <div class="embed-responsive embed-responsive-4by3">
                                                                    <iframe style="width: 100%; height: 100%"
                                                                            class="embed-responsive-item"
                                                                            src="https://www.youtube.com/embed/{!! $itemvideo->link !!}"
                                                                            title="YouTube video player" frameborder="0"
                                                                            allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                                                                            allowfullscreen></iframe>
                                                                </div>

                                                            </div>

                                                        </div>
                                                    @endforeach
                                                @endif

                                            </div>


                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="grid__item large--three-tenths medium--one-whole small--one-whole">
                                <div class="blog-sidebar">


                                    <div class="all-tags">
                                        <div class="blog-sb-title clearfix">
                                            <h3>
                                                PHOTO
                                            </h3>
                                        </div>
                                        <div class="all-tags-wrapper clearfix">

                                        </div>
                                    </div>


                                    <div class="grid gallery-wrapper mg-left-15">


                                        <div
                                            class=" grid__item large--one-third medium--one-third small--one-third  pd-left15">
                                            <div class="gallery-item">

                                                <a href="//theme.hstatic.net/1000345738/1000619099/14/ft_1.png?v=399"
                                                   data-fancybox="home-gallery-images" data-caption="gallery 1">
                                                    <img
                                                        src="//theme.hstatic.net/1000345738/1000619099/14/ft_1.png?v=399"
                                                        alt="gallery 1">
                                                    <div class="overlay">

                                                    </div>
                                                </a>
                                            </div>
                                        </div>


                                        <div
                                            class=" grid__item large--one-third medium--one-third small--one-third  pd-left15">
                                            <div class="gallery-item">

                                                <a href="//theme.hstatic.net/1000345738/1000619099/14/ft_2.png?v=399"
                                                   data-fancybox="home-gallery-images" data-caption="gallery 2">
                                                    <img
                                                        src="//theme.hstatic.net/1000345738/1000619099/14/ft_2.png?v=399"
                                                        alt="gallery 2">
                                                    <div class="overlay">

                                                    </div>
                                                </a>
                                            </div>
                                        </div>


                                        <div
                                            class=" grid__item large--one-third medium--one-third small--one-third  pd-left15">
                                            <div class="gallery-item">

                                                <a href="//theme.hstatic.net/1000345738/1000619099/14/ft_3.png?v=399"
                                                   data-fancybox="home-gallery-images" data-caption="gallery 3">
                                                    <img
                                                        src="//theme.hstatic.net/1000345738/1000619099/14/ft_3.png?v=399"
                                                        alt="gallery 3">
                                                    <div class="overlay">

                                                    </div>
                                                </a>
                                            </div>
                                        </div>


                                        <div
                                            class=" grid__item large--one-third medium--one-third small--one-third  pd-left15">
                                            <div class="gallery-item">

                                                <a href="//theme.hstatic.net/1000345738/1000619099/14/ft_4.png?v=399"
                                                   data-fancybox="home-gallery-images" data-caption="gallery 4">
                                                    <img
                                                        src="//theme.hstatic.net/1000345738/1000619099/14/ft_4.png?v=399"
                                                        alt="gallery 4">
                                                    <div class="overlay">

                                                    </div>
                                                </a>
                                            </div>
                                        </div>


                                        <div
                                            class=" grid__item large--one-third medium--one-third small--one-third  pd-left15">
                                            <div class="gallery-item">

                                                <a href="//theme.hstatic.net/1000345738/1000619099/14/ft_5.png?v=399"
                                                   data-fancybox="home-gallery-images" data-caption="gallery 5">
                                                    <img
                                                        src="//theme.hstatic.net/1000345738/1000619099/14/ft_5.png?v=399"
                                                        alt="gallery 5">
                                                    <div class="overlay">

                                                    </div>
                                                </a>
                                            </div>
                                        </div>


                                        <div
                                            class=" grid__item large--one-third medium--one-third small--one-third  pd-left15">
                                            <div class="gallery-item">

                                                <a href="//theme.hstatic.net/1000345738/1000619099/14/ft_6.png?v=399"
                                                   data-fancybox="home-gallery-images" data-caption="gallery 6">
                                                    <img
                                                        src="//theme.hstatic.net/1000345738/1000619099/14/ft_6.png?v=399"
                                                        alt="gallery 6">
                                                    <div class="overlay">

                                                    </div>
                                                </a>
                                            </div>
                                        </div>


                                    </div>


                                </div>

                            </div>

                        </div>
                    </div>
                </div>
            </section>


        </main>
    </div>
@stop
