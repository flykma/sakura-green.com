<header id="header" class="">
    <div class="hd-desktop medium--hide small--hide">
        <div id="header-fixed" class="tm-headerbar-top">
            <div class="uk-container uk-flex uk-flex-middle">
                <a href="{!! route('home') !!}" class="uk-logo" style="width:130px">
                    <img alt="công ty cổ phẩn Sakura green" src="{!! asset("frontend/images/logo.png") !!}"
                         srcset="{!! asset("frontend/images/logo.png") !!} 81w" sizes="(min-width: 81px) 81px"
                         data-width="81" data-height="81"></a>
                <div class="uk-margin-auto-left">
                    <div class="uk-grid-medium uk-child-width-auto uk-flex-middle uk-grid uk-grid">
                        <div class="uk-first-column">
                            <div class="uk-panel widget-search" id="widget-search-4">
                                <form method="get" class="woocommerce-product-search" action="{!! route('search') !!}"
                                      style="margin: auto">

                                    <div class="uk-inline uk-width-1-1">
                                    <span class="uk-form-icon uk-icon" uk-icon="icon: search">
                                       <svg width="20" height="20" viewBox="0 0 20 20"
                                            xmlns="http://www.w3.org/2000/svg" data-svg="search">
                                          <circle fill="none" stroke="#000" stroke-width="1.1" cx="9" cy="9"
                                                  r="7"></circle>
                                          <path fill="none" stroke="#000" stroke-width="1.1"
                                                d="M14,14 L18,18 L14,14 Z"></path>
                                       </svg>
                                    </span>
                                        <input type="search"
                                               class="search-field uk-input aa-input" placeholder="{!! trans('messages.search') !!}" value=""
                                               name="s" >
                                        <pre aria-hidden="true"
                                             style="position: absolute; visibility: hidden; white-space: pre; font-size: 14px; font-style: normal; font-variant: normal; font-weight: 400; word-spacing: 0; letter-spacing: normal; text-indent: 0; text-rendering: auto; text-transform: none;"></pre>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div>
                            <div class="uk-panel widget-custom-html" id="widget-custom_html-5">
                                <div class="textwidget custom-html-widget">
                                    <div id="header-short-code"
                                         class="uk-child-width-auto uk-flex-middle uk-grid-small uk-grid" uk-grid="">
                                        <div class="theodoidonhang uk-first-column">

                                        </div>
                                        <div class="signin-signup">
                                            <img src="{!! asset("frontend/images/profile.png") !!}" alt="">
                                            <div class="content">
                                                <span>{!! trans('messages.account') !!}</span><br>
                                                <span><a href="#modal-login" uk-toggle="">{!! trans('messages.login') !!}</a> | <a
                                                        href="#modal-register" uk-toggle="">{!! trans('messages.register') !!}</a></span>
                                            </div>
                                        </div>
                                        <div class="giohang-header">
                                            <a href="{!! route('cart') !!}"><img
                                                    src="https://vuonsinhthai.com.vn/wp-content/themes/vuonsinhthai/images/cart-mb-f.svg"></a>
                                            <div class="navbar-actions">
                                                <div class="uk-inline shopping-cart">
                                                    <a href="{!! route('cart') !!}"
                                                       class="dropdown-toggle cart-img cart-mini-mb">
                                                        <b id="countShoppingCart">{!! count((array) session('cart')) !!} </b>
                                                        <span>{!! trans('messages.cart') !!}</span>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="hottline-header uk-flex uk-flex-middle">
                                            <img src="{!! asset("frontend/images/telephone.png") !!}">
                                            <div class="content">
                                                <a href="tel:097.800.7172">097.800.7172</a>
                                            </div>
                                        </div>
                                        <div class="uk-flex uk-flex-middle form-group">
                                            <a href="{!! route('changelanguage','vi') !!}"><img src="{!! asset('frontend/images/flag_vn.png') !!}" alt="" style="margin-right: 5px"> </a>
                                            <a href="{!! route('changelanguage','en') !!}"><img src="{!! asset('frontend/images/flag_uk.png') !!}" alt=""></a>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="hd-center">
            <div class="wrapper">
                <div class="inner">
                    <div class="grid">
                        <div class="grid__item large--nine-twelfths text-right hd-menu">
                            <div class="hd-navbar">
                                <ul class="no-bullets">
                                    <li class=" dropdown">
                                        <a href="{!! route('home') !!}">
                                            <i class="fas fa-home"></i>
                                        </a>
                                    </li>
                                    <li class="dropdown">
                                        <a href="javascript:void(0)">
                                            Giới thiệu <i class="fas fa-chevron-down"></i>
                                        </a>

                                        <ul class="no-bullets dropdown-menu" style="    min-width: 236px !important;">
                                            <?php $intro = \App\CategoryIntroduct::where('status', 1)->get(); ?>
                                            @foreach($intro as $itemintro)
                                                <li>
                                                    <a href="{!! route('intro',$itemintro->slug) !!}">{!! $itemintro->name !!}</a>
                                                </li>
                                            @endforeach

                                        </ul>
                                    </li>
                                    <li class="dropdown">
                                        <a href="javascript:void(0)">
                                            Sản phẩm
                                            <i class="fas fa-chevron-down"></i>
                                        </a>
                                        <ul class="no-bullets dropdown-menu" style="    min-width: 236px !important;">
                                            <?php $catpro = \App\CategoryProduct::where('status', 1)->get(); ?>
                                            @foreach($catpro as $itemcatpro)
                                                <li>
                                                    <a href="{!! route('listproduct',$itemcatpro->slug) !!}">{!! $itemcatpro->categoryName !!}</a>
                                                </li>
                                            @endforeach
                                            <li>
                                                <a href="{!! route('catpro') !!}">Tất cả sản phẩm </a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class=" dropdown">
                                        <a href="javascript:void(0)">
                                            Tin tức
                                            <i class="fa fa-angle-down" aria-hidden="true"></i>
                                        </a>
                                        <ul class="no-bullets dropdown-menu">
                                            <?php $catnew = \App\CategoryNew::where('status', 1)->get(); ?>
                                            @foreach($catnew as $itemcatnew)
                                                <li>
                                                    <a href="{!! route('listnews',$itemcatnew->slug) !!}">{!! $itemcatnew->categorynew !!}</a>
                                                </li>
                                            @endforeach
                                        </ul>
                                    </li>
                                    <li class=" dropdown">
                                        <a href="javascript:void(0)">
                                            Kiến thức <i class="fa fa-angle-down" aria-hidden="true"></i>
                                        </a>
                                        <ul class="no-bullets dropdown-menu">
                                            <?php $catknowledge = \App\CategoryKnowledge::where('status', 1)->get(); ?>
                                            @foreach($catknowledge as $itemcatnew)
                                                <li>
                                                    <a href="{!! route('know',$itemcatnew->slug) !!}">{!! $itemcatnew->name !!}</a>
                                                </li>
                                            @endforeach
                                        </ul>
                                    </li>
                                    <li class="dropdown">
                                        <a href="javascript:void(0)">Gallery <i class="fa fa-angle-down"
                                                                                aria-hidden="true"></i></a>
                                        <ul class="no-bullets dropdown-menu">
{{--                                            <li>--}}
{{--                                                <a href="{!!  !!}">Hình ảnh</a>--}}
{{--                                            </li>--}}
                                            <li>
                                                <a href="{!! route('video') !!}">Video</a>
                                            </li>

                                        </ul>
                                    </li>

                                    <li class="dropdown">
                                        <a href="{!! route('contact')!!}">Liên hệ </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



    <div class=" mobile-header large--hide">
        <div class="grid">
            <div class="grid__item medium--one-third small--one-half">
                <div class="hd-logo text-left">
                    <a href="{!! route('home') !!}">
                        <img src="{!! asset("frontend/images/logo.png") !!}" alt="Sakura Green">
                    </a>
                </div>
            </div>
            <div
                class="grid__item large--two-twelfths push--large--eight-twelfths medium--two-thirds small--one-half clearfix text-right">
                <div class="hd-btnMenu">
                    <a href="javascript:void(0)" class="icon-fallback-text site-nav__link js-drawer-open-right"
                       aria-controls="NavDrawer" aria-expanded="false">
                        <span>Menu</span>
                        <i class="fa fa-bars" aria-hidden="true"></i>
                    </a>
                </div>
                <div class="desktop-cart-wrapper1">
                    <a href="{!! route('cart') !!}" class="hd-cart">
                        <span class="hd-cart-count">{!! count((array) session('cart')) !!} </span>
                        <i class="fa fa-shopping-bag" aria-hidden="true"></i>
                    </a>
                    <div class="quickview-cart">
                        <h3>
                            Giỏ hàng trống
                            <span class="btnCloseQVCart">
                                <i class="fa fa-times" aria-hidden="true"></i>
                           </span>
                        </h3>
                        <ul class="no-bullets">
                            <li>Bạn chưa có sản phẩm nào trong giỏ hàng!</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
