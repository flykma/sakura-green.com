<html lang="vi">
<head>
    <!-- Basic page needs ================================================== -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta property="og:locale" content="vi_VN">
    <!-- Title and description ================================================== -->
    <title>
        @yield('title')
    </title>
    <!-- CSS ================================================== -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    @yield('style')
    <link href='{!! asset("frontend/css/timber.css") !!}' rel='stylesheet' type='text/css' media='all'/>
    <link href='{!! asset("frontend/css/suplo-style.css") !!}' rel='stylesheet' type='text/css' media='all'/>
    <link href='{!! asset("frontend/css/theme.1.css/") !!}' rel='stylesheet' type='text/css' media='all'/>
    <link href='{!! asset("frontend/css/owl.carousel.css") !!}' rel='stylesheet' type='text/css' media='all'/>
    <link href='{!! asset("frontend/css/owl.theme.css") !!}' rel='stylesheet' type='text/css' media='all'/>
    <link href='{!! asset("frontend/css/owl.transitions.css") !!}' rel='stylesheet' type='text/css' media='all'/>
    <link href='{!! asset("frontend/css/slick.css") !!}' rel='stylesheet' type='text/css' media='all'/>
    <link href='{!! asset("frontend/css/slick-theme.css") !!}' rel='stylesheet' type='text/css' media='all'/>
    <link href='{!! asset("frontend/css/animate.css") !!}' rel='stylesheet' type='text/css' media='all'/>
    <link href='{!! asset("frontend/css/custom.css") !!}' rel='stylesheet' type='text/css' media='all'/>
    <link href='{!! asset("frontend/css/xzoom.css") !!}' rel='stylesheet' type='text/css' media='all'/>
    @toastr_css

    <style>
        .hd-center {
            border-bottom: 1px solid green;
        }
    </style>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <!-- fontawesome -->
    <script defer src="https://use.fontawesome.com/releases/v5.0.6/js/all.js"></script>
    <link rel="stylesheet" href="{!! asset("frontend/css/all.css") !!}">
    <script src='{!! asset("frontend/js/jquery.min.js") !!}' type='text/javascript'></script>
    <script src='{!! asset("frontend/js/modernizr.min.js") !!}' type='text/javascript'></script>
    <script src='{!! asset("frontend/js/owl.carousel.min.js") !!}' type='text/javascript'></script>
    <script src='{!! asset("frontend/js/slick.min.js") !!}' type='text/javascript'></script>
    <script src='{!! asset("frontend/js/wow.min.js") !!}' type='text/javascript'></script>
    <script src='{!! asset("frontend/js/xzoom.min.js") !!}' type='text/javascript'></script>
    <script src='{!! asset("frontend/js/setup.js") !!}' type='text/javascript'></script>
    <link href="https://fonts.googleapis.com/css?family=Merriweather:400,900,900i&display=swap" rel="stylesheet">

    <script>
        $(document).ready(function () {
            $('#main-slider').on('init', function (e, slick) {
                var $firstAnimatingElements = $('div.main-slide:first-child').find('[data-animation]');
                doAnimations($firstAnimatingElements);
            });
            $('#main-slider').on('beforeChange', function (e, slick, currentSlide, nextSlide) {
                var $animatingElements = $('div.main-slide[data-slick-index="' + nextSlide + '"]').find('[data-animation]');
                doAnimations($animatingElements);
            });
            $('#main-slider').slick({
                autoplay: true,
                arrows: false,
                autoplaySpeed: 10000,
                dots: true,
                fade: true
            });
            $('.home-featured-slider').slick({
                autoplay: true,
                arrows: true,
                autoplaySpeed: 10000,
                dots: false
            });
            $('.gallery-slider').slick({
                slidesToShow: 3,
                slidesToScroll: 1,
                autoplay: true,
                autoplaySpeed: 10000,
                dots: false,
                responsive: [
                    {
                        breakpoint: 1024,
                        settings: {
                            slidesToShow: 3,
                            slidesToScroll: 1,
                            infinite: true,
                        }
                    },
                    {
                        breakpoint: 769,
                        settings: {
                            slidesToShow: 2,
                            slidesToScroll: 1,
                            arrows: false,
                            dots: true,
                        }
                    },
                    {
                        breakpoint: 480,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1,
                            arrows: false,
                            dots: true,
                        }
                    }
                ]
            });
            $('.reviews-slider').slick({
                slidesToShow: 3,
                slidesToScroll: 1,
                //autoplay: true,
                autoplaySpeed: 10000,
                dots: false,
                responsive: [
                    {
                        breakpoint: 1024,
                        settings: {
                            slidesToShow: 3,
                            slidesToScroll: 3,
                            infinite: true,
                            dots: true
                        }
                    },
                    {
                        breakpoint: 769,
                        settings: {
                            slidesToShow: 2,
                            slidesToScroll: 2,
                            arrows: false,
                            dots: true,
                        }
                    },
                    {
                        breakpoint: 480,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1,
                            arrows: false,
                            dots: true,
                        }
                    }
                ]
            });

            function doAnimations(elements) {
                var animationEndEvents = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
                elements.each(function () {
                    var $this = $(this);
                    var $animationDelay = $this.data('delay');
                    var $animationType = 'animated ' + $this.data('animation');
                    $this.css({
                        'animation-delay': $animationDelay,
                        '-webkit-animation-delay': $animationDelay
                    });
                    $this.addClass($animationType).one(animationEndEvents, function () {
                        $this.removeClass($animationType);
                    });
                });
            }


            $("#related-product-slider").owlCarousel({
                items: 4,
                itemsDesktop: [1000, 4],
                itemsDesktopSmall: [900, 4],
                itemsTablet: [768, 3],
                itemsMobile: [480, 2],
                navigation: true,
                pagination: false,
                slideSpeed: 1000,
                paginationSpeed: 1000,
                navigationText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>', '<i class="fa fa-angle-right" aria-hidden="true"></i>']
            });
            $("#owl-blog-single-slider1").owlCarousel({
                items: 2,
                itemsDesktop: [1000, 2],
                itemsDesktopSmall: [900, 2],
                itemsTablet: [600, 1],
                itemsMobile: false,
                navigation: true,
                pagination: false,
                margin: 0,
                slideSpeed: 1000,
                paginationSpeed: 1000,
                navigationText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>', '<i class="fa fa-angle-right" aria-hidden="true"></i>']
            });
            $("#owl-blog-single-slider2").owlCarousel({
                items: 2,
                itemsDesktop: [1000, 2],
                itemsDesktopSmall: [900, 2],
                itemsTablet: [600, 1],
                itemsMobile: false,
                navigation: true,
                pagination: false,
                slideSpeed: 1000,
                paginationSpeed: 1000,
                navigationText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>', '<i class="fa fa-angle-right" aria-hidden="true"></i>']
            });

            $("#owl-blog-single-slider3").owlCarousel({
                items: 2,
                itemsDesktop: [1000, 2],
                itemsDesktopSmall: [900, 2],
                itemsTablet: [600, 1],
                itemsMobile: false,
                navigation: true,
                pagination: false,
                slideSpeed: 1000,
                paginationSpeed: 1000,
                navigationText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>', '<i class="fa fa-angle-right" aria-hidden="true"></i>']
            });

            $("#owl-blog-single-slider4").owlCarousel({
                items: 2,
                itemsDesktop: [1000, 2],
                itemsDesktopSmall: [900, 2],
                itemsTablet: [600, 1],
                itemsMobile: false,
                navigation: true,
                pagination: false,
                slideSpeed: 1000,
                paginationSpeed: 1000,
                navigationText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>', '<i class="fa fa-angle-right" aria-hidden="true"></i>']
            });

            $("#owl-blog-single-slider5").owlCarousel({
                items: 2,
                itemsDesktop: [1000, 2],
                itemsDesktopSmall: [900, 2],
                itemsTablet: [600, 1],
                itemsMobile: false,
                navigation: true,
                pagination: false,
                slideSpeed: 1000,
                paginationSpeed: 1000,
                navigationText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>', '<i class="fa fa-angle-right" aria-hidden="true"></i>']
            });

            $("#owl-blog-single-slider6").owlCarousel({
                items: 2,
                itemsDesktop: [1000, 2],
                itemsDesktopSmall: [900, 2],
                itemsTablet: [600, 1],
                itemsMobile: false,
                navigation: true,
                pagination: false,
                slideSpeed: 1000,
                paginationSpeed: 1000,
                navigationText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>', '<i class="fa fa-angle-right" aria-hidden="true"></i>']
            });

            $("#owl-blog-single-slider7").owlCarousel({
                items: 2,
                itemsDesktop: [1000, 2],
                itemsDesktopSmall: [900, 2],
                itemsTablet: [600, 1],
                itemsMobile: false,
                navigation: true,
                pagination: false,
                slideSpeed: 1000,
                paginationSpeed: 1000,
                navigationText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>', '<i class="fa fa-angle-right" aria-hidden="true"></i>']
            });

            $("#owl-blog-single-slider8").owlCarousel({
                items: 2,
                itemsDesktop: [1000, 2],
                itemsDesktopSmall: [900, 2],
                itemsTablet: [600, 1],
                itemsMobile: false,
                navigation: true,
                pagination: false,
                slideSpeed: 1000,
                paginationSpeed: 1000,
                navigationText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>', '<i class="fa fa-angle-right" aria-hidden="true"></i>']
            });

            $("#owl-blog-single-slider9").owlCarousel({
                items: 2,
                itemsDesktop: [1000, 2],
                itemsDesktopSmall: [900, 2],
                itemsTablet: [600, 1],
                itemsMobile: false,
                navigation: true,
                pagination: false,
                slideSpeed: 1000,
                paginationSpeed: 1000,
                navigationText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>', '<i class="fa fa-angle-right" aria-hidden="true"></i>']
            });

            $("#owl-blog-single-slider10").owlCarousel({
                items: 2,
                itemsDesktop: [1000, 2],
                itemsDesktopSmall: [900, 2],
                itemsTablet: [600, 1],
                itemsMobile: false,
                navigation: true,
                pagination: false,
                slideSpeed: 1000,
                paginationSpeed: 1000,
                navigationText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>', '<i class="fa fa-angle-right" aria-hidden="true"></i>']
            });
        });
    </script>

</head>
