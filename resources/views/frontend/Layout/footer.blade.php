@php $footer = App\Setting::find(1); @endphp
<footer id="footer" style="background: url('{!! asset('uploads/'.$footer->bg_footer) !!}') no-repeat center;">
    <div class="wrapper">
        <div class="inner">

            <div class="footer-logo text-center">
                {{--                <a href="/">--}}
                {{--                    <img src="{!! asset("frontend/images/ft_logo.png") !!}" alt="AmBio">--}}
                {{--                </a>--}}
                <div class="ft-social-network">
                    <a href="{!! $footer->facebook !!}" target="_blank" class="fb-icon"><i class="fab fa-facebook"></i></a>
                    <a href="{!! $footer->youtube !!}" target="_blank" class="yt-icon"><i class="fab fa-youtube" aria-hidden="true"></i></a>

                </div>
            </div>
            <div class="grid-uniform">
                <div class="grid__item large--one-quarter medium--one-half small--one-whole">
                    <div class="footer-info">
                        <h3 class="footer-title">Liên hệ</h3>
                        <ul class="no-bullets">
                            <li>
                                {!! $footer->company !!}
                            </li>
                            <li>
                                <i class="fa fa-map-marker" aria-hidden="true"></i>
                                <span>{!! $footer->address !!}</span>
                            </li>
                            <li>
                                <i class="fa fa-phone" aria-hidden="true"></i> <a href="tel:{!! $footer->phone !!}">
                                    0{!! number_format($footer->phone,0) !!}</a>
                            </li>
                            <li>
                                <i class="fa fa-envelope" aria-hidden="true"></i> <a
                                    href="mailto:{!! $footer->email !!}">{!! $footer->email !!}</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="grid__item large--one-quarter medium--one-half small--one--whole">
                    <div class="ft-menu">
                        <h3 class="footer-title">Giới thiệu </h3>
                        @php $intro = App\CategoryIntroduct::where('status',1)->get(); @endphp
                        <ul class="ft-menu-nav no-bullets">
                            @foreach($intro as $detail)
                                <li>
                                    <a href="{!! route('intro',$detail->slug) !!}">{!! $detail->name !!}</a>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
                <div class="grid__item large--one-quarter medium--one-half small--one--whole">
                    <div class="ft-menu">
                        <h3 class="footer-title">Kiến thức</h3>
                        @php $knowledge = App\CategoryKnowledge::where('status',1)->get(); @endphp
                        <ul class="ft-menu-nav no-bullets">
                            @foreach($knowledge as $detail)
                                <li>
                                    <a href="{!! route('know',$detail->slug) !!}">{!! $detail->name !!}</a>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <div class="footer-copyright text-center">
        copyright &copy; <a href="htt://sakura-green.com">sakura-green.com</a>
    </div>
</footer>

<div id="mobile-bottom-navigation" class="large--hide medium--hide">
    <div class="grid mg-left-0">
        <div class="grid__item medium--two-tenths small--two-tenths pd-left0">
            <div class="mobile-nav-item">
                <a href="{!! route('catenew') !!}">
                    <i class="fa fa-tag" aria-hidden="true"></i><br/>Tin tức
                </a>
            </div>
        </div>
        <div class="grid__item medium--two-tenths small--two-tenths pd-left0">
            <div class="mobile-nav-item">
                <a href="{!! route('catpro') !!}">
                    <i class="fa fa-gift" aria-hidden="true"></i><br/>Sản phẩm
                </a>
            </div>
        </div>
        <div class="grid__item medium--two-tenths small--two-tenths pd-left0">
            <div class="mobile-nav-item">
                <a href="">
                    <i class="fa fa-user" aria-hidden="true"></i><br/>Tài khoản
                </a>
            </div>
        </div>
        <div class="grid__item medium--two-tenths small--two-tenths pd-left0">
            <div class="mobile-nav-item">
                <a href="{!! route('contact') !!}">
                    <i class="fa fa-phone" aria-hidden="true"></i> <br/>Liên hệ
                </a>
            </div>
        </div>
        <div class="grid__item medium--two-tenths small--two-tenths pd-left0">
            <div class="mobile-nav-item">
                <a href="">
                    <i class="fa fa-shopping-cart" aria-hidden="true"></i><br/>Giỏ hàng
                    <span class="number hd-cart-count">
                  {!! count((array) session('cart')) !!}
                  </span>
                </a>
            </div>
        </div>
    </div>
</div>

<div class="float-contact">
    <a href="mailto:{!! $footer->email !!}?subject = Feedback&body = Message" target="_blank">
        <div class="hotline-phone-ring-circle"></div>
        <div class="hotline-phone-ring-circle-fill"></div>
        <div class="chat-zalo">

        </div>
    </a>
    <a href="tel:{!! $footer->phone !!}">
        <div class="chat-face"></div>
    </a>
    <a href="https://zalo.me/{!! $footer->phone !!}" target="_blank">
        <div class="hotline">

        </div>
    </a>

</div>
<button  id="myBtn" >Top</button>
<style>
    #myBtn {
        display: block; /* Hidden by default */
        position: fixed; /* Fixed/sticky position */
        bottom: 20px; /* Place the button at the bottom of the page */
        right: 30px; /* Place the button 30px from the right */
        z-index: 99; /* Make sure it does not overlap */
        border: none; /* Remove borders */
        outline: none; /* Remove outline */
        background-color: red; /* Set a background color */
        color: white; /* Text color */
        cursor: pointer; /* Add a mouse pointer on hover */
        padding: 15px; /* Some padding */
        border-radius: 10px; /* Rounded corners */
        font-size: 18px; /* Increase font size */
    }

    #myBtn:hover {
        background-color: #555; /* Add a dark-grey background on hover */
    }
</style>
<script type="text/javascript">

  $('#myBtn').click(function () {
      alert(1);
      scrollFunction();
  })

    function scrollFunction() {
        if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
            alert(3)
        } else {
            alert(9);
        }
    }

    // When the user clicks on the button, scroll to the top of the document
    function topFunction() {
        document.body.scrollTop = 0; // For Safari
        document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
    }
</script>

<!-- Modal -->
<div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered " role="document">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <div class="modal-header">
                <h1 class="modal-title" id="exampleModalLongTitle">Thông tin sản phẩm</h1>

            </div>
            <div class="modal-body">

            </div>
            <div class="modal-footer">


            </div>
        </div>
    </div>
</div>

<style>

    .float-contact {
        position: fixed;
        bottom: 80px;
        right: 20px;
        z-index: 99999;
    }

    .chat-zalo {
        background-image: url(https://cdn.autoads.asia/maxlead/1.6.x/content/images/widget_icon_contact_form.svg);
        border-radius: 50% !important;
        padding: 0;
        color: white;
        display: block;
        margin-bottom: 6px;
        height: 49px;
        width: 57px;
        background-repeat: no-repeat;
        margin: auto;
        background-position: center;
        cursor: pointer;
    }

    .chat-face {
        background-image: url(https://cdn.autoads.asia/maxlead/1.6.x/content/images/widget_icon_click_to_call.svg);
        border-radius: 50% !important;
        padding: 0;
        color: white;
        display: block;
        margin-bottom: 6px;
        height: 49px;
        width: 57px;
        background-repeat: no-repeat;
        margin: auto;
        background-position: center;
        cursor: pointer;
    }

    .hotline {
        background-image: url(https://cdn.autoads.asia/maxlead/1.6.x/content/images/widget_icon_zalo.svg);
        border-radius: 50% !important;
        padding: 0;
        color: white;
        display: block;
        margin-bottom: 6px;
        height: 49px;
        width: 57px;
        background-repeat: no-repeat;
        margin: auto;
        background-position: center;
        cursor: pointer;
    }

    ul.no-bullets {
        padding: 0px;
    }
</style>

{{-- Using Toastr notifi--}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
<script>

    function openProTabs(evt, cityName) {
        var i, pro_tabcontent, pro_tablinks;
        pro_tabcontent = document.getElementsByClassName("pro-tabcontent");
        for (i = 0; i < pro_tabcontent.length; i++) {
            pro_tabcontent[i].style.display = "none";
        }
        pro_tablinks = document.getElementsByClassName("pro-tablinks");
        for (i = 0; i < pro_tablinks.length; i++) {
            pro_tablinks[i].className = pro_tablinks[i].className.replace(" active", "");
        }
        document.getElementById(cityName).style.display = "block";
        evt.currentTarget.className += " active";
    }

</script>


<script src='{{asset('frontend/js/option_selection.js')}}' type='text/javascript'></script>

<script src="{{asset('frontend/js/fastclick.min.js?v=399')}}" type='text/javascript'></script>
<script src="{{asset('frontend/js/script.js?v=399')}}" type='text/javascript'></script>
<script src="{{asset('frontend/js/timber.js?v=399')}}" type='text/javascript'></script>
<link rel="stylesheet" href="{{asset('frontend/css/jquery.fancybox.min.css')}}"/>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.1.20/jquery.fancybox.min.js"></script>
<script>
    let success = '{!! trans('messages.success') !!}';
    $('.quick-view').click(function (e) {
        let id = $(this).data('id');
        let name = $(this).data('name');
        let price = $(this).data('price');
        let image = $(this).data('image');
        let symbol = $(this).data('symbol');
        let link = $(this).data('link');

        $('.modal-body').html('<form class="grid" id="form-quick-view">\n' +
            '<div class="grid__item large--four-tenths">\n' +
            '<div class="image-zoom">\n' +
            '<img id="p-product-image-feature" class="p-product-image-feature"\n' +
            'src="' + image + '">\n' +
            '</div>\n' +
            '</div>\n' +
            '<div class="grid__item large--six-tenths pull-right">\n' +
            '<h4 class="p-title   modal-title " id="">' + name + '</h4>\n' +
            '<p class="product-more-info">\n' +
            '\t\t\t\t\t<span class="product-sku">\n' +
            '\t\t\t\t\t\tMã sản phẩm: <span id="ProductSku">' + symbol + '</span>\n' +
            '\t\t\t\t\t</span>\n' +
            '</p>\n' +
            '<div class="form-input product-price-wrapper">\n' +
            '<div class="product-price">\n' +
            '<span class="p-price ">' + price + '₫</span>\n' +
            '<del></del>\n' +
            '</div>\n' +
            '<em id="PriceSaving"></em>\n' +
            ' </div>\n' +
            '<div id="swatch-quick-view" class="select-swatch"></div>\n' +
            ' <div class="form-input" style="width: 100%">\n' +
            '\n' +
            '<div class="qv-readmore">\n' +
            '<span> hoặc </span><a class="read-more p-url"\n' +
            'href="' + link + '" role="button">Xem chi\n' +
            ' tiết</a>\n' +
            '</div>\n' +
            '</div>\n' +
            ' </div>\n' +
            '</form>');
        $('.modal-footer').html('<button type="button" class="btn btn-secondary addtocart" >Thêm vào giỏ hàng</button><button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>');

    });

    function addToCart(id){
        let url = '{!! route('addtocart','id') !!}';
        let link = url.replace('id', id);
        $.ajax({
            url: link,
            method: 'GET',
            success: function (data) {
                let count = Object.keys(data).length;
                $('#countShoppingCart').html(count);
                $('.hd-cart-count').html(count);
                toastr.success(success);

            }
        });
    }

    $('.quickbuy').click(function (e) {
        e.preventDefault();
        let id = $(this).data('id');
        let url = '{!! route('addtocart','id') !!}';
        let link = url.replace('id', id);
        $.ajax({
            url: link,
            method: 'GET',
            success: function (data) {
                window.location.href = '{!! route('cart') !!}';


            }
        });
    });
</script>

@yield('script')
</body>
</html>
