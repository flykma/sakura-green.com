<body id="sakuragreen" class="template-index" >
<div id="NavDrawer" class="drawer drawer--right">
    <div class="drawer__header">
        <div class="drawer__close js-drawer-close">
            <button type="button" class="icon-fallback-text">
                <span>Đóng</span><i class="fa fa-times" aria-hidden="true"></i>
            </button>
        </div>
    </div>
    <!-- begin mobile-nav -->
    <ul class="mobile-nav">
        <li class="mobile-nav__item mobile-nav__search">
            <div class="search-form-wrapper-mb" data-id="im here">
                <form id="searchauto_mb" action="{!! route('search') !!}" class="searchform-categoris ultimate-search">
                    <div class="wpo-search">
                        <div class="wpo-search-inner">
                            <div class="input-group">
                                <input id="searchtext_mb" name="q" value=""  maxlength="40" class="form-control input-search" type="text" size="20" placeholder="Tìm kiếm..."/>
                                <span class="input-group-btn">
                              <button type="submit"  id="searchsubmit_mb"><i class="fa fa-search" aria-hidden="true"></i></button>
                              </span>
                            </div>
                        </div>
                        <input type="hidden" class="collection_id" value="(collectionid:product>=0)" />
                        <input type="hidden" class="collection_handle" value="all" />
                        <input type="hidden" class="collection_name" value="all" />
                    </div>
                </form>
            </div>
        </li>
        <li class="mobile-nav__item">
            <a href="{!! route('home') !!}" class="mobile-nav__link">Trang chủ</a>
        </li>
        <li class="mobile-nav__item">
            <a href="{!! route('catpro') !!}" class="mobile-nav__link">Sản phẩm</a>
        </li>
        <li class="mobile-nav__item">
            <a href="{!! route('listnews','tin-tuc') !!}" class="mobile-nav__link">Tin tức</a>
        </li>
        <li class="mobile-nav__item">
            <a href="{!! route('listnews','su-kien') !!}" class="mobile-nav__link">Sự kiện</a>
        </li>
        <li class="mobile-nav__item">
            <a href="{!! route('video') !!}" class="mobile-nav__link">Video</a>
        </li>
        <li class="mobile-nav__item">
            <a href="{!! route('contact') !!}" class="mobile-nav__link">Liên hệ</a>
        </li>
        <li class="mobile-nav__item">
            <a href="javascript:void(0)" id="customer_login_link">Đăng nhập</a>
        </li>
        <li class="mobile-nav__item">
            <a href="" id="customer_register_link">Đăng kí</a>
        </li>
    </ul>
    <!-- //mobile-nav -->
</div>
<div class="cart-overlay"></div>
<div id="CartDrawer" class="drawer drawer--left">
    <div class="drawer__header">
        <div class="drawer__title h3">Giỏ hàng</div>
        <div class="drawer__close js-drawer-close">
            <button type="button" class="icon-fallback-text">
                <span class="icon icon-x" aria-hidden="true"></span>
                <span class="fallback-text">"Đóng"</span>
            </button>
        </div>
    </div>
    <div id="CartContainer"></div>
</div>
