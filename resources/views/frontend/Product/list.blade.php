@extends('frontend.Layout.master')
@section('title','Danh sách sản phẩm')
@section('content')
    <section id="collection-wrapper">
        <div class="wrapper">
            <div class="inner">
                <div class="grid">
                    <div class="grid__item large--three-quarters medium--one-whole small--one-whole">
                        <div class="collection-content-wrapper">
                            <div class="collection-head">
                                <div class="grid mg-left-15">
                                    <div
                                        class="grid__item large--two-thirds medium--one-whole small--one-whole pd-left15">
                                        <div class="collection-title">
                                            <h1>Tất cả sản phẩm</h1>
                                        </div>
                                    </div>

                                    <div
                                        class="grid__item large--one-third medium--one-whole small--one-whole pd-left15">
                                        <div class="collection-sorting-wrapper">
                                            <!-- /snippets/collection-sorting.liquid -->
                                            <div class="form-horizontal text-right">
                                                <label for="SortBy">Sắp xếp</label>
                                                <select name="sort" id="sort">
                                                    <option value="id" data-sort="DESC">Mới nhất</option>
                                                    <option value="productname" data-sort="ASC">Theo bảng chữ cái từ A-Z</option>
                                                    <option value="productname" data-sort="DESC">Theo bảng chữ cái từ Z-A</option>
                                                    <option value="price" data-sort="ASC">Giá từ thấp tới cao</option>
                                                    <option value="price" data-sort="DESC">Giá từ cao tới thấp</option>
                                                    <option value="id" data-sort="ASC">Cũ nhất</option>
                                                </select>
                                            </div>


                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="collection-body">
                                <div class="grid-uniform product-list md-mg-left-10 product">

                                </div>
                                <div class="pagination not-filter">
                                    <div id="pagination-" class="text-center clear-left">
                                        <div class="pagination-custom">


                                        </div>
                                    </div>
                                </div>
                                <style>
                                    ul.pagination {
                                        list-style: none;
                                    }

                                    ul.pagination li {
                                        display: inline-block;
                                    }
                                </style>
                            </div>
                        </div>

                    </div>

                    <div class="grid__item large--one-quarter medium--one-whole small--one-whole">
                        <div class="collection-sidebar-wrapper">
                            <div class="grid">
                                <div class="grid__item large--one-whole medium--one-half small--one-whole">
                                    <div class="collection-filter-price">
                                        <button class="accordion cs-title col-sb-trigger panel-sort" id="catproduct-sort">
                                            <span>Danh mục sản phẩm</span>
                                        </button>
                                        <div class="panel  sidebar-sort" id="catsidebar-sort">
                                            @php $catpro = App\CategoryProduct::where('status',1)->get(); @endphp
                                            <ul class="no-bullets filter-price clearfix">
                                                @foreach($catpro as $itemcatpro)

                                                    <li>
                                                        <a href="{!! route('listproduct',$itemcatpro->slug) !!}">--{!! $itemcatpro->categoryName !!}</a>

                                                    </li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    </div>
                                </div>

                                <div class="grid__item large--one-whole medium--one-half small--one-whole">
                                    <div class="collection-filter-price">
                                        <button class="accordion cs-title col-sb-trigger panel-sort" id="panel-sort">
                                            <span>Khoảng giá</span>
                                        </button>
                                        <div class="panel  sidebar-sort" id="sidebar-sort">
                                            <ul class="no-bullets filter-price clearfix">
                                                <li>
                                                    <label>
                                                        <input type="radio" name="price-filter" data-price="0" value="1000000000000000">
                                                        <span>Tất cả</span>
                                                    </label>
                                                </li>
                                                <li>
                                                    <label>
                                                        <input type="radio" name="price-filter" data-price="0" value="100000">
                                                        <span>Nhỏ hơn 100,000₫</span>
                                                    </label>
                                                </li>
                                                <li>
                                                    <label>
                                                        <input type="radio" name="price-filter" data-price="100000" value="200000">
                                                        <span>Từ 100,000₫ - 200,000₫</span>
                                                    </label>
                                                </li>
                                                <li>
                                                    <label>
                                                        <input type="radio" name="price-filter" data-price="200000" value="300000">
                                                        <span>Từ 200,000₫ - 300,000₫</span>
                                                    </label>
                                                </li>
                                                <li>
                                                    <label>
                                                        <input type="radio" name="price-filter" data-price="300000" value="400000">
                                                        <span>Từ 300,000₫ - 400,000₫</span>
                                                    </label>
                                                </li>
                                                <li>
                                                    <label>
                                                        <input type="radio" name="price-filter" data-price="400000" value="">
                                                        <span>Từ 400,000₫ - 500,000₫</span>
                                                    </label>
                                                </li>
                                                <li>
                                                    <label>
                                                        <input type="radio" name="price-filter" data-price="500000" value="10000000000000">
                                                        <span>Lớn hơn 500,000₫</span>
                                                    </label>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>

@stop

@section('script')
    <script>
            let url = '{!! asset('uploads/product/') !!}';
            let product = document.getElementsByClassName('product')
            $.ajax({
                url: '{!! route('getlistpro') !!}',
                method: 'GET',
                dataType: 'json',
                success: function (result) {
                    //console.log(data);
                    // data = JSON.parse(result);
                    $.each(result, function (index, value) {
                        let link = '{!! route('getdetailpro','proslug') !!}';
                        let urllink = link.replace('proslug',value["slug"]);
                        let vnd = parseFloat(value["price"], 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").replace('.00','').toString();
                        $('.product').append('<div class="md-pd-left10 grid__item large--one-third medium--one-half small--one-half">\n' +
                            '<div class="product-item">\n' +
                            '<div class="product-img">\n' +
                            '<a href="'+ urllink +'">\n' +
                            '<picture>\n' +
                            '<source media="(max-width: 480px)" srcset="' + url + '/' + value["photo"] + '">\n' +
                            '<source media="(min-width: 481px)" srcset=" ' + url + '/' + value["photo"] + '">\n' +
                            '<img id="'+ value["id"]+'" src="' + url + '/' + value["photo"] + '" alt="' +value["productname"] + '">\n' +
                            '</picture>\n' +
                            '</a>\n' +
                            '<div class="product-tags">\n' +
                            '</div>\n' +
                            '</div>\n' +
                            '<div class="product-info">\n' +
                            '<div class="product-title">\n' +
                            '<a href="'+ urllink +'">' +  value["productname"] +
                            '</a>\n' +
                            '</div>\n' +
                            '<div class="product-price clearfix">\n' +
                            '<span class="current-price">' + vnd + ' đ</span>\n' +
                            '</div>\n' +
                            '<div class="product-actions text-center clearfix">\n' +
                            '<div>\n' +
                            '<button type="button" onclick="addToCart('+  value["id"] +')" class="btn-addToCart medium--hide small--hide" data-id="'+ value["id"] +'"> <span><i class="fa fa-cart-plus" aria-hidden="true"></i></span></button>' +
                            '<button type="button" class="btn-quickview quick-view medium--hide small--hide" data-toggle="modal" data-target=".bd-example-modal-lg" data-id="'+ value["id"] +'" data-name="' +  value["productname"] +'" data-price="' +  value["price"] + '" data-image="' + url + '/' +  value["photo"] + '" data-symbol="'+  value["symbol"] +'" data-link=""><span><i class="fa fa-search-plus" aria-hidden="true"></i></span> </button>' +
                            '<button type="button" class="btn-buyNow quickbuy medium--hide small--hide" data-id="'+  value["id"] +'"> Mua ngay</button>' +
                            '</a>\n' +
                            '</div>\n' +
                            '</div>\n' +
                            '</div>\n' +
                            '</div>\n' +
                            '</div>');

                    });
                }
            });

            $('#sort').change(function (e) {
                e.preventDefault();
                let sort = $(this).val();
                let datasort = $('#sort option:selected').data('sort');
                $.ajax({
                    url: '{!! route('sort') !!}',
                    data: {sort: sort, datasort: datasort},
                    dataType: 'json',
                    success: function (result) {
                        //console.log(data);
                        // data = JSON.parse(result);
                        $('.product').html("");
                        $.each(result, function (index, value) {
                            let link = '{!! route('getdetailpro','proslug') !!}';
                            let urllink = link.replace('proslug',value["slug"]);
                            let vnd = parseFloat(value["price"], 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").replace('.00','').toString();
                            $('.product').append('<div class="md-pd-left10 grid__item large--one-third medium--one-half small--one-half">\n' +
                                '<div class="product-item">\n' +
                                '<div class="product-img">\n' +
                                '<a href="'+ urllink +'">\n' +
                                '<picture>\n' +
                                '<source media="(max-width: 480px)" srcset="' + url + '/' + value["photo"] + '">\n' +
                                '<source media="(min-width: 481px)" srcset=" ' + url + '/' + value["photo"] + '">\n' +
                                '<img id="'+ value["id"]+'" src="' + url + '/' + value["photo"] + '" alt="' +value["productname"] + '">\n' +
                                '</picture>\n' +
                                '</a>\n' +
                                '<div class="product-tags">\n' +
                                '</div>\n' +
                                '</div>\n' +
                                '<div class="product-info">\n' +
                                '<div class="product-title">\n' +
                                '<a href="'+ urllink +'">' +  value["productname"] +
                                '</a>\n' +
                                '</div>\n' +
                                '<div class="product-price clearfix">\n' +
                                '<span class="current-price">' + vnd + ' đ</span>\n' +
                                '</div>\n' +
                                '<div class="product-actions text-center clearfix">\n' +
                                '<div>\n' +
                                '<button type="button" onclick="addToCart('+  value["id"] +')" class="btn-addToCart medium--hide small--hide" data-id="'+ value["id"] +'"> <span><i class="fa fa-cart-plus" aria-hidden="true"></i></span></button>' +
                                '<button type="button" class="btn-quickview quick-view medium--hide small--hide" data-toggle="modal" data-target=".bd-example-modal-lg" data-id="'+ value["id"] +'" data-name="' +  value["productname"] +'" data-price="' +  value["price"] + '" data-image="' + url + '/' +  value["photo"] + '" data-symbol="'+  value["symbol"] +'" data-link=""><span><i class="fa fa-search-plus" aria-hidden="true"></i></span> </button>' +
                                '<button type="button" class="btn-buyNow quickbuy medium--hide small--hide" data-id="'+  value["id"] +'"> Mua ngay</button>' +
                                '</a>\n' +
                                '</div>\n' +
                                '</div>\n' +
                                '</div>\n' +
                                '</div>\n' +
                                '</div>');

                        });
                    }
                });
            });

            $('input[type=radio][name=price-filter]').change(function() {
                let pricemin = $(this).data('price');
                let pricemax = $(this).val();
                $.ajax({
                    url: '{!! route('pricesort') !!}',
                    data: {pricemin: pricemin, pricemax: pricemax},
                    dataType: 'json',
                    success: function (result) {
                        //console.log(data);
                        // data = JSON.parse(result);
                        $('.product').html("");
                        if(result.length > 0) {

                            $.each(result, function (index, value) {
                                let link = '{!! route('getdetailpro','proslug') !!}';
                                let urllink = link.replace('proslug',value["slug"]);
                                let vnd = parseFloat(value["price"], 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").replace('.00','').toString();
                                $('.product').append('<div class="md-pd-left10 grid__item large--one-third medium--one-half small--one-half">\n' +
                                    '<div class="product-item">\n' +
                                    '<div class="product-img">\n' +
                                    '<a href="'+ urllink +'">\n' +
                                    '<picture>\n' +
                                    '<source media="(max-width: 480px)" srcset="' + url + '/' + value["photo"] + '">\n' +
                                    '<source media="(min-width: 481px)" srcset=" ' + url + '/' + value["photo"] + '">\n' +
                                    '<img id="'+ value["id"]+'" src="' + url + '/' + value["photo"] + '" alt="' +value["productname"] + '">\n' +
                                    '</picture>\n' +
                                    '</a>\n' +
                                    '<div class="product-tags">\n' +
                                    '</div>\n' +
                                    '</div>\n' +
                                    '<div class="product-info">\n' +
                                    '<div class="product-title">\n' +
                                    '<a href="'+ urllink +'">' +  value["productname"] +
                                    '</a>\n' +
                                    '</div>\n' +
                                    '<div class="product-price clearfix">\n' +
                                    '<span class="current-price">' + vnd + ' đ</span>\n' +
                                    '</div>\n' +
                                    '<div class="product-actions text-center clearfix">\n' +
                                    '<div>\n' +
                                    '<button type="button" onclick="addToCart('+  value["id"] +')" class="btn-addToCart medium--hide small--hide" data-id="'+ value["id"] +'"> <span><i class="fa fa-cart-plus" aria-hidden="true"></i></span></button>' +
                                    '<button type="button" class="btn-quickview quick-view medium--hide small--hide" data-toggle="modal" data-target=".bd-example-modal-lg" data-id="'+ value["id"] +'" data-name="' +  value["productname"] +'" data-price="' +  value["price"] + '" data-image="' + url + '/' +  value["photo"] + '" data-symbol="'+  value["symbol"] +'" data-link=""><span><i class="fa fa-search-plus" aria-hidden="true"></i></span> </button>' +
                                    '<button type="button" class="btn-buyNow quickbuy medium--hide small--hide" data-id="'+  value["id"] +'"> Mua ngay</button>' +
                                    '</a>\n' +
                                    '</div>\n' +
                                    '</div>\n' +
                                    '</div>\n' +
                                    '</div>\n' +
                                    '</div>');

                            });
                        } else {
                            $('.product').append("<h3>Không thấy kêt quả</h3>")
                        }

                    }
                });

            });

    </script>
    <script>
        $('#panel-sort').click(function (e) {
            if($('#panel-sort').hasClass('active')) {
                $('#panel-sort').removeClass('active');
                $('#sidebar-sort').css('max-height','0px');
            }
            else {
                $('#panel-sort').addClass('active');
                $('#sidebar-sort').css('max-height','280px');
            }

        });

        $('#catproduct-sort').click(function (e) {
            if($('#catproduct-sort').hasClass('active')) {
                $('#catproduct-sort').removeClass('active');
                $('#catsidebar-sort').css('max-height','0px');
            }
            else {
                $('#catproduct-sort').addClass('active');
                $('#catsidebar-sort').css('max-height','280px');
            }

        });
    </script>
@stop
