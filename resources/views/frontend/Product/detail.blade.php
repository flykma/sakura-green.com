@extends('frontend.Layout.master')
@section('title',$product->productname)
@section('content')
    <section id="product-wrapper">
        <div class="wrapper">
            <div class="inner">
                <div itemscope="" itemtype="">
                    <div class="grid product-single">
                        <div class="grid__item large--five-twelfths medium--one-whole small--one-whole">


                            <div class="large-5 column">
                                <div class="xzoom-container">
                                    <img class="xzoom" id="xzoom-default"
                                         src="{!! asset('uploads/'.$product->photo) !!}"
                                         xoriginal="{!! asset('uploads/'.$product->photo) !!}"/>
                                    <div class="xzoom-thumbs">
                                        @if($photo)
                                            @foreach($photo as $itemphoto)
                                                <a href="{!! asset('uploads/'.$itemphoto) !!}"><img
                                                        class="xzoom-gallery" width="80"
                                                        src="{!! asset('uploads/'.$itemphoto) !!}"
                                                        xpreview="{!! asset('uploads/'.$itemphoto) !!}"
                                                    ></a>
                                            @endforeach
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="large-7 column"></div>


                        </div>
                        <div class="grid__item large--seven-twelfths medium--one-whole small--one-whole">
                            <div class="product-content">
                                <div class="pro-content-head clearfix">
                                    <h1 itemprop="name">{!! $product->productname !!}</h1>
                                    <div class="pro-brand">
                                        <span class="title">{!! trans('messages.trademark') !!}: Sakura green</span>
                                    </div>
                                    <span>|</span>

                                </div>
                                <div class="pro-price clearfix">
                                    <span
                                        class="current-price ProductPrice">{!! number_format($product->price,0) !!}₫</span>
                                </div>
                                <div class="pro-short-desc">
                                    {!! $product->description !!}
                                </div>
                                <form action="" method="post" enctype="multipart/form-data" id="AddToCartForm"
                                      class="form-vertical">
                                    <div class="grid mg-left-5">
                                        <div
                                            class="grid__item large--four-fifths medium--two-thirds small--one-whole pd-left5">
                                            <div class="product-actions clearfix">
                                                <a href="javascript:void(0)" class="btnHome btnBuyNow"
                                                   onclick="addToCart({!! $product->id !!})">{!! trans('messages.addtocart') !!}</a>
                                                <button type="button" name="buy"  data-id="{!! $product->id !!}" class="btnHome quickbuy">
                                                    {!! trans('messages.buynow') !!}
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </form>

                            </div>
                        </div>
                    </div>
                    <div class="product-description-wrapper">
                        <div class="tab clearfix">
                            <button class="pro-tablinks" onclick="openProTabs(event, 'protab0')"
                                    id="defaultOpenProTabs"><span>{!! trans('messages.description') !!}</span></button>
                            <button class="pro-tablinks" onclick="openProTabs(event, 'protab1')"><span>{!! trans('messages.uses') !!}</span>
                            </button>
                            <button class="pro-tablinks active" onclick="openProTabs(event, 'protab2')"><span>{!! trans('messages.guide') !!}</span>
                            </button>
                        </div>
                        <div id="protab0" class="pro-tabcontent" style="display: none;">
                            {!! $product->description !!}
                        </div>
                        <div id="protab1" class="pro-tabcontent" style="display: none;">
                            {!! $product->contents !!}
                        </div>
                        <div id="protab2" class="pro-tabcontent" style="display: block;">
                            {!! $product->guide !!}
                        </div>
                    </div>
                    <div id="related-products">
                        <div class="home-section-head clearfix">
                            <h2>{!! trans('messages.relatedproducts') !!}</h2>
                        </div>
                        <div class="home-section-body">
                            <div id="related-product-slider" class="grid mg-left-15 owl-carousel owl-theme">

                                @if($products)
                                    @foreach($products as $detail)

                                        <div class="grid__item pd-left15 item">


                                            <div class="product-item">

                                                <div class="product-img">
                                                    <a href="{!! route("getdetailpro",$detail->slug) !!}">
                                                        <picture>
                                                            <source media="(max-width: 480px)"
                                                                    srcset="{!! asset('uploads/product/'.$detail->photo) !!}">
                                                            <source media="(min-width: 481px)"
                                                                    srcset="{!! asset('uploads/product/'.$detail->photo) !!}">
                                                            <img id="{!! $detail->id !!}"
                                                                 src="{!! asset('uploads/product/'.$detail->photo) !!}"
                                                                 alt="{!! $detail->productname !!}"/>
                                                        </picture>

                                                    </a>

                                                    <div class="product-tags">


                                                    </div>
                                                </div>

                                                <div class="product-info">
                                                    <div class="product-title">
                                                        <a href="{!! route("getdetailpro",$detail->slug) !!}">{!! $detail->productname !!}</a>
                                                    </div>

                                                    <div class="product-price clearfix">
                                                        <span class="current-price">{!! number_format($detail->price,0) !!}₫</span>

                                                    </div>

                                                    <div class="product-actions text-center clearfix">
                                                        <div>
                                                            <button type="button"
                                                                    class="btn-addToCart medium--hide small--hide"
                                                                    onclick="addToCart({!! $detail->id !!})"><span><i
                                                                        class="fa fa-cart-plus" aria-hidden="true"></i></span>
                                                            </button>
                                                            <button type="button"
                                                                    class="btn-quickview quick-view medium--hide small--hide"
                                                                    data-toggle="modal"
                                                                    data-target=".bd-example-modal-lg"
                                                                    data-id="{!! $detail->id !!} "
                                                                    data-name="{!! $detail->productname!!}"
                                                                    data-price="{!! $detail->price !!}"
                                                                    data-image="{!! asset("uploads/product/".$detail->photo)  !!}"
                                                                    data-symbol="{!! $detail->symbol !!}"
                                                                    data-link="{!! route('getdetailpro',$detail->slug) !!}">
                                                                <span><i class="fa fa-search-plus"
                                                                         aria-hidden="true"></i></span></button>
                                                            <button type="button"
                                                                    class="btn-buyNow quickbuy medium--hide small--hide"
                                                                    data-id="{!! $detail->id !!}"> {!! trans('messages.buynow') !!}
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>

                                        </div>
                                    @endforeach
                                @endif


                            </div>
                        </div>
                    </div>

                    <div id="seen-products" class="seen-products-slider">
                        <div class="home-section-head clearfix">
                            <h2>{!! trans('messages.productsviewed') !!}</h2>
                        </div>
                        <div class="home-section-body">
                            <div class="show">
                                <div class="product-seen grid mg-left-15">
                                    <div id="owl-spdx" class="owl-carousel new-slide owl-theme"
                                         style="opacity: 1; display: block;">
                                        <div class="owl-wrapper-outer">
                                            <div class="owl-wrapper"
                                                 style="width: 5202px; left: 0px; display: block; transform: translate3d(0px, 0px, 0px); transition: all 1000ms ease 0s;">

                                                @if(session('seen'))
                                                    @foreach(session('seen') as $id=>$detail)
                                                        <div class="owl-item" style="width: 289px;">
                                                            <div class="grid__item item pd-left15">

                                                                <div class="seen-item product-item">
                                                                    <div class="product-img">
                                                                        <a href="{!! route("getdetailpro",$detail['slug']) !!}">
                                                                            <img
                                                                                src="{!! asset('uploads/product/'.$detail['image']) !!}"
                                                                                alt="{!! $detail['name'] !!}">
                                                                        </a>
                                                                        <div class="product-tags">

                                                                        </div>
                                                                    </div>

                                                                    <div class="product-info">
                                                                        <div class="product-title">
                                                                            <a href="{!! route("getdetailpro",$detail['slug']) !!}">{!! $detail['name'] !!}</a>
                                                                        </div>

                                                                        <div class="product-price clearfix">
                                                                            <span class="current-price">{!! number_format($detail['price'],0) !!}₫</span>
                                                                            <span class="original-price"></span>
                                                                        </div>
                                                                        <div
                                                                            class="product-actions text-center clearfix">
                                                                            <div>
                                                                                <button type="button"
                                                                                        class="btn-addToCart addtocart medium--hide small--hide"
                                                                                        onclick="addToCart({!! $id !!})"><span><i
                                                                                            class="fa fa-cart-plus"
                                                                                            aria-hidden="true"></i></span>
                                                                                </button>
                                                                                <button type="button"
                                                                                        class="btn-quickview quick-view medium--hide small--hide"
                                                                                        data-toggle="modal"
                                                                                        data-target=".bd-example-modal-lg"
                                                                                        data-id="{!! $id !!} "
                                                                                        data-name="{!! $detail['name'] !!}!!}"
                                                                                        data-price="{!! $detail['price'] !!}"
                                                                                        data-image="{!! asset("uploads/product/".$detail['image'])  !!}"
                                                                                        data-symbol=""
                                                                                        data-link="{!! route("getdetailpro",$detail['slug']) !!}">
                                                                                                        <span><i class="fa fa-search-plus"
                                                                                                                 aria-hidden="true"></i></span>
                                                                                </button>
                                                                                <button type="button"
                                                                                        class="btn-buyNow quickbuy medium--hide small--hide"
                                                                                        data-id="{!! $id !!}">{!! trans('messages.buynow') !!}
                                                                                </button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    @endforeach
                                                @endif
                                            </div>
                                        </div>

                                    </div>


                                </div>
                            </div>
                        </div>
                    </div>



                </div>
            </div>
        </div>
    </section>
@stop
