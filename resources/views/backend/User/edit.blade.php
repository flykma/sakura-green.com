@extends('backend.Layout.master')
@section('title','Edit User')
@section('content')
    <div class="content-wrapper">

        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">
                            <i class="fa fa-plus" aria-hidden="true"></i> Sửa thông tin người dùng
                        </h1>
                        <div class="more_info"></div>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{route('admin')}}"><i class="fa fa-home fa-1x"></i>
                                    Trang chủ</a></li>
                            <li class="breadcrumb-item active">Sửa người dùng</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>


        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header with-border">
                                <h2 class="card-title">Sửa thông tin người dùng</h2>
                                <div class="card-tools">
                                    <div class="btn-group float-right mr-5">
                                        <a href="{{route('listuser')}}" class="btn  btn-flat btn-default"
                                           title="List"><i class="fa fa-list"></i><span class="hidden-xs"> Trở lại danh sách</span></a>
                                    </div>
                                </div>
                            </div>
                            @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            <form action="{{route('postupdateuser',$user->id)}}" method="post" accept-charset="UTF-8"
                                  class="form-horizontal" id="form-main"
                                  enctype="multipart/form-data">
                                @csrf

                                <div class="card-body">
                                    <div class="form-group  row ">
                                        <label for="name" class="col-sm-2  control-label">Tên đầy đủ</label>
                                        <div class="col-sm-8">
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text"><i
                                                            class="fas fa-pencil-alt"></i></span>
                                                </div>
                                                <input type="text" id="name" name="name" value="{!! $user->name !!}"
                                                       class="form-control name" placeholder=""/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group  row ">
                                        <label for="email" class="col-sm-2  control-label">Email</label>
                                        <div class="col-sm-8">
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text"><i
                                                            class="fas fa-pencil-alt"></i></span>
                                                </div>
                                                <input type="text" id="email" name="email" value="{!! $user->email !!}"
                                                       class="form-control email" placeholder="" disabled/>
                                                <input type="hidden" value="{!! $user->password !!}" name="password">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group  row ">
                                        <label for="avatar" class="col-sm-2  control-label">Hình đại diện</label>
                                        <div class="col-sm-8">
                                            <div class="input-group">
                                                <input type="file" id="avatar" name="avatar"
                                                       value="{!! $user->avatar !!}" class="form-control avatar"
                                                       placeholder=""/>
                                                <img id="blah" src="{!! url("uploads",$user->avatar) !!}"
                                                     alt="your image" class="img-responsive" style="width: 200px;
    margin-left: 10px;"/>
                                                <script>
                                                    avatar.onchange = evt => {
                                                        const [file] = avatar.files
                                                        if (file) {
                                                            blah.src = URL.createObjectURL(file)
                                                        }
                                                    }
                                                </script>
                                            </div>
                                            <div id="preview_avatar" class="img_holder">
                                            </div>
                                        </div>
                                    </div>
{{--                                    <div class="form-group  row ">--}}
{{--                                        <label for="password" class="col-sm-2  control-label">Mật khẩu</label>--}}
{{--                                        <div class="col-sm-8">--}}
{{--                                            <div class="input-group">--}}
{{--                                                <div class="input-group-prepend">--}}
{{--                                                    <span class="input-group-text"><i--}}
{{--                                                            class="fas fa-pencil-alt"></i></span>--}}
{{--                                                </div>--}}
{{--                                                <input type="password" id="password" name="password" value=""--}}
{{--                                                       class="form-control password" placeholder=""/>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}


                                    <div class="form-group row ">
                                        <label for="permission" class="col-sm-2  control-label">Thêm quyền hạn</label>
                                        <div class="col-sm-8">
                                            <select class="form-control permission select2"
                                                    data-placeholder="Thêm quyền hạn" style="width: 100%;"
                                                    name="role">
                                                <option value=""></option>
                                                <option value="1" {!! $user->role == 1? "selected" : "" !!}>
                                                    Administrator..
                                                </option>
                                                <option value="2" {!! $user->role == 2? "selected" : "" !!}>Customer
                                                </option>

                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="card-footer row">

                                    <div class="col-md-2">
                                    </div>
                                    <div class="col-md-8">
                                        <div class="btn-group float-right">
                                            <button type="submit" class="btn btn-primary">Gửi</button>
                                        </div>
                                        <div class="btn-group float-left">
                                            <button type="reset" class="btn btn-warning">Làm lại</button>
                                        </div>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>

    </div>
@stop

