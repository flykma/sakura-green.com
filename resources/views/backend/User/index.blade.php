@extends('backend.Layout.master')
@section('title','List User')
@section('style')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css"/>
    <link href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">
@stop
@section('content')
    <div class="content-wrapper">

        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">
                            <i class="fa fa-indent" aria-hidden="true"></i> Danh sách người dùng
                        </h1>
                        <div class="more_info"></div>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{!! route('admin') !!}"><i
                                        class="fa fa-home fa-1x"></i> Trang chủ</a></li>
                            <li class="breadcrumb-item active">Danh sách người dùng</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>

        @if(session()->has('error'))
            <div class="alert alert-danger">
                {{ session()->get('error') }}
            </div>
        @endif
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header with-border">
                                <div class="card-tools">
                                    <div class="menu-right">
                                        <form action="" id="button_search">
                                            <div class="input-group input-group" style="width: 350px;">
                                                <input type="text" name="keyword"
                                                       class="form-control rounded-0 float-right" placeholder="Tìm tên"
                                                       value="">
                                                <div class="input-group-append">
                                                    <button type="submit" class="btn btn-primary"><i
                                                            class="fas fa-search"></i></button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                <div class="float-left">
                                </div>

                            </div>
                            <div class="card-header with-border">
                                <br>
                                <a href="https://www.tutsmake.com/how-to-install-yajra-datatables-in-laravel/"
                                   class="btn btn-secondary">Back to Post</a>
                                <a href="javascript:void(0)" class="btn btn-info ml-3" id="create-new-product">Add
                                    New</a>
                                <br><br>
                            </div>

                            <div class="card-body p-0" id="pjax-container">
                                <div id="url-sort" data-urlsort="" style="display: none;"></div>
                                <div class="table-responsive">
                                    <table class="table table-bordered table-striped" id="user_datatables">
                                        <thead>
                                        <tr>

                                            <th>STT</th>
                                            <th>Tên đăng nhập</th>
                                            <th>Tên đầy đủ</th>
                                            <th>Quyền hạn</th>
                                            <th>Thao tác</th>
                                        </tr>
                                        </thead>
                                    </table>


                                    <div class="modal fade" id="ajax-user-modal" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h4 class="modal-title" id="userCrudModal"></h4>
                                                </div>
                                                <div class="modal-body">
                                                    <form id="productForm" name="productForm" class="form-horizontal">
                                                        @csrf
                                                        <input type="hidden" name="product_id" id="product_id">
                                                        <div class="form-group">
                                                            <label for="name"
                                                                   class="col-sm-2 control-label">Email</label>
                                                            <div class="col-sm-12">
                                                                <input type="email" class="form-control" id="email"
                                                                       name="email" placeholder="" value=""
                                                                       maxlength="50" required="">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="name"
                                                                   class="col-sm-2 control-label">Name</label>
                                                            <div class="col-sm-12">
                                                                <input type="text" class="form-control" id="name"
                                                                       name="name" placeholder="" value=""
                                                                       maxlength="50" required="">
                                                            </div>
                                                        </div>

                                                        <div class="form-group password">
                                                            <label for="name"
                                                                   class="col-sm-2 control-label">Password</label>
                                                            <div class="col-sm-12">
                                                                <input type="password" class="form-control"
                                                                       id="password"
                                                                       name="password" placeholder="" value=""
                                                                       maxlength="50" required="">
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label for="name"
                                                                   class="col-sm-2 control-label">Role</label>
                                                            <div class="col-sm-12">
                                                                <select name="role" id="role" class="form-control">
                                                                    <option value="1">Administrator</option>
                                                                    <option value="0">USER</option>
                                                                </select>
                                                            </div>
                                                        </div>


                                                        <div class="col-sm-offset-2 col-sm-10">
                                                            <button type="submit" class="btn btn-primary" id="btn-save"
                                                                    value="Save Changes">Save changes
                                                            </button>
                                                        </div>
                                                    </form>
                                                </div>
                                                <div class="modal-footer">

                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                </div>

                            </div>

                            <div class="card-footer clearfix">
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </section>

    </div>
@stop
@section('script')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
    <script>
        $(document).ready(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                }
            });
            $('#user_datatables').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: '{!! route('listuser') !!}',
                    type: 'GET',
                },
                columns: [

                    {data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false, searchable: false},
                    {data: 'email',name:'email'},
                    {data: 'name',name:'name'},
                    {data: 'role',name: 'role'},
                    {data: 'action', name: 'action', orderable: false, searchable: false}
                ],
                order: [[0, 'desc']],
            });
            $('#create-new-product').click(function () {
                $('#btn-save').val("create-product");
                $('#product_id').val('');
                $('#productForm').trigger("reset");
                $('#userCrudModal').html("Add New Product");
                $('#ajax-user-modal').modal('show');
            });


            $('body').on('click', '.edit-product', function () {
                let id = $(this).data('id');
                $.get('user-edit/'+id, function (data) {
                    $('#title-error').hide();
                    $('#product_code-error').hide();
                    $('#description-error').hide();
                    $('#userCrudModal').html("Edit Product");
                    $('#btn-save').html("edit-product");
                    $('#ajax-user-modal').modal('show');
                    $('#product_id').val(data.id);
                    $('#product_id').attr('disabled',true);
                    $('#name').val(data.name);
                    $('#email').val(data.email);
                    $('.password').hide();
                })
            });

            $('body').on('click', '#delete-product', function () {

                let id = $(this).data('id');
                let url = '{!! route('deleteuser','id') !!}';
                let link = url.replace("id",id);
                if (confirm("Are You sure want to delete !")) {
                    $.ajax({
                        type: "GET",
                        url: link,
                        success: function (data) {
                            let oTable = $('#laravel_datatable').dataTable();
                            oTable.load(false);
                            location.reload();
                        },
                        error: function (data) {
                            console.log('Error:', data);
                        }
                    });
                }
            });

        });

        if ($("#productForm").length > 0) {
            $("#productForm").validate({

                submitHandler: function (form) {

                    var actionType = $('#btn-save').val();
                    $('#btn-save').html('Sending..');

                    $.ajax({
                        data: $('#productForm').serialize(),
                        url: '{!! route('postadduser') !!}',
                        type: "POST",
                        dataType: 'json',
                        success: function (data) {

                            $('#productForm').trigger("reset");
                            $('#ajax-user-modal').modal('hide');
                            $('#btn-save').html('Save Changes');
                            let oTable = $('#laravel_datatable').dataTable();
                            oTable.load(false);
                            location.reload();

                        },
                        error: function (data) {
                            console.log('Error:', data);
                            $('#btn-save').html('Save Changes');
                        }
                    });
                }
            })
        }
    </script>

@stop

