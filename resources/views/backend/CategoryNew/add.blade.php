@extends('backend.Layout.master')
@section('title','Add Category News')
@section('content')
    <div class="content-wrapper">

        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">
                            <i class="fa fa-plus" aria-hidden="true"></i> Thêm mới danh mục
                        </h1>
                        <div class="more_info"></div>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{!! route('admin') !!}"><i
                                        class="fa fa-home fa-1x"></i> Trang chủ</a></li>
                            <li class="breadcrumb-item active">Thêm mới danh mục</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>


        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header with-border">
                                <h2 class="card-title">Thêm mới</h2>
                                <div class="card-tools">
                                    <div class="btn-group float-right mr-5">
                                        <a href="{!! route('listcatnew') !!}" class="btn  btn-flat btn-default"
                                           title="List"><i class="fa fa-list"></i><span class="hidden-xs"> Trở lại danh sách</span></a>
                                    </div>
                                </div>
                            </div>
                            @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif

                            <form action="{!! route('postaddcatnew') !!}" method="post" accept-charset="UTF-8" class="form-horizontal" id="form-main"
                                  enctype="multipart/form-data">
                                @csrf
                                <div class="card">
                                    <div class="card-body">
                                        <div class="form-group row  ">
                                            <label for="title" class="col-sm-2 col-form-label">Tên danh mục tin tức<span class="seo"
                                                                                                             title="SEO"><i
                                                        class="fa fa-coffee" aria-hidden="true"></i></span></label>
                                            <div class="col-sm-8">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text"><i class="fas fa-pencil-alt"></i></span>
                                                    </div>
                                                    <input type="text" id="name" name="name"
                                                           value="" class="form-control" placeholder=""/>
                                                </div>
                                                <span class="form-text"><i class="fa fa-info-circle"></i> Tối đa 200 kí tự</span>
                                            </div>
                                        </div>

                                        <div class="form-group row  ">
                                            <label for="vi__keyword" class="col-sm-2 col-form-label">Từ khóa <span
                                                    class="seo" title="SEO"><i class="fa fa-coffee"
                                                                               aria-hidden="true"></i></span></label>
                                            <div class="col-sm-8">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text"><i class="fas fa-pencil-alt"></i></span>
                                                    </div>
                                                    <input type="text" id="seotitle" name="seotitle"
                                                           value="" class="form-control " placeholder=""/>
                                                </div>
                                                <span class="form-text"><i class="fa fa-info-circle"></i> Tối đa 200 kí tự
</span>
                                            </div>
                                        </div>
                                        <div class="form-group row  ">
                                            <label for="description" class="col-sm-2 col-form-label">Mô tả <span
                                                    class="seo" title="SEO"><i class="fa fa-coffee" aria-hidden="true"></i></span></label>
                                            <div class="col-sm-8">
                                                <textarea type="text" id="description" name="description" class="form-control description" placeholder=""></textarea><span class="form-text"><i class="fa fa-info-circle"></i> Tối đa 300 kí tự
</span>
                                            </div>
                                        </div>

                                        <div class="form-group row ">
                                            <label for="parent" class="col-sm-2 col-form-label">Chọn danh mục</label>
                                            <div class="col-sm-8">
                                                <select class="form-control parent select2" style="width: 100%;"
                                                        name="parentid">
                                                    <option value=""></option>
                                                    <option value="0" selected>==ROOT==
                                                    </option>
                                                    @foreach($allcatnew as $item)

                                                        <option value="{!! $item->id !!}">{!! $item->categorynew !!}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group  row">
                                            <label for="status" class="col-sm-2 col-form-label">Trạng thái</label>
                                            <div class="col-sm-8">
                                            <select name="status" id="" class="form-control">
                                                <option value="1">Active</option>
                                                <option value="0">Block</option>
                                            </select>
                                            </div>
{{--                                            <div class="col-sm-8">--}}
{{--                                                <input class="checkbox" type="checkbox" name="status">--}}
{{--                                            </div>--}}
                                        </div>
                                    </div>

                                    <div class="card-footer row" id="card-footer">
                                        <div class="col-md-2">
                                        </div>
                                        <div class="col-md-8">
                                            <div class="btn-group float-right">
                                                <button type="submit" class="btn btn-primary">Gửi</button>
                                            </div>
                                            <div class="btn-group float-left">
                                                <button type="reset" class="btn btn-warning">Làm lại</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>

                        </div>
                    </div>
                </div>
        </section>

    </div>


@stop

