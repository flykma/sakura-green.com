@extends('backend.Layout.master')
@section('title','Comment Manager')
@section('content')
    <div class="content-wrapper">

        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">
                            <i class="fa fa-indent" aria-hidden="true"></i> Danh sách
                        </h1>
                        <div class="more_info"></div>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{!! route('admin') !!}"><i
                                        class="fa fa-home fa-1x"></i> Trang
                                    chủ</a></li>
                            <li class="breadcrumb-item active">Danh sách</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>


        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header with-border">
                                <div class="card-tools">
                                    <div class="menu-right">
                                        <form action="sc_admin/news" id="button_search">
                                            <div class="input-group input-group" style="width: 350px;">
                                                <input type="text" name="keyword"
                                                       class="form-control rounded-0 float-right"
                                                       placeholder="Tìm tiêu đề" value="">
                                                <div class="input-group-append">
                                                    <button type="submit" class="btn btn-primary"><i
                                                            class="fas fa-search"></i></button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                <div class="float-left">
                                </div>

                            </div>

                            <div class="card-body p-0" id="pjax-container">
                                <div id="url-sort" data-urlsort="sc_admin/news?" style="display: none;"></div>
                                <div class="table-responsive">
                                    <table class="table table-hover box-body text-wrap table-bordered">
                                        <thead>
                                        <tr>
                                            <th></th>
                                            <th>ID</th>
                                            <th>Họ tên</th>
                                            <th>Email</th>
                                            <th>Số điện thoại</th>
                                            <th>Địa chỉ</th>
                                            <th>Lời nhắn</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @if(isset($allcomment))
                                            <?php $i = 1; ?>
                                            @foreach($allcomment as $item)
                                                <tr>
                                                    <td>
                                                        <input class="checkbox grid-row-checkbox" type="checkbox"
                                                               data-id="{!! $item->id !!}">
                                                    </td>
                                                    <td>{!! $i !!}</td>
                                                    <td>{!! $item->name !!}</td>
                                                    <td>{!! $item->email !!}</td>

                                                    <td>{!! $item->phone !!}</td>

                                                    <td>{!! $item->address !!}</td>
                                                    <td>{!! $item->contents !!}</td>
                                                </tr>
                                                <?php $i++ ?>
                                            @endforeach()
                                        @endif
                                        </tbody>
                                    </table>
                                </div>

                            </div>

                            <div class="card-footer clearfix">
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </section>

    </div>
@stop
