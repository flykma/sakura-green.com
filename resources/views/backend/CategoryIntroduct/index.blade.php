@extends('backend.Layout.master')
@section('title','List Category Introduct')
@section('content')
    <div class="content-wrapper">

        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">
                            <i class="fa fa-indent" aria-hidden="true"></i> Danh sách danh mục
                        </h1>
                        <div class="more_info"></div>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{!! route('admin') !!}"><i class="fa fa-home fa-1x"></i> Trang chủ</a></li>
                            <li class="breadcrumb-item active">Danh sách danh mục</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        @if (session('message'))
            <div class="alert alert-success">
                {{ session('message') }}
            </div>
        @endif
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header with-border">
                                <div class="card-tools">
                                    <div class="menu-right">
                                        <form action="" id="button_search">
                                            <div class="input-group input-group" style="width: 350px;">
                                                <input type="text" name="keyword" class="form-control rounded-0 float-right" placeholder="Nhập từ khóa" value="">
                                                <div class="input-group-append">
                                                    <button type="submit" class="btn btn-primary"><i class="fas fa-search"></i></button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                <div class="float-left">
                                </div>

                            </div>
                            <div class="card-header with-border">
                                <div class="card-tools">
                                    <div class="menu-right">
                                        <a href="{!! route('getaddcatintro') !!}" class="btn  btn-success  btn-flat" title="New" id="button_create_new">
                                            <i class="fa fa-plus" title="action.add_new"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="float-left">
                                    <div class="menu-left">
                                        <button type="button" class="btn btn-default grid-select-all"><i class="far fa-square"></i></button>
                                    </div>
                                    <div class="menu-left">
                                        <span class="btn btn-flat btn-danger grid-trash" title="Xóa"><i class="fas fa-trash-alt"></i></span>
                                    </div>
                                    <div class="menu-left">
                                        <span class="btn btn-flat btn-primary grid-refresh" title="Làm mới"><i class="fas fa-sync-alt"></i></span>
                                    </div>
                                    <div class="menu-left">
                                        <div class="input-group float-right ml-1" style="width: 350px;">
                                            <div class="btn-group">
                                                <select class="form-control rounded-0 float-right" id="order_sort">
                                                    <option value="id__desc">ID giảm dần</option><option value="id__asc">ID tăng dần</option><option value="title__desc">Tiêu đề theo thứ tự z-a</option><option value="title__asc">Tiêu đề theo thứ tự a-z</option>
                                                </select>
                                            </div>
                                            <div class="input-group-append">
                                                <button id="button_sort" type="submit" class="btn btn-primary"><i class="fas fa-sort-amount-down-alt"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="card-body p-0" id="pjax-container">

                                <div class="table-responsive">
                                    <table class="table table-hover box-body text-wrap table-bordered">
                                        <thead>
                                        <tr>
                                            <th></th>
                                            <th>ID</th>
                                            <th>Tên</th>
                                            <th>Trạng thái</th>
                                            <th>Thao tác</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php $i =1 ?>
                                        @foreach($catintro as $item)
                                            <tr>
                                                <td>
                                                    <input class="checkbox grid-row-checkbox" type="checkbox" data-id="15">
                                                </td>
                                                <td>{!! $i !!}</td>
                                                <td>{!! $item->name	 !!}</td>
                                                <td><span class="badge badge-success">{!! $item->status == 1? "Active" : "Block" !!}</span></td>
                                                <td>
                                                    <a href="{!! route("geteditcatintro",$item->id) !!}"><span title="Chỉnh sửa" type="button" class="btn btn-flat btn-primary"><i class="fa fa-edit"></i></span></a>&nbsp;
                                                    <a href="{!! route("deletecatintro",$item->id) !!}"><span title="Chỉnh sửa" type="button" class="btn btn-flat btn-danger"><i class="fas fa-trash-alt"></i></span></a>

                                            </tr>
                                            {!! $i++ !!}
                                        @endforeach()
                                        </tbody>
                                    </table>
                                </div>
                                <div class="block-pagination clearfix m-10">
                                    <div class="ml-3 float-left">
                                        Hiển thị <b>1</b>-<b>15</b> của <b>15</b> kết quả</b>
                                    </div>
                                    <div class="pagination pagination-sm mr-3 float-right">
                                        <ul class="pagination pagination-sm no-margin pull-right">

                                            <li class="page-item disabled"><span class="page-link pjax-container">&laquo;</span></li>



                                            <li class="page-item active"><span class="page-link pjax-container">1</span></li>

                                            <li class="page-item disabled"><span class="page-link pjax-container">&raquo;</span></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>

                            <div class="card-footer clearfix">
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </section>

    </div>
@stop


