@extends('backend.Layout.master')
@section('title','Setting')
@section('content')
    <div class="content-wrapper">

        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">
                            <i class="fa fa-plus" aria-hidden="true"></i> Cài đặt trang
                        </h1>
                        <div class="more_info"></div>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{!! route('admin') !!}"><i
                                        class="fa fa-home fa-1x"></i> Trang
                                    chủ</a></li>
                            <li class="breadcrumb-item active">Cài đặt trang</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>


        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header with-border">
                                <h2 class="card-title">Setting</h2>
                            </div>
                            @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            @if(isset($setting))
                                <form action="{!! route('posteditsetting',$setting->id) !!}" method="post" accept-charset="UTF-8"
                                      class="form-horizontal" id="form-main" enctype="multipart/form-data">
                                    <div class="card-body">
                                        @csrf
                                        <div class="card">

                                            <div class="card-body">
                                                <div class="form-group  row ">
                                                    <label for="name" class="col-sm-2 col-form-label">Tên công ty <span
                                                            class="seo" title="SEO"><i class="fa fa-coffee"
                                                                                       aria-hidden="true"></i></span></label>
                                                    <div class="col-sm-8">
                                                        <div class="input-group">
                                                            <div class="input-group-prepend">
                                                            <span class="input-group-text"><i
                                                                    class="fas fa-pencil-alt"></i></span>
                                                            </div>
                                                            <input type="text" id="name" name="name"
                                                                   value="{!! $setting->company !!}" class="form-control name" placeholder=""/>
                                                        </div>
                                                        <span class="form-text">
<i class="fa fa-info-circle"></i> Tối đa 200 kí tự
</span>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="card-body">
                                                <div class="form-group  row ">
                                                    <label for="name" class="col-sm-2 col-form-label">Địa chỉ công ty<span
                                                            class="seo" title="SEO"><i class="fa fa-coffee"
                                                                                       aria-hidden="true"></i></span></label>
                                                    <div class="col-sm-8">
                                                        <div class="input-group">
                                                            <div class="input-group-prepend">
                                                            <span class="input-group-text"><i
                                                                    class="fas fa-pencil-alt"></i></span>
                                                            </div>
                                                            <input type="text" id="address" name="address"
                                                                   value="{!! $setting->address !!}" class="form-control link" placeholder=""/>
                                                        </div>
                                                        <span class="form-text">
<i class="fa fa-info-circle"></i> Tối đa 200 kí tự
</span>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="card-body">
                                                <div class="form-group  row ">
                                                    <label for="name" class="col-sm-2 col-form-label">Email<span
                                                            class="seo" title="SEO"><i class="fa fa-coffee"
                                                                                       aria-hidden="true"></i></span></label>
                                                    <div class="col-sm-8">
                                                        <div class="input-group">
                                                            <div class="input-group-prepend">
                                                            <span class="input-group-text"><i
                                                                    class="fas fa-pencil-alt"></i></span>
                                                            </div>
                                                            <input type="text" id="email" name="email"
                                                                   value="{!! $setting->email !!}" class="form-control link" placeholder=""/>
                                                        </div>
                                                        <span class="form-text">
<i class="fa fa-info-circle"></i> Tối đa 200 kí tự
</span>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="card-body">
                                                <div class="form-group  row ">
                                                    <label for="phone" class="col-sm-2 col-form-label">Số điện thoại công ty<span
                                                            class="seo" title="SEO"><i class="fa fa-coffee"
                                                                                       aria-hidden="true"></i></span></label>
                                                    <div class="col-sm-8">
                                                        <div class="input-group">
                                                            <div class="input-group-prepend">
                                                            <span class="input-group-text"><i
                                                                    class="fas fa-pencil-alt"></i></span>
                                                            </div>
                                                            <input type="number" id="phone" name="phone"
                                                                   value="{!! $setting->phone !!}" class="form-control phone" placeholder=""/>
                                                        </div>
                                                        <span class="form-text">
<i class="fa fa-info-circle"></i> Tối đa 200 kí tự
</span>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="card-body">
                                                <div class="form-group  row ">
                                                    <label for="phone" class="col-sm-2 col-form-label">Link Facebook công ty<span
                                                            class="seo" title="SEO"><i class="fa fa-coffee"
                                                                                       aria-hidden="true"></i></span></label>
                                                    <div class="col-sm-8">
                                                        <div class="input-group">
                                                            <div class="input-group-prepend">
                                                            <span class="input-group-text"><i
                                                                    class="fas fa-pencil-alt"></i></span>
                                                            </div>
                                                            <input type="text" id="facebook" name="facebook"
                                                                   value="{!! $setting->facebook !!}" class="form-control facebook" placeholder=""/>
                                                        </div>
                                                        <span class="form-text">
<i class="fa fa-info-circle"></i> Tối đa 200 kí tự
</span>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="card-body">
                                                <div class="form-group  row ">
                                                    <label for="phone" class="col-sm-2 col-form-label">Link Youtube công ty<span
                                                            class="seo" title="SEO"><i class="fa fa-coffee"
                                                                                       aria-hidden="true"></i></span></label>
                                                    <div class="col-sm-8">
                                                        <div class="input-group">
                                                            <div class="input-group-prepend">
                                                            <span class="input-group-text"><i
                                                                    class="fas fa-pencil-alt"></i></span>
                                                            </div>
                                                            <input type="text" id="youtube" name="youtube"
                                                                   value="{!! $setting->youtube !!}" class="form-control facebook" placeholder=""/>
                                                        </div>
                                                        <span class="form-text">
<i class="fa fa-info-circle"></i> Tối đa 200 kí tự
</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group  row ">
                                            <label for="image" class="col-sm-2 col-form-label">Logo công ty</label>
                                            <div class="col-sm-8">
                                                <div class="input-group">
                                                    <input type="file" id="image" name="image" value=""
                                                           class="form-control input-sm image" placeholder=""/>

                                                </div>

                                            </div>
                                        </div>

                                        <div class="form-group  row ">
                                            <label for="image" class="col-sm-2 col-form-label">Background Footer</label>
                                            <div class="col-sm-8">
                                                <div class="input-group">
                                                    <input type="file" id="bg_footer" name="bg_footer" value=""
                                                           class="form-control input-sm image" placeholder=""/>

                                                </div>

                                            </div>
                                        </div>

                                    </div>

                                    <div class="card-footer row">
                                        <div class="col-md-2">
                                        </div>
                                        <div class="col-md-8">
                                            <div class="btn-group float-right">
                                                <button type="submit" class="btn btn-primary">Gửi</button>
                                            </div>
                                            <div class="btn-group float-left">
                                                <button type="reset" class="btn btn-warning">Làm lại</button>
                                            </div>
                                        </div>
                                    </div>

                                </form>
                            @else
                                <form action="{!! route('postsetting') !!}" method="post" accept-charset="UTF-8"
                                      class="form-horizontal" id="form-main" enctype="multipart/form-data">
                                    <div class="card-body">
                                        @csrf
                                        <div class="card">

                                            <div class="card-body">
                                                <div class="form-group  row ">
                                                    <label for="name" class="col-sm-2 col-form-label">Tên công ty <span
                                                            class="seo" title="SEO"><i class="fa fa-coffee"
                                                                                       aria-hidden="true"></i></span></label>
                                                    <div class="col-sm-8">
                                                        <div class="input-group">
                                                            <div class="input-group-prepend">
                                                            <span class="input-group-text"><i
                                                                    class="fas fa-pencil-alt"></i></span>
                                                            </div>
                                                            <input type="text" id="name" name="name"
                                                                   value="" class="form-control name" placeholder=""/>
                                                        </div>
                                                        <span class="form-text">
<i class="fa fa-info-circle"></i> Tối đa 200 kí tự
</span>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="card-body">
                                                <div class="form-group  row ">
                                                    <label for="name" class="col-sm-2 col-form-label">Địa chỉ công ty<span
                                                            class="seo" title="SEO"><i class="fa fa-coffee"
                                                                                       aria-hidden="true"></i></span></label>
                                                    <div class="col-sm-8">
                                                        <div class="input-group">
                                                            <div class="input-group-prepend">
                                                            <span class="input-group-text"><i
                                                                    class="fas fa-pencil-alt"></i></span>
                                                            </div>
                                                            <input type="text" id="address" name="address"
                                                                   value="" class="form-control link" placeholder=""/>
                                                        </div>
                                                        <span class="form-text">
<i class="fa fa-info-circle"></i> Tối đa 200 kí tự
</span>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="card-body">
                                                <div class="form-group  row ">
                                                    <label for="phone" class="col-sm-2 col-form-label">Số điện thoại công ty<span
                                                            class="seo" title="SEO"><i class="fa fa-coffee"
                                                                                       aria-hidden="true"></i></span></label>
                                                    <div class="col-sm-8">
                                                        <div class="input-group">
                                                            <div class="input-group-prepend">
                                                            <span class="input-group-text"><i
                                                                    class="fas fa-pencil-alt"></i></span>
                                                            </div>
                                                            <input type="number" id="phone" name="phone"
                                                                   value="" class="form-control phone" placeholder=""/>
                                                        </div>
                                                        <span class="form-text">
<i class="fa fa-info-circle"></i> Tối đa 200 kí tự
</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="card-body">
                                                <div class="form-group  row ">
                                                    <label for="phone" class="col-sm-2 col-form-label">Link Facebook công ty<span
                                                            class="seo" title="SEO"><i class="fa fa-coffee"
                                                                                       aria-hidden="true"></i></span></label>
                                                    <div class="col-sm-8">
                                                        <div class="input-group">
                                                            <div class="input-group-prepend">
                                                            <span class="input-group-text"><i
                                                                    class="fas fa-pencil-alt"></i></span>
                                                            </div>
                                                            <input type="text" id="facebook" name="facebook"
                                                                   value="" class="form-control facebook" placeholder=""/>
                                                        </div>
                                                        <span class="form-text">
<i class="fa fa-info-circle"></i> Tối đa 200 kí tự
</span>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="card-body">
                                                <div class="form-group  row ">
                                                    <label for="phone" class="col-sm-2 col-form-label">Link Youtube công ty<span
                                                            class="seo" title="SEO"><i class="fa fa-coffee"
                                                                                       aria-hidden="true"></i></span></label>
                                                    <div class="col-sm-8">
                                                        <div class="input-group">
                                                            <div class="input-group-prepend">
                                                            <span class="input-group-text"><i
                                                                    class="fas fa-pencil-alt"></i></span>
                                                            </div>
                                                            <input type="text" id="youtube" name="youtube"
                                                                   value="" class="form-control facebook" placeholder=""/>
                                                        </div>
                                                        <span class="form-text">
<i class="fa fa-info-circle"></i> Tối đa 200 kí tự
</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group  row ">
                                            <label for="image" class="col-sm-2 col-form-label">Logo công ty</label>
                                            <div class="col-sm-8">
                                                <div class="input-group">
                                                    <input type="file" id="image" name="image" value=""
                                                           class="form-control input-sm image" placeholder=""/>

                                                </div>

                                            </div>
                                        </div>

                                        <div class="form-group  row ">
                                            <label for="image" class="col-sm-2 col-form-label">Background Footer</label>
                                            <div class="col-sm-8">
                                                <div class="input-group">
                                                    <input type="file" id="bg_footer" name="bg_footer" value=""
                                                           class="form-control input-sm bg_footer" placeholder=""/>

                                                </div>

                                            </div>
                                        </div>

                                    </div>

                                    <div class="card-footer row">
                                        <div class="col-md-2">
                                        </div>
                                        <div class="col-md-8">
                                            <div class="btn-group float-right">
                                                <button type="submit" class="btn btn-primary">Gửi</button>
                                            </div>
                                            <div class="btn-group float-left">
                                                <button type="reset" class="btn btn-warning">Làm lại</button>
                                            </div>
                                        </div>
                                    </div>

                                </form>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </section>

    </div>
@stop

