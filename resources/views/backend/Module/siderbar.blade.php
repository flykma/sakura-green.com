<aside class="main-sidebar sidebar-light-pink elevation-4 sidebar-no-expand">
    <!-- Brand Logo -->
    <a href="{!! route('admin') !!}" class="brand-link navbar-secondary">
        Sakura-green
    </a>

    <!-- Sidebar -->
    <div class="sidebar sidebar-lightblue">

        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column nav-legacy" data-widget="treeview" role="menu">

                <li class="nav-link header">
                    <i class="nav-icon  fas fa-users"></i>
                    <p class="sub-header">Quản trị tài khoản</p>
                </li>

                <li class="nav-item ">
                    <a href="{!! route('listuser') !!}" class="nav-link">
                        <i class="fas fa-user nav-icon"></i>
                        <p>Danh sách tài khoản</p>
                    </a>
                </li>

                <li class="nav-item ">
                    <a href="{!! route('getadduser') !!}" class="nav-link">
                        <i class="fas fa-user-circle nav-icon"></i>
                        <p>Đăng ký</p>
                    </a>
                </li>
                {{--                    </ul>--}}
                {{--                </li>--}}

                <li class="nav-link header">
                    <i class="nav-icon  fas fa-file-signature "></i>
                    <p class="sub-header"> Nội dung</p>
                </li>

                <li class="nav-item ">
                    <a href="{!! route('listcatnew') !!}" class="nav-link">
                        <i class="nav-icon fas fa-clone"></i>
                        <p>
                            Danh mục tin tức
                        </p>
                    </a>
                </li>
                <li class="nav-item ">
                    <a href="{!! route('listnew') !!}" class="nav-link">
                        <i class="nav-icon far fa-file-powerpoint"></i>
                        <p>
                            Tin tức
                        </p>
                    </a>
                </li>

                <li class="nav-link header">
                    <i class="nav-icon  fab fa-shopify "></i>
                    <p class="sub-header"> Cửa hàng</p>
                </li>

                <li class="nav-item ">
                    <a href="{!! route('listcatpro') !!}" class="nav-link">
                        <i class="fas fa-folder-open nav-icon"></i>
                        <p>Danh mục sản phẩm</p>
                    </a>
                </li>
                <li class="nav-item ">
                    <a href="{!! route('listpro') !!}" class="nav-link">
                        <i class="far fa-file-image nav-icon"></i>
                        <p>Sản phẩm</p>
                    </a>
                </li>

                {{--                    </ul>--}}
                {{--                </li>--}}
                <li class="nav-item has-treeview">
                    <a href="#" class="nav-link">
                        <i class="nav-icon  fas fa-cart-arrow-down "></i>
                        <p>
                            Quản lý đơn hàng
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>

                    <ul class="nav nav-treeview">
                        <li class="nav-item ">
                            <a href="http://localhost:8080/s-cart/public/sc_admin/order" class="nav-link">
                                <i class="fas fa-shopping-cart nav-icon"></i>
                                <p>Đơn hàng</p>
                            </a>
                        </li>
                    </ul>
                </li>

                <li class="nav-link header">
                    <i class="nav-icon  fas fa-info"></i>
                    <p class="sub-header">Giới thiệu</p>
                </li>

                <li class="nav-item ">
                    <a href="{!! route('listcatintro') !!}" class="nav-link">
                        <i class="fas fa-info nav-icon"></i>
                        <p>Dạnh mục</p>
                    </a>
                </li>

                <li class="nav-item has-treeview">
                    <a href="#" class="nav-link">
                        <i class="fas fa-info nav-icon"></i>
                        <p>

                            Bài viết
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>

                    <ul class="nav nav-treeview">
                        <?php $intro = \App\CategoryIntroduct::where('status', 1)->get(); ?>
                        @foreach($intro as $itemcatintro)
                            <li class="nav-item ">
                                <a href="{!! route('getaddintro',$itemcatintro->id) !!}" class="nav-link">
                                    <i class="fas fa-check-square nav-icon"></i>
                                    <p>{!! $itemcatintro->name !!}</p>
                                </a>
                            </li>
                        @endforeach
                    </ul>
                </li>

                <li class="nav-link header">
                    <i class="nav-icon  fa fa-file-code"></i>
                    <p class="sub-header">Kiến thức</p>
                </li>

                <li class="nav-item ">
                    <a href="{!! route('listcatknowledge') !!}" class="nav-link">
                        <i class="fas fa-info nav-icon"></i>
                        <p>Dạnh mục</p>
                    </a>
                </li>

                <li class="nav-item has-treeview">
                    <a href="#" class="nav-link">
                        <i class="fas fa-info nav-icon"></i>
                        <p>

                            Bài viết
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>

                    <ul class="nav nav-treeview">
                        <?php $know = \App\CategoryKnowledge::where('status', 1)->get(); ?>
                        @foreach($know as $itemcatknow)
                            <li class="nav-item ">
                                <a href="{!! route('getaddknowledge',$itemcatknow->id) !!}" class="nav-link">
                                    <i class="fas fa-check-square nav-icon"></i>
                                    <p>{!! $itemcatknow->name !!}</p>
                                </a>
                            </li>
                        @endforeach
                    </ul>
                </li>


                <li class="nav-link header">
                    <i class="nav-icon  fas fa-sort-amount-up "></i>
                    <p class="sub-header"> Danh mục khác</p>
                </li>
                <li class="nav-item ">
                    <a href="{!! route('listvideo') !!}" class="nav-link">
                        <i class="fas fa-bars nav-icon"></i>
                        <p>Quản lý VIDEOS</p>
                    </a>
                </li>

                <li class="nav-item ">
                    <a href="{!! route('listslider') !!}" class="nav-link">
                        <i class="fas fa-bars nav-icon"></i>
                        <p>Quản lý Slider</p>
                    </a>
                </li>

                <li class="nav-item ">
                    <a href="{!! route('comment') !!}" class="nav-link">
                        <i class="fas fa-bars nav-icon"></i>
                        <p>Danh sách liên hệ</p>
                    </a>
                </li>

                <li class="nav-item ">
                    <a href="{!! route('setting') !!}" class="nav-link">
                        <i class="fas fa-bars nav-icon"></i>
                        <p>SETTING PAGES</p>
                    </a>
                </li>

                <li class="nav-item has-treeview">
                    <a href="#" class="nav-link">
                        <i class="nav-icon  fas fa-chart-pie "></i>
                        <p>
                            Quản lý báo cáo
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>

                    <ul class="nav nav-treeview">
                        <li class="nav-item ">
                            <a href="" class="nav-link">
                                <i class="fas fa-bars nav-icon"></i>
                                <p>Báo cáo sản phẩm</p>
                            </a>
                        </li>
                    </ul>
                </li>


            </ul>

        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
