@extends('backend.Layout.master')
@section('title','List Product')
@section('content')
    <div class="content-wrapper">

        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">
                            <i class="fa fa-indent" aria-hidden="true"></i> Danh sách sản phẩm
                        </h1>
                        <div class="more_info"></div>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{!! route('admin') !!}"><i
                                        class="fa fa-home fa-1x"></i> Trang chủ</a></li>
                            <li class="breadcrumb-item active">Danh sách sản phẩm</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>


        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header with-border">
                                <div class="card-tools">
                                    <div class="menu-right">
                                        <form action="" id="button_search">
                                            <div class="input-group input-group float-left">

                                                <input type="text" name="keyword"
                                                       class="form-control rounded-0 float-right"
                                                       placeholder="Tìm tên và SKU" value="">
                                                <div class="input-group-append">
                                                    <button type="submit" class="btn btn-primary"><i
                                                            class="fas fa-search"></i></button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                <div class="float-left">
                                </div>

                            </div>
                            <div class="card-header with-border">
                                <div class="card-tools">
                                    <div class="menu-right">
                                        <a href="{!! route('getaddpro') !!}" class="btn btn-success btn-flat"
                                           title="Tạo sản phẩm"
                                           id="button_create_new">
                                            <i class="fa fa-plus"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="float-left">
                                    <div class="menu-left">
                                        <button type="button" class="btn btn-default grid-select-all"><i
                                                class="far fa-square"></i></button>
                                    </div>
                                    <div class="menu-left">
                                        <span class="btn btn-flat btn-danger grid-trash" title="Xóa"><i
                                                class="fas fa-trash-alt"></i></span>
                                    </div>
                                    <div class="menu-left">
                                        <span class="btn btn-flat btn-primary grid-refresh" title="Làm mới"><i
                                                class="fas fa-sync-alt"></i></span>
                                    </div>
                                    <div class="menu-left">
                                        <div class="input-group float-right ml-1" style="width: 350px;">
                                            <div class="btn-group">
                                                <select class="form-control rounded-0 float-right" id="order_sort">
                                                    <option value="id__desc">ID giảm dần</option>
                                                    <option value="id__asc">ID tăng dần</option>
                                                    <option value="name__desc">Tên theo thứ tự z-a</option>
                                                    <option value="name__asc">Tên theo thứ tự a-z</option>
                                                </select>
                                            </div>
                                            <div class="input-group-append">
                                                <button id="button_sort" type="submit" class="btn btn-primary"><i
                                                        class="fas fa-sort-amount-down-alt"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="card-body p-0" id="pjax-container">
                                <div id="url-sort" data-urlsort="sc_admin/product?" style="display: none;"></div>
                                <div class="table-responsive">
                                    <table class="table table-hover box-body text-wrap table-bordered">
                                        <thead>
                                        <tr>
                                            <th></th>
                                            <th>ID</th>
                                            <th>Hình ảnh</th>
                                            <th>Tên</th>
                                            <th>Mã sản phẩm</th>
                                            <th>Danh mục</th>
                                            <th>Giá</th>
                                            <th>Giá giảm</th>
                                            <th>Mô tả ngắn sản phẩm</th>
                                            <th>Trạng thái</th>
                                            <th>Hiển thị trang chủ</th>
                                            <th>Thao tác</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php $i = 1; ?>
                                        @foreach($allpro as $item)
                                            <tr>
                                                <td>
                                                    <input class="checkbox grid-row-checkbox" type="checkbox"
                                                           data-id="{!! $item->id !!}">
                                                </td>
                                                <td>{!! $i !!}</td>
                                                <td><img alt="{!! $item->productname !!}"
                                                         title="{!! $item->productname !!}"
                                                         src="{!! asset('uploads/product/'.$item->photo) !!}"
                                                         style=" width:50px; height:50px;"></td>
                                                <td>{!! $item->productname !!}</td>
                                                <td>{!! $item->symbol !!}</td>
                                                <td>
                                                    <?php $catpro = \App\CategoryProduct::find($item->catpro_id);
                                                    echo $catpro->categoryName;
                                                    ?>
                                                </td>
                                                <td>{!! $item->price !!}</td>
                                                <td>{!! $item->discount !!}</td>
                                                <td>{!! substr($item->description,0,50) !!}...</td>

                                                <td><span
                                                        class="badge badge-success">{!! $item->status == 1? "Bài đã được duyệt" : "Bài chưa được duyệt" !!}</span>
                                                </td>
                                                <td><span class="badge badge-success">{!! $item->showhome == 1? "ON" : "OFF" !!}</span></td>
                                                <td>
                                                    <a href="{!! route('geteditpro',$item->id) !!}">
<span title="Sửa" type="button" class="btn btn-flat btn-primary">
 <i class="fa fa-edit"></i>
</span>
                                                    </a>&nbsp;
                                                    <a href="{!! route('deletepro',$item->id) !!}">
<span title="Xóa" type="button" class="btn btn-flat btn-danger">
 <i class="fas fa-trash-alt"></i>
</span>
                                                    </a>&nbsp;
                                                </td>
                                            </tr>
                                            {!! $i++ !!}
                                        @endforeach()
                                        </tbody>
                                    </table>
                                </div>

                            </div>

                            <div class="card-footer clearfix">
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </section>

    </div>
@stop
