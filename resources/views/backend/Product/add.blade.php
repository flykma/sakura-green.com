@extends('backend.Layout.master')
@section('title','Add Product')
@section('content')
    <div class="content-wrapper">

        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">
                            <i class="fa fa-plus" aria-hidden="true"></i> Tạo sản phẩm
                        </h1>
                        <div class="more_info"></div>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{!! route('admin') !!}"><i
                                        class="fa fa-home fa-1x"></i> Trang chủ</a></li>
                            <li class="breadcrumb-item active">Tạo sản phẩm</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header with-border">
                                <h2 class="card-title">Tạo mới một sản phẩm</h2>
                                <div class="card-tools">
                                    <div class="btn-group float-right mr-5">
                                        <a target=_new href="{!! route('listpro') !!}" class="btn  btn-flat btn-default"
                                           title="List">
                                            <i class="fa fa-list"></i><span class="hidden-xs"> Trở lại danh sách</span>
                                        </a>
                                    </div>
                                </div>
                            </div>

                            @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            <form action="{!! route('postaddpro') !!}" method="post" name="form_name"
                                  accept-charset="UTF-8" class="form-horizontal" id="form-main"
                                  enctype="multipart/form-data">
                                @csrf
                                <div id="main-add" class="card-body">

                                    <div class="card">
                                        <div class="card-body">
                                            <div class="form-group row ">
                                                <label for="vi__name" class="col-sm-2 col-form-label">Tên sản phẩm<span
                                                        class="seo" title="SEO"><i class="fa fa-coffee"
                                                                                   aria-hidden="true"></i></span>
                                                </label>
                                                <div class="col-sm-8">
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text"><i
                                                                    class="fas fa-pencil-alt"></i></span>
                                                        </div>
                                                        <input type="text" id="vi__name" name="nameproduct" value=""
                                                               class="form-control input-sm vi__name" placeholder=""/>
                                                    </div>
                                                    <span class="form-text">
<i class="fa fa-info-circle"></i> Tối đa 200 kí tự
</span>
                                                </div>
                                            </div>
                                            <div class="form-group row kind  ">
                                                <label for="sku" class="col-sm-2 col-form-label">Mã sản phẩm</label>
                                                <div class="col-sm-8">
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                    <span class="input-group-text"><i
                                                            class="fas fa-pencil-alt"></i></span>
                                                        </div>
                                                        <input type="text" style="width: 100px;" id="sku" name="symbol"
                                                               value=""
                                                               class="form-control input-sm sku" placeholder=""/>
                                                    </div>
                                                    <span class="form-text">
Chỉ sử dụng kí tự trong nhóm: &quot;A-Z&quot;, &quot;a-z&quot;, &quot;0-9&quot; and &quot;-_&quot;
</span>
                                                </div>
                                            </div>
                                            <div class="form-group row kind   ">
                                                <label for="price" class="col-sm-2 col-form-label">Giá sản phẩm</label>
                                                <div class="col-sm-8">
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                    <span class="input-group-text"><i
                                                            class="fas fa-pencil-alt"></i></span>
                                                        </div>
                                                        <input type="number" style="width: 100px;" id="price"
                                                               name="price"
                                                               value="0" class="form-control input-sm price"
                                                               placeholder=""/>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group row kind   ">
                                                <label for="cost" class="col-sm-2 col-form-label">Giảm giá</label>
                                                <div class="col-sm-8">
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                    <span class="input-group-text"><i
                                                            class="fas fa-pencil-alt"></i></span>
                                                        </div>
                                                        <input type="number" style="width: 100px;" id="cost"
                                                               name="discount"
                                                               value="0" class="form-control input-sm cost"
                                                               placeholder=""/>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group row   ">
                                                <label for="vi__keyword" class="col-sm-2 col-form-label">Từ khóa <span
                                                        class="seo" title="SEO"><i class="fa fa-coffee"
                                                                                   aria-hidden="true"></i></span></label>
                                                <div class="col-sm-8">
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text"><i
                                                                    class="fas fa-pencil-alt"></i></span>
                                                        </div>
                                                        <input type="text" id="seotitle" name="seotitle" value=""
                                                               class="form-control input-sm seotitle" placeholder=""/>
                                                    </div>
                                                    <span class="form-text">
<i class="fa fa-info-circle"></i> Tối đa 200 kí tự
</span>
                                                </div>
                                            </div>

                                            <div class="form-group row   ">
                                                <label for="vi__keyword" class="col-sm-2 col-form-label">Thẻ Tag <span
                                                        class="seo" title="SEO"><i class="fa fa-coffee"
                                                                                   aria-hidden="true"></i></span></label>
                                                <div class="col-sm-8">
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text"><i
                                                                    class="fas fa-pencil-alt"></i></span>
                                                        </div>
                                                        <input type="text" id="tag" name="tag" value=""
                                                               class="form-control input-sm tag" placeholder=""/>
                                                    </div>
                                                    <span class="form-text">
<i class="fa fa-info-circle"></i> Tối đa 200 kí tự
</span>
                                                </div>
                                            </div>
                                            <div class="form-group row ">
                                                <label for="description" class="col-sm-2 col-form-label">Mô tả <span
                                                        class="seo" title="SEO"><i class="fa fa-coffee"
                                                                                   aria-hidden="true"></i></span></label>
                                                <div class="col-sm-8">
                                                    <textarea id="descriptions" name="descriptions"
                                                              class="form-control input-sm descriptions"
                                                              placeholder=""></textarea>
                                                    <span class="form-text">
<i class="fa fa-info-circle"></i> Tối đa 300 kí tự
</span>
                                                </div>
                                            </div>
                                            <div class="form-group row kind  ">
                                                <label for="vi__content" class="col-sm-2 col-form-label">
                                                    công dụng
                                                </label>
                                                <div class="col-sm-8">
<textarea id="contents" class="contents" name="contents">

                                    </textarea>
                                                </div>
                                            </div>

                                            <div class="form-group row kind  ">
                                                <label for="vi__content" class="col-sm-2 col-form-label">
                                                    Hướng dẫn sử dụng
                                                </label>
                                                <div class="col-sm-8">
<textarea id="guide" class="guide" name="guide">

                                    </textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row kind  ">
                                        <label for="category" class="col-sm-2 col-form-label">
                                            Chọn danh mục
                                        </label>
                                        <div class="col-sm-8">

                                            <select class="form-control input-sm category select2"
                                                    data-placeholder="Chọn danh mục" style="width: 100%;"
                                                    name="category">
                                                <option value=""></option>
                                                @foreach($allcatpro as $item)
                                                    <option
                                                        value="{!! $item->id !!}">{!! $item->categoryName !!}</option>
                                                    <?php $subcat = \App\CategoryProduct::where('parentid', $item->id)->get(); ?>
                                                    @foreach($subcat as $subitem)
                                                        <option value="{!! $subitem->id !!}">
                                                            ---- {!! $subitem->categoryName !!}</option>
                                                    @endforeach
                                                @endforeach()
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group row kind  ">
                                        <label for="image" class="col-sm-2 col-form-label">
                                            Hình ảnh
                                        </label>
                                        <div class="col-sm-8">
                                            <div class="input-group">
                                                <input type="file" id="image" name="photo" value=""
                                                       class="form-control input-sm photo" placeholder=""/>

                                            </div>

                                        </div>
                                    </div>

                                    <div class="form-group row kind  ">
                                        <label for="image" class="col-sm-2 col-form-label">
                                            Thêm Hình ảnh
                                        </label>
                                        <div class="col-sm-8">
                                            <div class="input-group">
                                                <input type="file" id="image" name="images[]" value=""
                                                       class="form-control input-sm images" placeholder="" multiple/>

                                            </div>

                                        </div>
                                    </div>


                                    <div class="form-group row   ">
                                        <label for="sort" class="col-sm-2 col-form-label">Hiển thị trang chủ</label>
                                        <div class="col-sm-8">
                                            <select class="form-control input-sm category select2"
                                                    data-placeholder="Chọn danh mục" style="width: 100%;"
                                                    name="onhome">
                                                <option value="1">Hiển thị trang chủ</option>
                                                <option value="0">Không hiển thị</option>

                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group row   ">
                                        <label for="sort" class="col-sm-2 col-form-label">Bài viết HOT</label>
                                        <div class="col-sm-8">
                                            <select class="form-control input-sm category select2"
                                                    data-placeholder="Chọn danh mục" style="width: 100%;"
                                                    name="hot">
                                                <option value="1">Bài HOT</option>
                                                <option value="0">Bài viết thường</option>

                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row ">
                                        <label for="status" class="col-sm-2 col-form-label">Trạng thái</label>
                                        <div class="col-sm-8">
                                            <select class="form-control input-sm category select2"
                                                    data-placeholder="Chọn danh mục" style="width: 100%;"
                                                    name="status">
                                                <option value="1">Duyệt bài viết</option>
                                                <option value="0">Chưa duyệt bài</option>

                                            </select>
                                        </div>
                                    </div>
                                    <hr class="kind ">


                                </div>

                                <div class="card-footer kind row" id="card-footer">
                                    <div class="col-md-2">
                                    </div>
                                    <div class="col-md-8">
                                        <div class="btn-group float-right">
                                            <button type="submit" class="btn btn-primary">Gửi</button>
                                        </div>
                                        <div class="btn-group float-left">
                                            <button type="reset" class="btn btn-warning">Làm lại</button>
                                        </div>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>

    </div>
@stop
@section('script')
    <script src="{!! asset('admin/plugin/ckeditor/ckeditor.js') !!}"></script>
    <script src="{!! asset('admin/plugin/ckfinder/ckfinder.js') !!}"></script>
    <script>
        CKEDITOR.replace('contents', {
            filebrowserBrowseUrl: '{{ asset('admin/plugin/ckfinder/ckfinder.html') }}',
            filebrowserImageBrowseUrl: '{{ asset('admin/plugin/ckfinder/ckfinder.html?type=Images') }}',
            filebrowserFlashBrowseUrl: '{{ asset('admin/plugin/ckfinder/ckfinder.html?type=Flash') }}',
            filebrowserUploadUrl: '{{ asset('admin/plugin/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files') }}',
            filebrowserImageUploadUrl: '{{ asset('admin/plugin/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images') }}',
            filebrowserFlashUploadUrl: '{{ asset('admin/plugin/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash') }}'
        });

        CKEDITOR.replace('guide', {
            filebrowserBrowseUrl: '{{ asset('admin/plugin/ckfinder/ckfinder.html') }}',
            filebrowserImageBrowseUrl: '{{ asset('admin/plugin/ckfinder/ckfinder.html?type=Images') }}',
            filebrowserFlashBrowseUrl: '{{ asset('admin/plugin/ckfinder/ckfinder.html?type=Flash') }}',
            filebrowserUploadUrl: '{{ asset('admin/plugin/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files') }}',
            filebrowserImageUploadUrl: '{{ asset('admin/plugin/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images') }}',
            filebrowserFlashUploadUrl: '{{ asset('admin/plugin/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash') }}'
        });

    </script>


@stop
