@extends('backend.Layout.master')
@section('title','Thêm bài viết giới thiệu')
@section('content')
    <div class="content-wrapper">

        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">
                            <i class="fa fa-plus" aria-hidden="true"></i> Giới thiệu công ty
                        </h1>
                        <div class="more_info"></div>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{!! route('admin') !!}"><i
                                        class="fa fa-home fa-1x"></i> Trang
                                    chủ</a></li>
                            <li class="breadcrumb-item active">Giới thiệu về công ty</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header with-border">
                                <h2 class="card-title">Giới thiệu</h2>
                            </div>
                            @if(isset($introduct))
                                <form action="{!! route('posteditintro',$catintro->id) !!}" method="post"
                                      accept-charset="UTF-8"
                                      class="form-horizontal" id="form-main" enctype="multipart/form-data">
                                    <div class="card-body">
                                        @csrf
                                        <div class="card">

                                            <div class="card-body">
                                                <div class="form-group  row ">
                                                    <label for="sort" class="col-sm-2 col-form-label">Thuộc anh
                                                        mục</label>
                                                    <div class="col-sm-8">
                                                        <div class="input-group">
                                                            <select class="form-control shop_store select2"
                                                                    data-placeholder="Trạng thái bài viết"
                                                                    style="width: 100%;"
                                                                    name="category">
                                                                @if(isset($catintro))

                                                                    <option
                                                                        value="{!! $catintro->id !!}"
                                                                        selected>{!! $catintro->name !!}</option>

                                                                @endif

                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-group row ">
                                                    <label for="content" class="col-sm-2 col-form-label">Nội
                                                        dung giới thiệu công ty</label>
                                                    <div class="col-sm-8">
<textarea class="contents" name="contents" id="contents">
                                    {!! $introduct->contents !!}
                                    </textarea>
                                                    </div>
                                                </div>

                                            </div>


                                        </div>


                                    </div>
                                    <div class="card-footer row">
                                        <div class="col-md-2">
                                        </div>
                                        <div class="col-md-8">
                                            <div class="btn-group float-right">
                                                <button type="submit" class="btn btn-primary">Gửi</button>
                                            </div>
                                            <div class="btn-group float-left">
                                                <button type="reset" class="btn btn-warning">Làm lại</button>
                                            </div>
                                        </div>
                                    </div>

                                </form>
                            @else

                                <form action="{!! route('postaddintro',$catintro->id) !!}" method="post"
                                      accept-charset="UTF-8"
                                      class="form-horizontal" id="form-main" enctype="multipart/form-data">
                                    <div class="card-body">
                                        @csrf
                                        <div class="card">

                                            <div class="card-body">
                                                <div class="form-group  row ">
                                                    <label for="sort" class="col-sm-2 col-form-label">Thuộc anh
                                                        mục</label>
                                                    <div class="col-sm-8">
                                                        <div class="input-group">
                                                            <select class="form-control shop_store select2"
                                                                    data-placeholder="Trạng thái bài viết"
                                                                    style="width: 100%;"
                                                                    name="category">
                                                                <option value=""></option>
                                                                @if(isset($catintro))

                                                                    <option
                                                                        value="{!! $catintro->id !!}"
                                                                        selected>{!! $catintro->name !!}</option>

                                                                @endif

                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-group row ">
                                                    <label for="content" class="col-sm-2 col-form-label">Nội
                                                        dung giới thiệu công ty</label>
                                                    <div class="col-sm-8">
<textarea class="contents" name="contents" id="contents">

                                    </textarea>
                                                    </div>
                                                </div>

                                            </div>


                                        </div>


                                    </div>
                                    <div class="card-footer row">
                                        <div class="col-md-2">
                                        </div>
                                        <div class="col-md-8">
                                            <div class="btn-group float-right">
                                                <button type="submit" class="btn btn-primary">Gửi</button>
                                            </div>
                                            <div class="btn-group float-left">
                                                <button type="reset" class="btn btn-warning">Làm lại</button>
                                            </div>
                                        </div>
                                    </div>

                                </form>
                            @endif

                        </div>
                    </div>
                </div>
            </div>
        </section>

    </div>
@stop
@section('script')
    <script src="{!! asset('admin/plugin/ckeditor/ckeditor.js') !!}"></script>
    <script src="{!! asset('admin/plugin/ckfinder/ckfinder.js') !!}"></script>
    <script>
        CKEDITOR.replace('contents', {
            filebrowserBrowseUrl: '{{ asset('admin/plugin/ckfinder/ckfinder.html') }}',
            filebrowserImageBrowseUrl: '{{ asset('admin/plugin/ckfinder/ckfinder.html?type=Images') }}',
            filebrowserFlashBrowseUrl: '{{ asset('admin/plugin/ckfinder/ckfinder.html?type=Flash') }}',
            filebrowserUploadUrl: '{{ asset('admin/plugin/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files') }}',
            filebrowserImageUploadUrl: '{{ asset('admin/plugin/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images') }}',
            filebrowserFlashUploadUrl: '{{ asset('admin/plugin/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash') }}'
        });

    </script>

@stop
