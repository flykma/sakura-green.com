@extends('backend.Layout.master')
@section('title','Edit News')
@section('content')
    <div class="content-wrapper">

        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">
                            <i class="fa fa-plus" aria-hidden="true"></i> Tạo Blog/News
                        </h1>
                        <div class="more_info"></div>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{!! route('admin') !!}"><i
                                        class="fa fa-home fa-1x"></i> Trang
                                    chủ</a></li>
                            <li class="breadcrumb-item active">Sửa Blog/News</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>


        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header with-border">
                                <h2 class="card-title">Sửa một Blog/News</h2>
                                <div class="card-tools">
                                    <div class="btn-group float-right mr-5">
                                        <a href="{!! route('listnew') !!}" class="btn  btn-flat btn-default"
                                           title="List"><i
                                                class="fa fa-list"></i><span class="hidden-xs"> Trở lại danh sách</span></a>
                                    </div>
                                </div>
                            </div>
                            @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif

                            <form action="{!! route('posteditnew',$new->id) !!}" method="post" accept-charset="UTF-8"
                                  class="form-horizontal" id="form-main" enctype="multipart/form-data">
                                <div class="card-body">
                                    @csrf
                                    <div class="card">

                                        <div class="card-body">
                                            <div class="form-group  row ">
                                                <label for="vi__title" class="col-sm-2 col-form-label">Tiêu đề <span
                                                        class="seo" title="SEO"><i class="fa fa-coffee"
                                                                                   aria-hidden="true"></i></span></label>
                                                <div class="col-sm-8">
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text"><i
                                                                    class="fas fa-pencil-alt"></i></span>
                                                        </div>
                                                        <input type="text" id="nametitle" name="nametitle"
                                                               value="{!! $new->nametitle !!}" class="form-control nametitle" placeholder=""/>
                                                    </div>
                                                    <span class="form-text">
<i class="fa fa-info-circle"></i> Tối đa 200 kí tự
</span>
                                                </div>
                                            </div>

                                            <div class="form-group  row ">
                                                <label for="vi__description" class="col-sm-2 col-form-label">Mô tả <span
                                                        class="seo" title="SEO"><i class="fa fa-coffee"
                                                                                   aria-hidden="true"></i></span></label>
                                                <div class="col-sm-8">
                                                    <textarea id="vi__description" name="descriptions"
                                                              class="form-control vi__description"
                                                              placeholder="">{!! $new->description !!}</textarea>
                                                    <span class="form-text">
<i class="fa fa-info-circle"></i> Tối đa 300 kí tự
</span>
                                                </div>
                                            </div>
                                            <div class="form-group row ">
                                                <label for="content" class="col-sm-2 col-form-label">Nội
                                                    dung</label>
                                                <div class="col-sm-8">
<textarea class="contents" name="contents" id="contents">{!! $new->contents !!}

                                    </textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group  row ">
                                        <label for="image" class="col-sm-2 col-form-label">Hình ảnh</label>
                                        <div class="col-sm-8">
                                            <div class="input-group">
                                                <input type="file" id="image" name="image" value=""
                                                       class="form-control input-sm image" placeholder=""/>

                                            </div>
                                            <div id="preview_image" class="img_holder">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group  row ">
                                        <label for="sort" class="col-sm-2 col-form-label">Chọn trạng thái</label>
                                        <div class="col-sm-8">
                                            <div class="input-group">
                                                <select class="form-control shop_store select2"
                                                        data-placeholder="Trạng thái bài viết" style="width: 100%;"
                                                        name="onhome">
                                                    <option value=""></option>
                                                    <option value="1" {!! $new->home == 1? "selected" : "" !!}>Hiển thị trang chủ</option>
                                                    <option value="0" {!! $new->home == 0? "selected" : "" !!}>Không hiển thị trên trang chủ</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group  row ">
                                        <label for="sort" class="col-sm-2 col-form-label">Hiển thị trang chủ</label>
                                        <div class="col-sm-8">
                                            <div class="input-group">
                                                <select class="form-control shop_store select2"
                                                        data-placeholder="Trạng thái bài viết" style="width: 100%;"
                                                        name="hot">
                                                    <option value=""></option>
                                                    <option value="1" {!! $new->hot == 1? "selected":"" !!}>HOT</option>
                                                    <option value="0" {!! $new->hot == 0? "selected":"" !!}>Bài bình thường</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group row ">
                                        <label for="shop_store"
                                               class="col-sm-2 col-form-label">Danh mục sản phẩm</label>
                                        <div class="col-sm-8">
                                            <select class="form-control shop_store select2"
                                                    data-placeholder="Chọn danh mục sản phẩm" style="width: 100%;"
                                                    name="categorynew">
                                                <option value=""></option>

                                                @foreach($allcatnew as $item)
                                                    <option value="{!! $item->id !!} " {!! $new->catnew_id == $item->id ? "selected" : "" !!}>{!! $item->categorynew !!}
                                                    </option>
                                                @endforeach

                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group  row ">
                                        <label for="vi__keyword" class="col-sm-2 col-form-label">Từ khóa <span
                                                class="seo" title="SEO"><i class="fa fa-coffee"
                                                                           aria-hidden="true"></i></span></label>
                                        <div class="col-sm-8">
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                            <span class="input-group-text"><i
                                                                    class="fas fa-pencil-alt"></i></span>
                                                </div>
                                                <input type="text" id="vi__keyword"
                                                       name="seotitle" value="{!! $new->seotitle !!}"
                                                       class="form-control descriptions" placeholder=""/>
                                            </div>
                                            <span class="form-text">
<i class="fa fa-info-circle"></i> Tối đa 200 kí tự
</span>
                                        </div>
                                    </div>

                                    <div class="form-group  row ">
                                        <label for="keyword" class="col-sm-2 col-form-label">Thẻ Tag <span
                                                class="seo" title="SEO"><i class="fa fa-coffee"
                                                                           aria-hidden="true"></i></span></label>
                                        <div class="col-sm-8">
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                            <span class="input-group-text"><i
                                                                    class="fas fa-pencil-alt"></i></span>
                                                </div>
                                                <input type="text" id="vi__keyword"
                                                       name="tag" value="{!! $new->tag !!}"
                                                       class="form-control descriptions" placeholder=""/>
                                            </div>
                                            <span class="form-text">
<i class="fa fa-info-circle"></i> Tối đa 200 kí tự, cách nhau bằng dấu phẩy
</span>
                                        </div>
                                    </div>

                                    <div class="form-group  row">
                                        <label for="status" class="col-sm-2 col-form-label">Chọn trạng thái</label>
                                        <div class="col-sm-8">
                                            <select class="form-control shop_store select2"
                                                    data-placeholder="Chọn trạng thái" style="width: 100%;"
                                                    name="status">
                                                <option value=""></option>

                                                <option value="1" {!! $new->status == 1? "selected" : "" !!}>Active</option>
                                                <option value="0" {!! $new->status == 0? "selected" : "" !!}>Block</option>


                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="card-footer row">
                                    <div class="col-md-2">
                                    </div>
                                    <div class="col-md-8">
                                        <div class="btn-group float-right">
                                            <button type="submit" class="btn btn-primary">Gửi</button>
                                        </div>
                                        <div class="btn-group float-left">
                                            <button type="reset" class="btn btn-warning">Làm lại</button>
                                        </div>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>

    </div>
@stop
@section('script')
    <script src="{!! asset('admin/plugin/ckeditor/ckeditor.js') !!}"></script>
    <script src="{!! asset('admin/plugin/ckfinder/ckfinder.js') !!}"></script>
    <script>
        CKEDITOR.replace('contents', {
            filebrowserBrowseUrl: '{{ asset('admin/plugin/ckfinder/ckfinder.html') }}',
            filebrowserImageBrowseUrl: '{{ asset('admin/plugin/ckfinder/ckfinder.html?type=Images') }}',
            filebrowserFlashBrowseUrl: '{{ asset('admin/plugin/ckfinder/ckfinder.html?type=Flash') }}',
            filebrowserUploadUrl: '{{ asset('admin/plugin/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files') }}',
            filebrowserImageUploadUrl: '{{ asset('admin/plugin/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images') }}',
            filebrowserFlashUploadUrl: '{{ asset('admin/plugin/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash') }}'
        });

    </script>


@stop
