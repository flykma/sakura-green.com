<footer class="main-footer">
    <strong>Copyright &copy; 2021 <a href="https://s-cart.org">Sakuragreen.com</a>.</strong>
    All rights
    reserved.
</footer>
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="{!! asset('admin/LTE/plugins/jquery/jquery.min.js') !!}"></script>
<!-- jQuery UI 1.11.4 -->
<script src="{!! asset('admin/LTE/plugins/jquery-ui/jquery-ui.min.js') !!}"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->

<!-- Bootstrap 4 -->
<script src="{!! asset('admin/LTE/plugins/bootstrap/js/bootstrap.bundle.min.js') !!}"></script>

<!-- JQVMap -->
<script src="{!! asset('admin/LTE/plugins/jqvmap/jquery.vmap.min.js') !!}"></script>
<script src="{!! asset('admin/LTE/plugins/jqvmap/maps/jquery.vmap.usa.js') !!}"></script>
<!-- daterangepicker -->

<!-- Tempusdominus Bootstrap 4 -->

<!-- Summernote -->
<script src="{!! asset('admin/LTE/plugins/summernote/summernote-bs4.min.js') !!}"></script>
<!-- overlayScrollbars -->
<script src="{!! asset('admin/LTE/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js') !!}"></script>


<!-- Sparkline -->
<script src="{!! asset('admin/LTE/plugins/sparklines/sparkline.js') !!}"></script>
<!-- FastClick -->
<script src="{!! asset('admin/LTE/plugins/fastclick/fastclick.js') !!}"></script>
<!-- AdminLTE App -->
<script src="{!! asset('admin/LTE/dist/js/adminlte.js') !!}"></script>

<script src="{!! asset('admin/plugin/sweetalert2.all.min.js') !!}"></script>

<!-- Select2 -->
<script src="{!! asset('admin/LTE/plugins/select2/js/select2.full.min.js') !!}"></script>

<script src="{!! asset('admin/plugin/bootstrap-switch.min.js') !!}"></script>
<!-- AdminLTE for demo purposes -->


<script src="{!! asset('admin/LTE/plugins/iCheck/icheck.min.js') !!}"></script>

@yield('script')
<!--message-->
<script>
    $(function () {
        $(".date_time").datepicker({
            dateFormat: "yy-mm-dd"
        });
    });
</script>

</body>
</html>
