<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => 'locale'],function () {
    Route::get('language/{locale}','HomeController@changeLanguage')->name('changelanguage');
    Route::get('/', 'HomeController@index')->name('home');
    Route::get('/login', 'Admin\LoginController@index')->name('login');
    Route::post('/login', 'Admin\LoginController@PostLogin')->name('postlogin');
    Route::get('/logout','Admin\LoginController@Logout')->name('logout');

    Route::get('/new','SNewController@index')->name('catenew');
    Route::get('/new/{catslug}','SNewController@listNew')->name('listnews');
    Route::get('/new/{catslug}/{newslug}','SNewController@getDetail')->name('detailnews');

    Route::get('/search','ProductController@searchProduct')->name('search');
    Route::get('sort','ProductController@Sort')->name('sort');
    Route::get('sortprice','ProductController@PriceSort')->name('pricesort');

    Route::get('/san-pham','ProductController@index')->name('catpro');
    Route::get('/listproduct','ProductController@getAllProduct')->name('getlistpro');
    Route::get('/danh-muc/{catslug}','ProductController@listProduct')->name('listproduct');
    Route::get('/detail/{proslug}','ProductController@getDetail')->name('getdetailpro');

    Route::get('/cart','ProductController@cart')->name('cart');
    Route::get('/add-to-cart/{id}','ProductController@addToCart')->name('addtocart');
    Route::post('/update-cart','ProductController@updateCart')->name('updatecart');
    Route::get('/remove-from-cart/{id}','ProductController@remoteCart')->name('removecart');

    Route::get('province','ProductController@Province')->name('province');
    Route::get('district/{id}','ProductController@District')->name('district');
    Route::get('ward/{id}','ProductController@Ward')->name('ward');

    Route::get('/checkout','ProductController@checkCart')->name('checkcart');
    Route::get('/order','ProductController@Order')->name('order');

    Route::get('/gioi-thieu/{slug}','IntroductController@index')->name('intro');
    Route::get('/kien-thuc/{slug}','KnowledgeController@index')->name('know');

    Route::get('/lien-he','ContactController@index')->name('contact');
    Route::post('/lien-he','ContactController@postComment')->name('postcomment');

    Route::get('/video','VideoController@Video')->name('video');
});



Route::prefix('backend')->middleware('checklogin')->group(function () {

    Route::get('/', 'Admin\HomeController@index')->name('admin');

    Route::get('/user','Admin\UserController@index')->name('listuser');
    Route::get('/user-add','Admin\UserController@getAdd')->name('getadduser');
    Route::post('/user-add','Admin\UserController@postAdd')->name('postadduser');
    Route::get('/user-edit/{id}','Admin\UserController@getEdit')->name('getedituser');
    Route::post('/user-updated/{id}','Admin\UserController@postUpdate')->name('postupdateuser');
    Route::get('/user-delete/{id}','Admin\UserController@delete')->name('deleteuser');

    Route::get('/product','Admin\ProductController@index')->name('listpro');
    Route::get('/product-add','Admin\ProductController@getAdd')->name('getaddpro');
    Route::post('/product-add','Admin\ProductController@postAdd')->name('postaddpro');
    Route::get('/product-edit/{id}','Admin\ProductController@getEdit')->name('geteditpro');
    Route::post('/product-edit/{id}','Admin\ProductController@postEdit')->name('posteditpro');
    Route::get('/product-delete/{id}','Admin\ProductController@delete')->name('deletepro');

    Route::get('/new','Admin\NewController@index')->name('listnew');
    Route::get('/new-add','Admin\NewController@getAdd')->name('getaddnew');
    Route::post('/new-add','Admin\NewController@postAdd')->name('postaddnew');
    Route::get('/new-edit/{id}','Admin\NewController@getEdit')->name('geteditnew');
    Route::post('/new-edit/{id}','Admin\NewController@postEdit')->name('posteditnew');
    Route::get('/new-delete/{id}','Admin\NewController@delete')->name('deletenew');

    Route::get('/catnew','Admin\CategoryNewController@index')->name('listcatnew');
    Route::get('/catnew-add','Admin\CategoryNewController@getAdd')->name('getaddcatnew');
    Route::post('/catnew-add','Admin\CategoryNewController@postAdd')->name('postaddcatnew');
    Route::get('/catnew-edit/{id}','Admin\CategoryNewController@getEdit')->name('geteditcatnew');
    Route::post('/catnew-edit/{id}','Admin\CategoryNewController@postEdit')->name('posteditcatnew');
    Route::get('/catnew-delete/{id}','Admin\CategoryNewController@delete')->name('deletecatnew');

    Route::get('/catpro','Admin\CategoryProductController@index')->name('listcatpro');
    Route::get('/catpro-add','Admin\CategoryProductController@getAdd')->name('getcatpro');
    Route::post('/catpro-add','Admin\CategoryProductController@postAdd')->name('postaddcatpro');
    Route::get('/catpro-edit/{id}','Admin\CategoryProductController@getEdit')->name('geteditcatpro');
    Route::post('/catpro-edit/{id}','Admin\CategoryProductController@postEdit')->name('posteditcatpro');
    Route::get('/catpro-delete/{id}','Admin\CategoryProductController@delete')->name('deletecatpro');


    Route::get('/video','Admin\VideoController@index')->name('listvideo');
    Route::get('/video-add','Admin\VideoController@getAdd')->name('getaddvideo');
    Route::post('/video-add','Admin\VideoController@postAdd')->name('postaddvideo');
    Route::get('/video-edit/{id}','Admin\VideoController@getEdit')->name('geteditvideo');
    Route::post('/video-edit/{id}','Admin\VideoController@postEdit')->name('postEditvideo');
    Route::get('/video-delete/{id}','Admin\VideoController@delete')->name('deletevideo');

    Route::get('/slider','Admin\SliderController@index')->name('listslider');
    Route::get('/slider-add','Admin\SliderController@getAdd')->name('getaddslider');
    Route::post('/slider-add','Admin\SliderController@postAdd')->name('postaddslider');
    Route::get('/slider-edit/{id}','Admin\SliderController@getEdit')->name('geteditslider');
    Route::post('/slider-edit/{id}','Admin\SliderController@postEdit')->name('posteditslider');
    Route::get('/slider-delete/{id}','Admin\SliderController@delete')->name('deleteslider');

    Route::get('/setting','Admin\SettingController@index')->name('setting');
    Route::post('/setting','Admin\SettingController@postAdd')->name('postsetting');
    Route::post('/setting/{id}','Admin\SettingController@postEdit')->name('posteditsetting');

    Route::get('/introduct-add/{catid}','Admin\IntroductController@getAdd')->name('getaddintro');
    Route::post('/introduct-add/{catid}','Admin\IntroductController@postAdd')->name('postaddintro');
    Route::post('/introduct-edit/{catid}','Admin\IntroductController@postEdit')->name('posteditintro');


    Route::get('/catintro','Admin\CategoryIntroductController@index')->name('listcatintro');
    Route::get('/catintro-add','Admin\CategoryIntroductController@getAdd')->name('getaddcatintro');
    Route::post('/catintro-add','Admin\CategoryIntroductController@postAdd')->name('postaddcatintro');
    Route::get('/catintro-edit/{id}','Admin\CategoryIntroductController@getEdit')->name('geteditcatintro');
    Route::post('/catintro-edit/{id}','Admin\CategoryIntroductController@postEdit')->name('posteditcatintro');
    Route::get('/catintro-delete/{id}','Admin\CategoryIntroductController@delete')->name('deletecatintro');

    Route::get('/catknowledge','Admin\CategoryKnowledgeController@index')->name('listcatknowledge');
    Route::get('/catknowledge-add','Admin\CategoryKnowledgeController@getAdd')->name('getaddcatknowledge');
    Route::post('/catknowledge-add','Admin\CategoryKnowledgeController@postAdd')->name('postaddcatknowledge');
    Route::get('/catknowledge-edit/{id}','Admin\CategoryKnowledgeController@getEdit')->name('geteditcatknowledge');
    Route::post('/catknowledge-edit/{id}','Admin\CategoryKnowledgeController@postEdit')->name('posteditcatknowledge');
    Route::get('/catknowledge-delete/{id}','Admin\CategoryKnowledgeController@delete')->name('deletecatknowledge');

    Route::get('/knowledge-add/{catid}','Admin\KnowledgeController@getAdd')->name('getaddknowledge');
    Route::post('/knowledge-add/{catid}','Admin\KnowledgeController@postAdd')->name('postaddknowledge');
    Route::post('/knowledge-edit/{catid}','Admin\KnowledgeController@postEdit')->name('posteditknowledge');

    Route::get('/order','Admin\OrderController@index')->name('listorder');
    Route::get('/comment','Admin\CommentController@index')->name('comment');

//    Route::get('/partner','Admin\PartnerController@index')->name('listpartner');
//    Route::get('/partner-add','Admin\PartnerController@getAdd')->name('getaddpartner');
//    Route::post('/partner-add','Admin\PartnerController@postAdd')->name('postaddpartner');
//    Route::get('/partner-edit/{id}','Admin\PartnerController@getEdit')->name('geteditpartner');
//    Route::post('/partner-edit/{id}','Admin\PartnerController@postEdit')->name('posteditpartner');
//    Route::get('/partner-delete/{id}','Admin\PartnerController@delete')->name('deletepartner');
//    Route::get('/recuitment','Admin\RecruitmentController@index')->name('listrecuitment');
//    Route::get('/recuitment-add','Admin\RecruitmentController@getAdd')->name('getaddrecuitment');
//    Route::post('/recuitment-add','Admin\RecruitmentController@postAdd')->name('postaddrecuitment');
//    Route::get('/recuitment-edit/{id}','Admin\RecruitmentController@getEdit')->name('geteditrecuitment');
//    Route::post('/recuitment-edit/{id}','Admin\RecruitmentController@postEdit')->name('posteditrecuitment');
//    Route::get('/recuitment-delete/{id}','Admin\RecruitmentController@delete')->name('deleterecuitment');

});
