<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SNew extends Model
{
    protected $table = "s_news";
    protected $fillable = ["nametitle","catnew_id","slug","contents","seotitle","status","home","tag","image"];

    public function categorynew() {
        return $this->belongsTo(CategoryNew::class);
    }
}
