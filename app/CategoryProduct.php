<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CategoryProduct extends Model
{
    protected $table = "category_products";
    protected $fillable = ["categoryName","parentid","slug","description","status","seotitle"];
}
