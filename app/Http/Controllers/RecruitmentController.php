<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Recruitment;
class RecruitmentController extends Controller
{
    public function index() {
        $reCruitment = Recruitment::orderBy('id', 'desc')->first();
        return view('frontend.Recruitment.index',compact('reCruitment'));
    }
}
