<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\UserRequest;
use App\User;
use GuzzleHttp\Psr7\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use phpDocumentor\Reflection\Types\True_;

class UserController extends Controller
{
    public function index()
    {
        if (request()->ajax()) {
            try {
                $user = User::all();
                return datatables()->of($user)->addColumn('action', function ($user) {
                        return '<a href="javascript:void(0)" data-toggle="tooltip" data-id="'.$user->id.'" data-original-title="Edit" class="edit btn btn-success edit-product">
Edit
</a><a href="javascript:void(0);" id="delete-product" data-toggle="tooltip" data-original-title="Delete" data-id="'.$user->id.'" class="delete btn btn-danger">
Delete
</a>';
                    })->editColumn('id','ID: {{$id}}')->removeColumn('password')->addIndexColumn()->make(true);
            } catch (Exception $e) {
                dd($e);
            }
        }
        return view('backend.User.index');
    }



//    public function getAdd()
//    {
//        return view('backend.User.add');
//    }

    public function postAdd(Request $request)
    {
        $user = User::updateOrCreate([
            'name'=> $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'role' => $request->role,

        ]);
        return response()->json($user);
//        $user = new User();
//        $filename = $request->avatar;
//        $filename->move("uploads", $filename->getClientOriginalName());
//        $user->name = $request->input('name');
//        $user->email = $request->input('email');
//        $user->password = Hash::make($request->input('password'));
//        $user->avatar = $filename->getClientOriginalName();
//        $user->role = $request->role;
//        $user->save();
//        return redirect()->route('listuser');

    }

    public function getEdit($id)
    {
        $where = array('id'=>$id);
        $user = User::where($where)->first();
        return response()->json($user);
    }

    public function postUpdate(Request $request,$id)
    {
        $user = User::find($id);


//        $name = $request->name;
//        $role = $request->role;
//        if ($request->hasFile('avatar'))
//        {
//            $file = $request->avatar;
//            $avatar = $request->avatar->getClientOriginalName();
//            $file->move("uploads", $avatar);
//            User::where('id',$id)->update(['name'=>$name,'avatar'=>$avatar,'role'=>$role]);
//            return redirect()->route('listuser');
//
//        }
//        User::where('id',$id)->update(['name'=>$name,'role'=>$role]);
//        return redirect()->route('listuser');

    }
    public function delete($id)
    {

        $user = User::where('id',$id)->delete();
        return \response()->json($user);

//        $iddelete = User::find($id);
//        if ($iddelete->id != $id) {
//            $iddelete->delete();
//            return redirect()->route('listuser');
//        }
//        else {
//            return back()->with("message","Bạn không thể xóa chính mình");
//        }

    }
}
