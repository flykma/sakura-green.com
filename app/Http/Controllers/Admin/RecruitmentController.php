<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\RecuitmentRequest;
use App\Recruitment;
use Illuminate\Http\Request\RecruitmentRequest;
use Illuminate\Support\Str;
class RecruitmentController extends Controller
{
    public function index() {
        $allRecruit = Recruitment::all();
        return view('backend.Recruitment.index',compact('allRecruit'));
    }

    public function getAdd() {
        return view('backend.Recruitment.add');
    }

    public function postAdd(RecuitmentRequest $request) {
        $reCruiment = new Recruitment();
        $reCruiment->name = $request->name;
        $reCruiment->slug = str::slug($request->name);
        $reCruiment->contents = $request->contents;
        $reCruiment->status = $request->status;
        if ($request->hasFile('image')) {
            $file = $request->image;
            $file->move("uploads",$file->getClientOriginalName());
            $reCruiment->image = $file->getClientOriginalName();
        }
        $reCruiment->save();
        return redirect()->route('listrecuitment');
    }

    public function getEdit($id){
        $reCruitment = Recruitment::find($id);
        return view('backend.Recruitment.edit',compact('reCruitment'));
    }

    public function postEdit(RecuitmentRequest $request,$id) {
        $reCruiment = Recruitment::find($id);
        $reCruiment->name = $request->name;
        $reCruiment->slug = str::slug($request->name);
        $reCruiment->contents = $request->contents;
        $reCruiment->status = $request->status;
        if ($request->hasFile('image')) {
            $file = $request->image;
            $file->move("uploads",$file->getClientOriginalName());
            $reCruiment->image = $file->getClientOriginalName();
        }
        $reCruiment->save();
        return redirect()->route('listrecuitment');
    }

    public function delete($id) {
        $delete = Recruitment::find($id);
        $delete->delete();
        return redirect()->route('listrecuitment');
    }
}
