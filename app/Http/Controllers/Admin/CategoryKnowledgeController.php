<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\CategoryKnowledge;
use App\Knowledge;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class CategoryKnowledgeController extends Controller
{
    public function index() {
        $catknowledge = CategoryKnowledge::orderBy('id','DESC')->paginate(10);
        return view('backend.CategoryKnowledge.index',compact('catknowledge'));
    }

    public function getAdd() {
        return view('backend.CategoryKnowledge.add');
    }

    public function postAdd(Request $request) {
        $validator = $request->validate(['name'=>'required']);
        if ($validator) {
            $catknowledge = new CategoryKnowledge();
            $catknowledge->name = $request->name;
            $catknowledge->slug = Str::slug($request->name);
            $catknowledge->status = $request->status;
            $catknowledge->save();
            return redirect()->route('listcatknowledge');
        }

    }

    public function getEdit($id) {
        $catknowledge = CategoryKnowledge::find($id);
        return view('backend.CategoryKnowledge.edit',compact('catknowledge'));
    }

    public function postEdit(Request $request,$id) {
        $validator = $request->validate(['name'=>'required']);
        if ($validator) {
            $catintro = CategoryKnowledge::find($id);
            $catintro->name = $request->name;
            $catintro->slug = Str::slug($request->name);
            $catintro->status = $request->status;
            $catintro->save();
            return redirect()->route('listcatknowledge');
        } else {
            return back()->with('message','Thêm thất bại');
        }
    }

    public function delete($id) {
        $knowledge = Knowledge::where('catid_knowledge',$id)->get();
        if (empty($knowledge)) {
            return back()->with('message','Xóa thất bại');
        } else {
            $delete = CategoryKnowledge::find($id);
            $delete->delete();
            return back()->with('message','Xóa thành công');
        }
    }
}
