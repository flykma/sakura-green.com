<?php

namespace App\Http\Controllers\Admin;

use App\CategoryNew;
use App\CategoryProduct;
use App\Http\Controllers\Controller;
use App\Http\Requests\CategoryProductRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\View\View;
use App\Product;
class CategoryProductController extends Controller
{
    public function index(){
        $listcatpro = CategoryProduct::where('parentid',0)->get();
        return view('backend.CategoryProduct.index',compact('listcatpro'));
    }

    public function getAdd() {
        $allcatpro = CategoryProduct::where('parentid',0)->get();
        return view('backend.CategoryProduct.add',compact('allcatpro'));
    }

    public function postAdd(CategoryProductRequest $request) {
        $allcatnew = CategoryNew::where('parentid',0)->get();
        $name = $request->name;
        $settitle = $request->seotitle;
        $description = $request->description;
        $parentid = $request->parentid;
        $status = $request->status;
        $slug = Str::slug($request->name);
        $catnew = CategoryProduct::create(['categoryName'=>$name,'slug'=>$slug,'parentid'=>$parentid,'description'=>$description,'seotitle'=>$settitle,'status'=>$status]);
        if ($catnew)
        {
            return redirect()->route('listcatpro');
        }
    }

    public function getEdit($id){
        $allcatpro= CategoryProduct::where('parentid',0)->get();
        $catpro = CategoryProduct::find($id);
        return view('backend.CategoryProduct.edit',compact(['catpro','allcatpro']));
    }
    public function postEdit(CategoryProductRequest $request,$id){
        $name = $request->name;
        $parentid = $request->parentid;
        $seotitle = $request->seotitle;
        $description = $request->description;
        $status = $request->status;
        CategoryProduct::where('id',$id)->update(['categoryname'=>$name,'parentid'=>$parentid,'seotitle'=>$seotitle,'description'=>$description,'status'=>$status]);
        return redirect()->route('listcatpro');
    }
    public function delete($id) {
        $iddelete = CategoryProduct::where('parentid',$id)->get();
        $product = Product::where('catpro_id',$id)->get();
        if (!$iddelete->isEmpty() || !$product->isEmpty()) {
            return back()->with("message","Không thể xóa danh mục chứa danh mục con hoặc có sản phẩm");
        }
        else {
            $delete = CategoryProduct::find($id);
            $delete->delete();
            return back()->with("message","Xóa thành công");
        }


    }
}
