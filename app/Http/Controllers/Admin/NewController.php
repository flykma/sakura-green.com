<?php

namespace App\Http\Controllers\Admin;

use App\CategoryNew;
use App\Http\Controllers\Controller;
use App\Http\Requests\NewRequest;
use App\SNew;
use CKSource\CKFinder\Filesystem\File\File;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class NewController extends Controller
{
    public function index()
    {
        $allnew = SNew::all();
        return view('backend.New.index', compact('allnew'));
    }

    public function getAdd()
    {
        $allcatnew = CategoryNew::where('status', 1)->get();
        return view('backend.New.add', compact('allcatnew'));
    }

    public function postAdd(NewRequest $request)
    {
        $image = $request->image;
        $snew = new SNew();
        $snew->nametitle = $request->nametitle;
        $snew->catnew_id = $request->categorynew;
        $snew->slug = Str::slug($request->nametitle);
        $snew->description = $request->descriptions;
        $snew->contents = $request->contents;
        $snew->seotitle = $request->seotitle;
        $snew->status = $request->status;
        $snew->home = $request->onhome;
        $snew->hot = $request->hot;
        $snew->tag = $request->tag;
        if ($request->hasFile('image'))
        {
            $snew->image = $image->getClientOriginalName();
            $image->move("uploads", $image->getClientOriginalName());
        }
        $snew->save();
        return redirect()->route('listnew');
    }

    public function getEdit($id)
    {
        $allcatnew = CategoryNew::where('status',1)->get();
        $new = SNew::find($id);
        return view('backend.New.edit',compact(['new'],'allcatnew'));
    }

    public function postEdit(NewRequest $request, $id)
    {
        $image = $request->image;
        $updatenew = SNew::find($id);
        $updatenew->nametitle = $request->nametitle;
        $updatenew->catnew_id = $request->categorynew;
        $updatenew->slug = Str::slug($request->nametitle);
        $updatenew->description = $request->descriptions;
        $updatenew->contents = $request->contents;
        $updatenew->seotitle = $request->seotitle;
        $updatenew->status = $request->status;
        $updatenew->home = $request->onhome;
        $updatenew->hot = $request->hot;
        $updatenew->tag = $request->tag;
        if ($request->hasFile('image'))
        {
            $updatenew->image = $image->getClientOriginalName();
            $image->move("uploads", $image->getClientOriginalName());
        }
        $updatenew->save();
        return redirect()->route('listnew');
    }

    public function delete($id)
    {
        $delete = SNew::find($id);
        $delete->delete();
        return back();
    }
}
