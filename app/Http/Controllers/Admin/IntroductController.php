<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Introduct;
use App\CategoryIntroduct;
use App\Http\Requests\IntroductRequest;
class IntroductController extends Controller
{
    public function getAdd($catid) {
        $catintro = CategoryIntroduct::find($catid);
        $introduct = Introduct::where('catid_intro',$catid)->first();
        return view('backend.Introduct.add',compact(['catintro','introduct']));

    }

    public function postAdd(IntroductRequest $request,$catintro)
    {
        $intro = new Introduct();
        $intro->contents = $request->contents;
        $intro->catid_intro = $catintro;
        $intro->save();
        return back();
    }

    public function postEdit(IntroductRequest $request,$catintro)
    {
        $intro = Introduct::where('catid_intro',$catintro)->update(['contents'=>$request->contents]);
        return back();
    }
}
