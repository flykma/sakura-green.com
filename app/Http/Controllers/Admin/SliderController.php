<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Slider;
use Illuminate\Support\Str;
use App\Http\Requests\SliderRequest;
class SliderController extends Controller
{
    public function index()
    {
        $allslider = Slider::all();
        return view('backend.Slider.index',compact('allslider'));
    }

    public function getAdd()
    {
        return view('backend.Slider.add');
    }

    public function postAdd(SliderRequest $request)
    {
        $image = $request->image;
        $slider = new Slider();
        $slider->name = $request->name;
        $slider->slug = Str::slug($request->name);
        $slider->contents = $request->description;
        $slider->status = $request->status;
        if ($request->hasFile('image'))
        {
            $slider->image = $image->getClientOriginalName();
            $image->move("uploads", $image->getClientOriginalName());
        }
        $slider->save();
        return redirect()->route('listslider');
    }

    public function getEdit($id)
    {
        $slider = Slider::find($id);
        return view('backend.Slider.edit', compact('slider'));
    }

    public function postEdit(SliderRequest $request, $id)
    {
        $image = $request->image;
        $slider = Slider::find($id);
        $slider->name = $request->name;
        $slider->slug = Str::slug($request->name);
        $slider->contents = $request->description;
        $slider->status = $request->status;
        if ($request->hasFile('image'))
        {
            $slider->image = $image->getClientOriginalName();
            $image->move("uploads", $image->getClientOriginalName());
        }
        $slider->save();
        return redirect()->route('listslider');
    }

    public function delete($id)
    {
        $slider = Slider::find($id);
        $slider ->delete();
        return redirect()->route('listslider');
    }

}
