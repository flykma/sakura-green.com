<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Setting;
class SettingController extends Controller
{
    public function index() {
        $setting = Setting::find(1);
        return view('backend.Setting.setting',compact('setting'));
    }
    public function postAdd(Request $request) {
        $setting = new Setting();
        $setting->company = $request->name;
        $setting->address = $request->address;
        $setting->phone = $request->phone;
        $setting->email = $request->email;
        $setting->facebook = $request->facebook;
        $setting->youtube = $request->youtube;
        if ($request->hasFile('bg_footer'))
        {
            $bg_footer = $request->bg_footer;
            $bg_footer->move('uploads',$file->getClientOriginalName());
            $setting->logo = $bg_footer->getClientOriginalName();
        }
        if ($request->hasFile('image'))
        {
            $file = $request->image;
            $file->move('uploads',$file->getClientOriginalName());
            $setting->logo = $file->getClientOriginalName();
        }
        $setting->save();
        return back();
    }

    public function postEdit(Request $request) {
        $setting = Setting::find(1);
        $setting->company = $request->name;
        $setting->address = $request->address;
        $setting->phone = $request->phone;
        $setting->email = $request->email;
        $setting->facebook = $request->facebook;
        $setting->youtube = $request->youtube;
        if ($request->hasFile('bg_footer'))
        {
            $bg_footer = $request->bg_footer;
            $bg_footer->move('uploads',$bg_footer->getClientOriginalName());
            $setting->bg_footer = $bg_footer->getClientOriginalName();
        }
        if ($request->hasFile('image'))
        {
            $file = $request->image;
            $file->move('uploads',$file->getClientOriginalName());
            $setting->logo = $file->getClientOriginalName();
        }
        $setting->save();
        return back();
    }
}
