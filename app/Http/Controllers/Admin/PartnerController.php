<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Partner;
use Illuminate\Support\Str;
use App\Http\Requests\PartnerRequest;

class PartnerController extends Controller
{
    public function index() {
        $allpartner = Partner::all();
        return view('backend.Partner.index',compact('allpartner'));
    }
    public function getAdd(){
        return view('backend.Partner.add');
    }
    public function postAdd(PartnerRequest $request){
        $partner = new Partner();
        $partner->name = $request->name;
        $partner->link = $request->link;
        $partner->status = $request->status;
        if ($request->hasFile('image'))
        {

            $file = $request->image;
            $file->move('uploads',$file->getClientOriginalName());
            $partner->image = $file->getClientOriginalName();
        }
        $partner->save();
        return redirect()->route('listpartner');
    }
    public function getEdit($id){
        $partner = Partner::find($id);
        return view('backend.Partner.edit',compact(['partner']));
    }
    public function postEdit(PartnerRequest $request,$id){
        $partner = Partner::find($id);
        $partner->name = $request->name;
        $partner->link = $request->link;
        $partner->status = $request->status;
        if ($request->hasFile('image'))
        {
            $file = $request->image;
            $file->move('uploads',$file->getClientOriginalName());
            $partner->image = $file->getClientOriginalName();
        }
        $partner->save();
        return redirect()->route('listpartner');
    }
    public function delete($id){
        return view();
    }
}
