<?php

namespace App\Http\Controllers\Admin;

use App\CategoryNew;
use App\CategoryProduct;
use App\Http\Controllers\Controller;
use App\Product;
use App\SNew;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    public function index(){
        $allcatnew = CategoryNew::all()->count();
        $allcatpro = CategoryProduct::all()->count();
        $allnew = SNew::all()->count();
        $allpro = Product::all()->count();
        $user = Auth::user();
        return view('backend.home',compact(['user','allcatnew','allcatpro','allnew','allpro']));
    }
}
