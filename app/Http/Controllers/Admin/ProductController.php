<?php

namespace App\Http\Controllers\Admin;

use App\CategoryProduct;
use App\Http\Controllers\Controller;
use App\Http\Requests\ProductRequest;
use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class ProductController extends Controller
{
    public function index(){
        $allpro = Product::all();
        return view('backend.Product.index',compact('allpro'));
    }
    public function getAdd(){
        $allcatpro = CategoryProduct::where(['status'=>1,'parentid'=>0])->get();
        return view('backend.Product.add',compact('allcatpro'));
    }
    public function postAdd(ProductRequest $request){
        $product = new Product();
        $product->productname = $request->nameproduct;
        $product->slug = Str::slug($request->nameproduct);
        $product->symbol = $request->symbol;
        $product->description = $request->descriptions;
        $product->price = $request->price;
        $product->contents = $request->contents;
        $product->discount = $request->discount;
        $product->seotitle = $request->seotitle;
        $product->tag = $request->tag;
        $product->catpro_id = $request->category;
        $product->status = $request->status;
        $product->guide = $request->guide;
        $product->hot = $request->hot;
        $product->showhome = $request->onhome;

        if($request->hasFile('photo')) {
            $photo = $request->file('photo');
            $photoName = $photo->getClientOriginalName();
            $photo->move("uploads/product",$photoName);
            $product->photo = $photoName;
        }

        if ($request->hasFile('images'))
        {
            foreach ($request->file('images') as $image)
            {
                $name = $image->getClientOriginalName();
                $image->move('uploads',$name);
                $product->image = $name;
                $data[] = $name;
            }
            $product->image =json_encode($data);
        }
        $product->save();
        return redirect()->route('listpro');
    }
    public function getEdit($id){
        $product = Product::find($id);
        $allcatpro = CategoryProduct::where(['status'=>1,'parentid'=>0])->get();
        return view('backend.Product.edit',compact(['product','allcatpro']));
    }
    public function postEdit(ProductRequest $request,$id){
        $product = Product::find($id);
        $product->productname = $request->nameproduct;
        $product->slug = Str::slug($request->nameproduct);
        $product->symbol = $request->symbol;
        $product->description = $request->descriptions;
        $product->price = $request->price;
        $product->contents = $request->contents;
        $product->discount = $request->discount;
        $product->seotitle = $request->seotitle;
        $product->tag = $request->tag;
        $product->catpro_id = $request->category;
        $product->status = $request->status;
        $product->guide = $request->guide;
        $product->hot = $request->hot;
        $product->showhome = $request->onhome;

        if($request->hasFile('photo')) {
            $photo = $request->file('photo');
            $photoName = $photo->getClientOriginalName();
            $photo->move("uploads/product",$photoName);
            $product->photo = $photoName;
        }
        if ($request->hasFile('images'))
        {
            foreach ($request->file('images') as $image)
            {
                $name = $image->getClientOriginalName();
                $image->move('uploads',$name);
                $product->image = $name;
                $data[] = $name;
            }
            $product->image =json_encode($data);
        }
        $product->save();
        return redirect()->route('listpro');
    }
    public function delete($id){
        $delete = Product::find($id);
        $delete->delete();
        return redirect()->back();
    }
}
