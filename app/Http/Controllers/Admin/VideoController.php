<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\VideoRequest;
use App\Video;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class VideoController extends Controller
{
    public function index()
    {
        $allvideos = Video::all();
        return view('backend.Videos.index', compact('allvideos'));
    }

    public function getAdd()
    {
        return view('backend.Videos.add');
    }

    public function postAdd(VideoRequest $request)
    {
        $video = new Video();
        $video->name = $request->name;
        $video->slug = Str::slug($request->name);
        $video->link = $request->link;
        $video->status = $request->status;
        $video->save();
        return redirect()->route('listvideo');
    }

    public function getEdit($id)
    {
        $video = Video::find($id);
        return view('backend.Videos.edit', compact('video'));
    }

    public function postEdit(VideoRequest $request, $id)
    {
        $video = Video::find($id);
        $video->name = $request->name;
        $video->slug = Str::slug($request->name);
        $video->link = $request->link;
        $video->status = $request->status;
        $video->save();
        return redirect()->route('listvideo');
    }

    public function delete($id)
    {
        $video = Video::find($id);
        $video->delete();
        return redirect()->route('listvideo');
    }
}
