<?php

namespace App\Http\Controllers\Admin;

use App\Comment;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CommentController extends Controller
{
    public function index()
    {
        $allcomment = Comment::all();
        return view('backend.Comments.index',compact('allcomment'));
    }
}
