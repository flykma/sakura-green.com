<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\LoginRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use MongoDB\Driver\Session;

class LoginController extends Controller
{
    public function index() {
        if (Auth::check()) {
            return redirect()->route('admin');
        }
        return view('backend.login.login');
    }

    public function PostLogin(LoginRequest $request){

        $credentials = $request->validate([
            'email' => ['required', 'email'],
            'password' => ['required'],
        ]);
        $email = $request->input('email');
        $password = $request->input('password');
        $remember = $request->input('remember');
        if (Auth::attempt(['email' => $email,'password' => $password,'role'=>1],$remember)) {
            $request->session()->regenerate();
            return redirect()->route('admin');
        }
        return redirect()->back();

    }

    public function Logout(LoginRequest $request) {
        Auth::logout();
        $request->session()->invalidate();
        $request->session()->regenerateToken();
        return redirect()->route('login');
    }
}
