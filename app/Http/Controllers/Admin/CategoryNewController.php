<?php

namespace App\Http\Controllers\Admin;

use App\CategoryNew;
use App\SNew;
use App\Http\Controllers\Controller;
use App\Http\Requests\CategoryNewRequest;
use Illuminate\Http\Request;
use Illuminate\View\View;
use \Illuminate\Support\Str;

class CategoryNewController extends Controller
{
    public function index() {
        $listcatnew = CategoryNew::where(['parentid'=>0])->get();
        return view('backend.CategoryNew.index',compact('listcatnew'));
    }
    public function getAdd(){
        $allcatnew = CategoryNew::where('parentid',0)->get();
        return view('backend.CategoryNew.add',compact('allcatnew'));
    }

    public function postAdd(CategoryNewRequest $request) {
        $allcatnew = CategoryNew::where('parentid',0)->get();
        $name = $request->name;
        $settitle = $request->seotitle;
        $description = $request->description;
        $parentid = $request->parentid;
        $status = $request->status;
        $slug = Str::slug($request->name);
        $catnew = CategoryNew::create(['categorynew'=>$name,'slug'=>$slug,'parentid'=>$parentid,'description'=>$description,'seotitle'=>$settitle,'status'=>$status]);
        if ($catnew)
        {
            return redirect()->route('listcatnew');
        }

    }

    public function getEdit($id) {
        $allcatnew = CategoryNew::where('parentid',0)->get();
        $catnew = CategoryNew::find($id);
        return view('backend.CategoryNew.edit',compact(['catnew','allcatnew']));
    }
    public function postEdit(CategoryNewRequest  $request,$id) {
        $name = $request->name;
        $parentid = $request->parentid;
        $seotitle = $request->seotitle;
        $description = $request->description;
        $status = $request->status;
        CategoryNew::where('id',$id)->update(['categorynew'=>$name,'parentid'=>$parentid,'seotitle'=>$seotitle,'description'=>$description,'status'=>$status]);
        return redirect()->route('listcatnew');
    }
    public function delete($id) {
        $iddelete = CategoryNew::where('parentid',$id)->get();
        $new = SNew::where('catnew_id',$id)->get();
        if (!$iddelete->isEmpty() || !$new->isEmpty())
        {
            return back()->with('message','Bạn không thể xóa danh mục chứa danh mục con hoặc có sản phẩm');
        }
        else {
            CategoryNew::find($id)->delete();
            return back()->with('message','Xóa thành công');
        }


    }
}
