<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\CategoryKnowledge;
use App\Knowledge;
use Illuminate\Http\Request;

class KnowledgeController extends Controller
{
    public function getAdd($catid) {
        $catknowledge = CategoryKnowledge::find($catid);
        $knowledge = Knowledge::where('catid_knowledge',$catid)->first();
        return view('backend.Knowledge.add',compact(['catknowledge','knowledge']));

    }

    public function postAdd(Request $request,$catknowledge)
    {
        $intro = new Knowledge();
        $intro->contents = $request->contents;
        $intro->catid_knowledge = $catknowledge;
        $intro->save();
        return back();
    }

    public function postEdit(Request $request,$catintro)
    {
        $intro = Knowledge::where('catid_knowledge',$catintro)->update(['contents'=>$request->contents]);
        return back();
    }
}
