<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\CategoryIntroduct;
use App\Introduct;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class CategoryIntroductController extends Controller
{
    public function index() {
        $catintro = CategoryIntroduct::orderBy('id','DESC')->paginate(10);
        return view('backend.CategoryIntroduct.index',compact('catintro'));
    }

    public function getAdd() {
        return view('backend.CategoryIntroduct.add');
    }

    public function postAdd(Request $request) {
        $validator = $request->validate(['name'=>'required']);
        if ($validator) {
            $catintro = new CategoryIntroduct();
            $catintro->name = $request->name;
            $catintro->slug = Str::slug($request->name);
            $catintro->status = $request->status;
            $catintro->save();
            return redirect()->route('listcatintro');
        }

    }

    public function getEdit($id) {
        $catintro = CategoryIntroduct::find($id);
        return view('backend.CategoryIntroduct.edit',compact('catintro'));
    }

    public function postEdit(Request $request,$id) {
        $validator = $request->validate(['name'=>'required']);
        if ($validator) {
            $catintro = CategoryIntroduct::find($id);
            $catintro->name = $request->name;
            $catintro->slug = Str::slug($request->name);
            $catintro->status = $request->status;
            $catintro->save();
            return redirect()->route('listcatintro');
        } else {
            return back()->with('message','Thêm thất bại');
        }
    }

    public function delete($id) {
        $intro = Introduct::where('catid_intro',$id)->get();
        if (empty($intro)) {
            return back()->with('message','Xóa thất bại');
        } else {
            $delete = CategoryIntroduct::find($id);
            $delete->delete();
            return back()->with('message','Xóa thành công');
        }
    }
}
