<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CategoryNew;
use App\SNew;
class SNewController extends Controller
{
    public function index() {
        $new = SNew::where('status',1)->orderBy('id','DESC')->paginate(6);
        return view('frontend.News.list',compact('new'));

    }

    public function listNew($catslug) {
        $catnew = CategoryNew::where('slug',$catslug)->first();
        $new = SNew::where(['status'=>1,'catnew_id'=>$catnew->id])->orderBy('id','DESC')->paginate(6);
        return view('frontend.Categorynew.detail',compact(['new','catnew']));
    }
    public function getDetail($catslug,$newslug)
    {
        $catnew = CategoryNew::where('slug',$catslug)->first();
        $news = SNew::where(['status'=>1,'catnew_id'=>$catnew->id])->orderBy('id','DESC')
                ->take(5)
                ->get();
        $new = SNew::where('slug',$newslug)->firstOrFail();
        return view('frontend.News.detail',compact(['new','news']));
    }
}
