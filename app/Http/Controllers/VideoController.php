<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Video;
class VideoController extends Controller
{
    public function Video() {
        $video = Video::where('status',1)->orderBy('id','DESC')->get();
        return view('frontend.Video.index',compact('video'));
    }
}
