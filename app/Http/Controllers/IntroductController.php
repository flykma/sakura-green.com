<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Introduct;
use App\CategoryIntroduct;
class IntroductController extends Controller
{
    public function index($slug) {
        $catintro = CategoryIntroduct::where('slug',$slug)->first();
        $intro = Introduct::where('catid_intro',$catintro->id)->first();
        return view('frontend.Introduct.index',compact(['intro','catintro']));
    }
}
