<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ContactRequest;
use App\Comment;

class ContactController extends Controller
{
    public function index() {
        return view('frontend.Contact.index');
    }
    public function postComment(ContactRequest $request)
    {
        $contact = new Comment();
        $contact->name = $request->name;
        $contact->email = $request->email;
        $contact->address = $request->address;
        $contact->phone = $request->phone;
        $contact->contents = $request->contents;
        $contact->save();
        return redirect()->route('contact');
    }
}
