<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CategoryKnowledge;
use App\Knowledge;

class KnowledgeController extends Controller
{
    public function index($slug) {
        $catknowledge = CategoryKnowledge::where('slug',$slug)->first();
        $knowledge = Knowledge::where('catid_knowledge',$catknowledge->id)->first();
        return view('frontend.Knowledge.index',compact(['knowledge','catknowledge']));
    }
}
