<?php

namespace App\Http\Controllers;

use App\User;
use App\Product;
use App\SNew;
use App\Slider;
use App\CategoryProduct;
use App\Video;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Session\Session;

class HomeController extends Controller
{
    public function index() {
        $slider = Slider::where('status',1)->get();
        $catpro = CategoryProduct::where('status',1)->get();
        $new = SNew::where('status',1)->orderBy('id','DESC')->skip(1)->limit(3)->get();
        $firstnew = SNew::where('status',1)->orderBy('id','DESC')->take(1)->first();
        return view('frontend.index',compact(['catpro','new','firstnew','slider']));
    }

    public function changeLanguage($locale) {
        \session()->put('locale',$locale);
        return redirect()->back();
    }


}
