<?php

namespace App\Http\Controllers;

use App\District;
use App\Mail\OrderMailer;
use App\Province;
use App\Ward;
use Illuminate\Http\Request;
use App\Product;
use App\CategoryProduct;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class ProductController extends Controller
{
    public function index()
    {

        return view('frontend.Product.list');
    }

    public function getAllProduct()
    {
        $allpro = Product::where('status', 1)->orderBy('id', 'DESC')->get();
        return response()->json($allpro);
    }

    public function listProduct($catslug)
    {
        $catpro = CategoryProduct::where('slug', $catslug)->first();
        $product = Product::where(['status' => 1, 'catpro_id' => $catpro->id])->orderBy('id', 'DESC')->paginate(9);
        return view('frontend.Categoryproduct.detail', compact(['product', 'catpro']));
    }

    public function Sort(Request $request)
    {
        if ($request->ajax()) {
            $data = $request->all();
            $product = Product::where('status', 1)->orderBy($data['sort'], $data['datasort'])->get();
            return response()->json($product);


        }
    }

    public function PriceSort(Request $request) {
        if ($request->ajax()) {
            $data = $request->all();
            $product = Product::where([['status','=',1], ['price' ,'>=', $data['pricemin']], ['price','<', $data['pricemax']]])->get();
            return response()->json($product);


        }
    }

    public function searchProduct(Request $request)
    {
        $s = $request->s;
        $search = Product::where('productname', 'like', '%' . $s . '%')->orderBy('id', 'DESC')->paginate(6);
        dd($search);
        return view('frontend.Search.search', compact(['search', 's']));
    }

    public function getDetail($proslug)
    {
        $product = Product::where('slug', $proslug)->firstOrFail();
        $catpro = CategoryProduct::where('id', $product->catpro_id)->first();
        $image = str_replace('["', '', $product->image);
        $images = str_replace('"]', '', $image);
        $photo = explode('","', $images);
        $products = Product::where(['status' => 1, 'catpro_id' => $catpro->id])->orderBy('id', 'DESC')
            ->take(4)
            ->get();
        $seen = session()->get('seen', []);
        $seen[$product->id] = [
            "name" => $product->productname,
            "slug" => $product->slug,
            "image" => $product->photo,
            "price" => $product->price,
            "symbol" => $product->symbol
        ];
        session()->put('seen', $seen);
        return view('frontend.Product.detail', compact(['product', 'products', 'photo']));
    }

    public function cart()
    {
        return view('frontend.Cart.cart');
    }

    public function addToCart($id)
    {
        $product = Product::findOrFail($id);
        $catpro = CategoryProduct::find($product->catpro_id);
        $cart = session()->get('cart', []);
        if (isset($cart[$id])) {
            $cart[$id]['quanlity']++;
        } else {
            $cart[$id] = [
                "name" => $product->productname,
                "quanlity" => 1,
                "slug" => $product->slug,
                "image" => $product->photo,
                "price" => $product->price,
                "catpro" => $catpro->slug,
                "symbol" => $product->symbol,
            ];
        }
        session()->put('cart', $cart);
        return response()->json($cart);
    }

    public function updateCart(Request $request)
    {
        if ($request->id && $request->quanlity) {
            $cart = session()->get('cart');
            $cart[$request->id]["quanlity"] = $request->quanlity;
            session()->put('cart', $cart);
            session()->flash('success', 'Cart updated successfully');
            return response()->json($cart[$request->id]);
        }
    }

    public function remoteCart($id)
    {
        if ($id) {
            $cart = session()->get('cart');
            if (isset($cart[$id])) {
                unset($cart[$id]);
                session()->put('cart', $cart);
                session()->flash('message', 'Xoá thành công');
                return response()->json(session('cart'));
            }

        }
    }

    public function Province()
    {
        $province = Province::all();
        return response()->json($province);
    }

    public function District($id)
    {
        $district = District::where('_province_id', $id)->get();
        return response()->json($district);
    }

    public function Ward($id)
    {
        $ward = Ward::where('_district_id', $id)->get();
        return response()->json($ward);
    }

    public function checkCart()
    {

        return view('frontend.Cart.checkout');
    }


    public function Order()
    {
        $data = [
            'name' => 'donpv',
        ];
        Mail::to('donpv.kma@gmail.com')->send(new OrderMailer(['data' => $data['name']]));


    }
}
