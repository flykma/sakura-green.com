<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|email|unique:users',
            'name' => 'required',
            'password' => 'required|confirmed',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Tên không được để trống',
            'email.required' =>'Không được để trống email',
            'email.email' => 'Định dạng email không đúng',
            'email.unique' =>'Tài khoản email đã tồn tại',
            'password.required'=>'Không được để trống mật khẩu',
            'password.confirmed' =>'Mật khẩu xác nhận không khớp'];
    }

}
