<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RecuitmentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'contents' =>'required',
        ];
    }

    public function  messages()
    {
        return [
            'name.required' => 'Tên tin tuyển dụng không được để trống',
            'contents.required' => 'Nội dung tin không được để trống'
        ];
    }
}
