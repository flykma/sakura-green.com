<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CategoryNew extends Model
{
    protected $table = "category_news";
    protected $fillable = ["categorynew","parentid","slug","description","status","seotitle"];

    public function snews() {
        return $this->hasMany(SNew::class);
    }
}

