<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Images extends Model
{
    protected $fillable = ['product_id','photoname'];

    public function product() {
        return $this->belongsTo('App\Product');
    }
}
