<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSNewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('s_news', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nametitle');
            $table->integer('catnew_id')->unsigned();
            $table->string('slug')->unique();
            $table->text('description')->nullable();
            $table->text('contents');
            $table->string('seotitle')->nullable();
            $table->boolean('status');
            $table->boolean('home');
            $table->boolean('hot');
            $table->string('tag')->nullable();
            $table->string('image')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('s_news');
    }
}
