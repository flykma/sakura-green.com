<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('productname');
            $table->string('slug')->unique();
            $table->string('symbol');
            $table->text('image')->nullable();
            $table->text('photo')->nullable();
            $table->text('description')->nullable();
            $table->text('contents')->nullable();
            $table->text('guide')->nullable();
            $table->decimal('price');
            $table->boolean('hot');
            $table->string('seotitle')->nullable();
            $table->integer('discount');
            $table->string('tag')->nullable();
            $table->integer('catpro_id')->unsigned();
            $table->boolean('status');
            $table->boolean('showhome');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
